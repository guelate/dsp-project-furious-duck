import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";
import { getToken } from "next-auth/jwt";

export async function middleware(req: NextRequest) {
  const secret = process.env.NEXTAUTH_SECRET;
  const token = await getToken({ req, secret });


  // console.log("coté middleware:", token);
  
  // console.log("Middleware is running for path:", req.nextUrl.pathname);

  const nonAdminRoutes = ["/jeuConcours", "/support", "/gains", "/article"];
  // const clientRoute = "/"

  //utilisateur connecté
  if (token) {
    //utilisateur ne peut pas accéder à l'admin
    if (token.role !== "admin" && req.nextUrl.pathname.startsWith("/admin")) {
      return NextResponse.redirect(new URL("/?error=not_authorized", req.url));
    }

    //admin ne peut pas accéder au jeu Concours
    if (
      token?.role === "admin" &&
      nonAdminRoutes.some((route) => req.nextUrl.pathname.startsWith(route))
    ) {
      return NextResponse.redirect(new URL("/admin", req.url));
    }
    
    //utilisateur bannie ne peut pas se connecter
    if (token.role == "user"  && !token.is_active) {
      return NextResponse.redirect(new URL("/block", req.url));
    }
  }

  //Si on est pas connecté on peut pas accéder à l'admin
  if (!token && req.nextUrl.pathname.startsWith("/admin")) {
   return NextResponse.redirect(new URL("/connexion", req.url));
 }

  return NextResponse.next();
}

export const config = {
  matcher: [
    "/",
    "/jeuConcours",
    "/support",
    "/gains",
    "/article",
    "/admin/:path*",
  ],
};

//google doit jouer
//user banni ne doit pas se connecter
//verifier aussi admin ne doit pas jouer 

