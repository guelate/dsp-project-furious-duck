import { render, screen } from "@testing-library/react";
import { SessionProvider } from "next-auth/react";
import Header from "@/components/Header";

// Mock de usePathname
jest.mock("next/navigation", () => ({
  usePathname: jest.fn().mockReturnValue("/"),
}));

// Créer un mock de `SessionProvider` pour envelopper le composant à tester
const MockSessionProvider = ({ children }: { children: React.ReactNode }) => (
  <SessionProvider session={null}>{children}</SessionProvider>
);

describe("Vérification de la présence du header pour assurer l'authentification", () => {
  it("should render the Header component without crashing", () => {
    // Rendre le composant Header avec le mock du SessionProvider
    render(
      <MockSessionProvider>
        <Header />
      </MockSessionProvider>
    );

    // Vérifier que le logo est présent dans le header
    const logoImage = screen.getByAltText("Logo");
    expect(logoImage).not.toBeNull(); 

  
  });
});
