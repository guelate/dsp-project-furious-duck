import { render, screen } from "@testing-library/react";
import { SessionProvider } from "next-auth/react";
import WheelText from "@/components/WheelText";

// Mock de `SessionProvider` pour les tests
const MockSessionProvider = ({ children }: { children: React.ReactNode }) => (
  <SessionProvider session={null}>{children}</SessionProvider>
);

describe("Vérification que la roue est présente", () => {
  it("should render the WheelText component without crashing", () => {
    // Rendu du composant enveloppé dans le `SessionProvider`
    render(
      <MockSessionProvider>
        <WheelText triggerSpin={() => {}} />
      </MockSessionProvider>
    );

    // Vérifier si le texte "Ne manquez pas votre chance !" est présent
    const heading = screen.getByText("Ne manquez pas votre chance !");
    expect(heading).not.toBeNull();
  });
});
