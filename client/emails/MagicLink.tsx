import {
    Body,
    Container,
    Head,
    Heading,
    Hr,
    Html,
    Img,
    Link,
    Preview,
    Section,
    Tailwind,
    Text,
  } from "@react-email/components";
  import * as React from "react";
  
  interface MagicLinkEmailProps {
    magicLink?: string;
    message?: string;
    email?: string;
  }
  
  const baseUrl = process.env.VERCEL_URL
    ? `https://${process.env.VERCEL_URL}`
    : "";
  
  export const MagicLinkEmail = ({
    magicLink, message, email
  }: MagicLinkEmailProps) => (
    <Tailwind
      config={{
        theme: {
          extend: {
            colors: {
              brand: "#007291",
            },
          },
        },
      }}
    >
    <Html>
      <Head />
      <Preview>Thé Tip Top</Preview>
      <Body className="bg-white font-sans">
        <Container className="m-auto p-5 pb-12 bg-no-repeat bg-bottom" style={{ backgroundImage: 'url("/assets/raycast-bg.png")' }}>
          
          <Heading className="text-2xl font-bold mt-12">Message Thé Tip Top</Heading>
          <Section className="my-6">
            {/* <Text className="text-base leading-6">
              <Link className="text-rose-500" href={magicLink}>
                👉 Cliquez ici pour vous connecter 👈
              </Link>
            </Text> */}
            <Text className="text-base leading-6">
                {message}
            </Text>
          </Section>
          <Text className="text-base leading-6">
             {email}
          </Text>
          <Hr className="border-t border-gray-300 mt-12" />
          
          <Text className="text-gray-600 text-xs ml-1">Thé Tip Top</Text>
          <Text className="text-gray-600 text-xs ml-1">
            Paris, France
          </Text>
        </Container>
      </Body>
    </Html>
    </Tailwind>
  );
  
  MagicLinkEmail.PreviewProps = {
    magicLink: "https://google.com",
    message: "Message",
    email: "Adresse Mail"
  } as MagicLinkEmailProps;
  
  export default MagicLinkEmail;
  