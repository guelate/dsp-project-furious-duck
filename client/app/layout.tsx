import type { Metadata } from "next";
import "./globals.css";
import Header from "./components/Header";
import Footer from "./components/Footer";
import SEO from './components/SEO';
import Script from 'next/script';
import ClientWrapper from './components/ClientWrapper'; 
import AuthProvider from "./components/AuthProvider"

export const metadata: Metadata = {
  title: "TheTipTop",
  description: "Generated by create next app",
  icons: {
    icon: "/logo.png",
    apple: "/logo.png",
  },
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="fr">
      <head>
        <SEO 
          title={metadata.title as string}
          description={metadata.description as string}
        />
        <Script
          strategy="afterInteractive"
          src="https://www.googletagmanager.com/gtag/js?id=G-KB40EHXHXP"
        />
        <Script
          id="google-analytics"
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', 'G-KB40EHXHXP');
            `,
          }}
        />
      </head>
      <body>
      <AuthProvider>
          <ClientWrapper>
            <Header />
            {children}
            <Footer />
          </ClientWrapper>
        </AuthProvider>
      </body>
    </html>
  );
}
//Google branch