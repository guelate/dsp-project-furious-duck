"use client"

import { useEffect, useState } from "react";
import Statistique from "./Statistique";

export default function AdminLine1() {
  const [inscrits, setInscrits] = useState<number>(0);
  const [participants, setParticipants] = useState<number>(0);
  const [nonParticipants, setNonParticipants] = useState<number>(0);
  const [emailAdmin, setEmailAdmin] = useState<number>(0);
  const [emailClient, setEmailClient] = useState<number>(0);
  const [tentativeIns, setTentativeIns] = useState<number>(0);
  const [tentativeCon, setTentativeCon] = useState<number>(0);
  const [tentativeP, setTentativeP] = useState<number>(0);

  // Fetch des statistiques utilisateurs (inscrits, participants, non-participants)
  const fetchStatistics = async () => {
    try {
      const response = await fetch("/api/tickets");
      const data = await response.json();

      setInscrits(data.totalUsers);        
      setParticipants(data.participants); 
      setNonParticipants(data.nonParticipants); 
    } catch (error) {
      console.error("Erreur lors de la récupération des statistiques:", error);
    }
  };

  const fetchCampaignEmail = async () => {
    try {
      console.log("Fetching email campaign logs count...");
      const response = await fetch('/api/logCount');
      console.log("Response status:", response.status);
      
      if (!response.ok) {
          throw new Error('Network response was not ok');
      }
      
      const data = await response.json();
      console.log("Fetched data:", data);
      setEmailAdmin(data.countEmailAdmin);
      setEmailClient(data.countEmailClient)
      setTentativeIns(data.countTI);
      setTentativeCon(data.countTC);
      setTentativeP(data.countTP);
    } catch (error) {
        console.error("Erreur lors de la récupération des statistiques:", error);
    }
};

  useEffect(() => {
    // Fetch les statistiques à chaque rendu
    fetchCampaignEmail();
    fetchStatistics();
  }, []);

  return (
    <div className="flex flex-col mt-10 ml-10 h-auto md:pl-5 md:pt-2">
      <div className="flex flex-col md:flex-row gap-10 md:gap-0 justify-between">
        {/* User */}
        <Statistique variant="short" title="Utilisateurs" style="">
          <div className="flex md:flex-row text-center">
            <p className="text-sm text-gray-500">
              Utilisateurs inscrit:
              <span className=" text-gray-500 px-1">{inscrits}</span>
            </p>

            <p className="text-sm text-gray-500">
              Utilisateurs participant:
              <span className=" text-gray-500 px-1">{participants}</span>
            </p>

            <p className="text-sm text-gray-500">
              Utilisateur non participant:
              <span className=" text-gray-500 px-1">{nonParticipants}</span>
            </p>
          </div>
        </Statistique>

        {/* Activity */}
        <Statistique
          variant="long"
          title="Activité"
          style="justify-center items-center"
        >
          <div className="flex flex-col gap-4">
            <div className="flex md:flex-row md:justify-center">
              <div className="flex md:flex-row md:gap-10">
                <p className="text-sm text-gray-500">
                  Tentative d'inscription:
                  <span className=" text-gray-500 px-1">{tentativeIns}</span>
                </p>

                <p className="text-sm text-gray-500">
                  Tentative de connexion:
                  <span className="text-gray-500 px-1">{tentativeCon}</span>
                </p>

                <p className="text-sm text-gray-500">
                  Tentative de participation:
                  <span className="text-gray-500 px-1">{tentativeP}</span>
                </p>
              </div>
            </div>

            <div className="flex gap-11 md:ml-5 py-2">
              <div className="flex md:gap-16">
                <p className="text-sm text-gray-500">
                  Email client envoyé:
                  <span className=" text-gray-500 px-1">{emailClient}</span>
                </p>
                <p className="text-sm text-gray-500">
                  Email admin envoyé:
                  <span className="text-gray-500 px-1">{emailAdmin}</span>
                </p>
              </div>
            </div>
          </div>
        </Statistique>
      </div>
    </div>
  );
}
