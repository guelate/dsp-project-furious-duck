import React, { useEffect, useState } from 'react';

const CookieNotice: React.FC = () => {
  const [accepted, setAccepted] = useState(false);

  useEffect(() => {

    const isAccepted = localStorage.getItem('cookiesAccepted') === 'true';
    setAccepted(isAccepted);
  }, []);

  const handleAccept = () => {

    localStorage.setItem('cookiesAccepted', 'true');
    setAccepted(true);

  };

  if (accepted) return null;

  return (
    <div className="fixed bottom-4 right-4 max-w-xs p-4 bg-white rounded-lg shadow-lg z-50">
      <span className="font-semibold text-gray-800">🍪 Avis sur les cookies</span>
      <p className="mt-4 text-sm leading-5 text-gray-600">
        Nous utilisons des cookies pour vous garantir la meilleure expérience sur notre site web.{' '}
        <a href="#" className="text-blue-500 hover:underline">
          Lire la politique de cookies
        </a>.
      </p>
      <div className="flex items-center justify-between mt-4 space-x-4">
        <button className="text-xs text-gray-800 underline hover:text-gray-500">
          Gérer vos préférences
        </button>
        <button
          className="text-xs font-medium text-white bg-gray-900 rounded-md px-4 py-2 hover:bg-gray-700"
          onClick={handleAccept}
        >
          Accepter
        </button>
        <button
          className="text-xs font-medium text-white bg-gray-900 rounded-md px-4 py-2 hover:bg-gray-700"
          onClick={handleAccept}
        >
          Refuser
        </button>
      </div>
    </div>
  );
};

export default CookieNotice;
