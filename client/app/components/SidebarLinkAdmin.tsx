"use client";

import Link from "next/link";

interface Props {
  href: string;
  children: any;
}

export default function SidebarLinkAdmin({ href, children }: Props) {
  return (
    <Link href={href}>
      <div
        className={`flex items-center px-4 py-2 my-1 text-base justify-between text-gray-200 hover:text-white hover:bg-[#33463a] rounded .rounded-lg"`}
      >
        {children}
      </div>
    </Link>
  );
}
