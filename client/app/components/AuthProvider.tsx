"use client";

import { SessionProvider } from "next-auth/react";

interface AuthProviderProps {
  children: any;
}

export default function AuthProvider({ children }: AuthProviderProps) {
  return <SessionProvider>{children}</SessionProvider>;
}
