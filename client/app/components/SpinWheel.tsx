import "./SpinWheel.css";
import React, { useRef, useEffect, useState } from "react";
import Popup from "./Popup";

interface CustomCSSProperties extends React.CSSProperties {
  "--i"?: number;
  "--clr"?: string;
}

interface SpinWheelProps {
  shouldSpin: boolean;
  onSpinEnd: () => void;
  prizeIndex: number | null; 
}

export default function SpinWheel({
  shouldSpin,
  onSpinEnd,
  prizeIndex,
}: SpinWheelProps) {
  const wheelRef = useRef<HTMLDivElement>(null);
  const [stoppedNumber, setStoppedNumber] = useState<number | null>(null);
  const [showPopup, setShowPopup] = useState(false);

  useEffect(() => {
    if (shouldSpin && prizeIndex !== null) {
      spinWheel(prizeIndex); // Passer l'index du gain à la fonction de rotation
    }
  }, [shouldSpin, prizeIndex]);

  const spinWheel = (prizeIndex: number) => {
    const wheel = wheelRef.current;

    if (wheel) {
      // Étape 1 : Calculer la position cible
      let value = Math.ceil(Math.random() * 60000); 
      const degreesPerSegment = 360 / 5;
      const targetDegrees = degreesPerSegment * prizeIndex; 

      // Ajouter des tours supplémentaires pour créer un effet visuel réaliste
      value += Math.ceil(Math.random() * 5) * 360 + targetDegrees;

      // Étape 2 : Appliquer la transformation
      wheel.style.transition = "transform 5s ease-out";
      wheel.style.transform = `rotate(${value}deg)`; 
   
      wheel.addEventListener(
        "transitionend",
        () => {
          setStoppedNumber(prizeIndex);
          setShowPopup(true); 
          onSpinEnd(); 
        },
        // Assurer que l'événement n'est exécuté qu'une fois
        { once: true } 
      );
    }
  };

  // Fonction pour définir la couleur en fonction du numéro arrêté
  const getColorForNumber = (i: number) => {
    if (stoppedNumber !== null && i !== stoppedNumber) {
      return "#D1D5DB"; // Couleur grise si ce n'est pas le segment arrêté
    }
    return "";
  };

  return (
    <div className="container">
      <div className="spinBtn" />
      <div ref={wheelRef} className="wheel">
        {[1, 2, 3, 4, 5].map((num) => (
          <div
            key={num}
            className="number"
            style={
              {
                "--i": num,
                "--clr": getColorForNumber(num) || getDefaultColor(num),
              } as CustomCSSProperties
            }
          >
            <span>{num}</span>
          </div>
        ))}
      </div>

      {/* Popup pour afficher le résultat */}
      {showPopup && stoppedNumber !== null && (
        <Popup
          stoppedNumber={stoppedNumber}
          onClose={() => setShowPopup(false)}
        />
      )}
    </div>
  );
}

// Fonction auxiliaire pour définir la couleur par défaut de chaque segment
const getDefaultColor = (num: number): string => {
  switch (num) {
    case 1:
      return "#D1D5DB"; // Gris
    case 2:
      return "#86EFAC"; // Vert clair
    case 3:
      return "#30593E"; // Vert foncé
    case 4:
      return "#86EFAC"; // Vert clair
    case 5:
      return "#c3c8d0"; // Gris plus foncé
    default:
      return "#D1D5DB"; // Gris par défaut
  }
};
