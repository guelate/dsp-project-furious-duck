"use client";

import { useState, useEffect } from "react";
import { Button } from "./ui/button";
import { AlignJustify, X } from "lucide-react";
import { Dialog, DialogContent, DialogTrigger } from "./ui/dialog";
import SignForm from "./SignForm";
import Image from "next/image";
import logo from "../../public/logo.png";
import { usePathname } from "next/navigation";
import Link from "next/link";
import { signOut, useSession } from "next-auth/react";
import { Badge } from "./ui/badge";

interface SessionUser {
  name?: string;
  email?: string;
  firstname?: string;
  image?: string;
  provider?: string;
}

export default function Header() {
  const pathname = usePathname();
  const { data: session } = useSession();
  const [user, setUser] = useState<SessionUser | null>(null);

  useEffect(() => {
    if (session?.user) {
      setUser(session.user as SessionUser);
    }
  }, [session]);

  const getDisplayName = () => {
    if (!user) return "Utilisateur";

    switch (user.provider) {
      case "google":
        return (
          user.firstname ||
          user.name ||
          user.email?.split("@")[0] ||
          "Utilisateur Google"
        );
      case "facebook":
        return user.name || user.email?.split("@")[0] || "Utilisateur Facebook";
      default:
        return user.firstname || user.email?.split("@")[0] || "Utilisateur";
    }
  };

  let Links = [
    { name: "Jeu Concours", link: "/jeuConcours" },
    { name: "Article", link: "/article" },
    { name: "Support", link: "/support" },
  ];
  let [open, setOpen] = useState(false);

  console.log("Session est:", session);
  console.log("User est:", user);

  return (
    <div
      className={`shadow-md w-full fixed top-0 left-0 z-20 ${
        pathname.startsWith("/admin") ? "hidden" : ""
      }`}
    >
      <div className="md:flex items-center justify-between bg-white py-2 md:px-10 px-7 md:border">
        <Link href="/" aria-label="Retour à l'accueil">
          <Image src={logo} width={80} height={80} alt="Logo" />
        </Link>

        <div
          onClick={() => setOpen(!open)}
          className="text-3xl absolute right-8 top-6 cursor-pointer md:hidden"
        >
          {open ? <X /> : <AlignJustify />}
        </div>

        <ul
          className={`md:gap-3 md:flex md:items-center md:pb-0 pb-12 absolute md:static bg-white z-10 left-0 top-20 w-full md:w-auto md:pl-0 pl-9 transition-all duration-500 ease-in ${
            open ? "top-10 " : "top-[-490px]"
          }`}
        >
          {Links.map((link) => (
            <li key={link.name} className="md:ml-8 text-xl md:my-0 my-7">
              <Link
                href={link.link}
                className="text-base hover:text-gray-400 duration-500 cursor-pointer text-[#445d4d]"
              >
                {link.name}
              </Link>
            </li>
          ))}

          {session ? (
            <div className="flex flex-col gap-3 md:flex md:flex-row w-1/4 items-center">
              <Badge className="rounded-lg bg-gray-200 hover:bg-gray-300 shadow-sm border border-gray-200 hover:border-gray-300 h-6 w-3/4 text-sm">
                {/* {getDisplayName()} */}
                {session?.user?.name}
              </Badge>
              {session.user?.image && (
                <Image
                  src={session.user.image}
                  width={32}
                  height={32}
                  alt="Profile"
                  className="rounded-full"
                />
              )}
              <Button
                onClick={() => signOut({ callbackUrl: "/" })}
                className="bg-[#445d4d] hover:bg-[#445d4d] rounded-lg duration-500 text-white"
              >
                Déconnexion
              </Button>
            </div>
          ) : (
            <div className="flex flex-col gap-3 md:flex md:flex-row w-1/4">
              <Dialog>
                <DialogTrigger asChild>
                  <Button className="bg-[#445d4d] hover:bg-[#445d4d] hover:shadow-lg rounded .rounded-lg duration-500 text-white">
                    Connexion
                  </Button>
                </DialogTrigger>
                <DialogContent className="justify-center items-center rounded-lg bg-white">
                  <SignForm signType="signIn" />
                </DialogContent>
              </Dialog>

              <Dialog>
                <DialogTrigger asChild>
                  <Button className="bg-[#445d4d] hover:bg-[#445d4d] hover:shadow-lg rounded .rounded-lg duration-500 text-white">
                    Inscription
                  </Button>
                </DialogTrigger>
                <DialogContent className="justify-center items-center rounded-lg bg-white">
                  <SignForm signType="signUp" />
                </DialogContent>
              </Dialog>
            </div>
          )}
        </ul>
      </div>
    </div>
  );
}

