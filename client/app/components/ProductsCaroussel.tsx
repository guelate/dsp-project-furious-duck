"use client";

import { Card, CardContent } from "@/components/ui/card";
import {
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
} from "@/components/ui/carousel";

const carouselItems = [
  {
    image: "/infuseur.png",
    title: "infuseur à thé	",
    description:
      "Un infuseur à thé pratique et élégant pour vos infusions quotidiennes.",
  },
  {
    image: "/theDetox.png",
    title: "100g de thé détox ou d'infusion",
    description:
      "Un assortiment de thés détox ou d'infusions pour une expérience relaxante. (au hasard)",
  },
  {
    image: "/signature.png",
    title: "100g de thé signature",
    description: "Une boîte de thé signature pour une dégustation raffinée.",
  },
  {
    image: "/coffret1.png",
    title: "coffret découverte d'une valeur de 39€",
    description:
      "Un coffret découverte contenant une sélection de thés exclusifs.",
  },
  {
    image: "/coffret2.png",
    title: "coffret découverte d'une valeur de 69€",
    description:
      "Un coffret découverte premium pour les amateurs de thé exigeants..",
  },
];

export default function ProductsCaroussel() {
  return (
    <div className="mb-40">
      <h1 className="flex justify-center items-center md:text-3xl text-[#445d4d] mb-20">LOT À GAGNER</h1>
      <Carousel className="w-full max-w-5xl mx-auto border border-gray-300 rounded .rounded-lg shadow-md hover:shadow-lg">
        <CarouselContent>
          {carouselItems.map((item, index) => (
            <CarouselItem key={index}>
              <div className="flex flex-row gap-6 p-6">
                <Card className="flex-shrink-0 w-[400px]">
                  <CardContent className="p-0">
                    <img
                      src={item.image}
                      alt={item.title}
                      className="w-full h-[300px] object-cover rounded .rounded-lg"
                    />
                  </CardContent>
                </Card>
                <div className="flex flex-col justify-center">
                  <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">
                    {item.title}
                  </h2>
                  <p className="text-gray-600">{item.description}</p>
                </div>
              </div>
            </CarouselItem>
          ))}
        </CarouselContent>
        <CarouselPrevious />
        <CarouselNext />
      </Carousel>
    </div>
  );
}
