import Head from 'next/head'

interface SEOProps {
  title: string;
  description: string;
  keywords?: string;
  ogImage?: string;
  ogUrl?: string;
}

export default function SEO({ 
  title, 
  description, 
  keywords, 
  ogImage, 
  ogUrl 
}: SEOProps) {
  const siteTitle = "Votre Nom de Site";
  
  return (
    <Head>
      <title>{`${title} | ${siteTitle}`}</title>
      <meta name="description" content={description} />
      {keywords && <meta name="keywords" content={keywords} />}
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:site_name" content={siteTitle} />
      {ogImage && <meta property="og:image" content={ogImage} />}
      {ogUrl && <meta property="og:url" content={ogUrl} />}
      <meta name="twitter:card" content="summary_large_image" />
      <link rel="canonical" href={ogUrl} />
    </Head>
  )
}