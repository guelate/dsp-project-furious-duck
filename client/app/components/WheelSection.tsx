"use client";
 
import { useState } from "react";
import SpinWheel from "./SpinWheel";
import WheelText from "./WheelText";
 
export default function WheelSection() {
  // État pour gérer si la roue doit tourner ou non
  const [shouldSpin, setShouldSpin] = useState(false);
 
  // Nouvel état pour gérer l'index du gain
  const [prizeIndex, setPrizeIndex] = useState<number | null>(null);
 
  // Fonction pour déclencher la rotation de la roue avec l'index du gain
  const triggerSpin = (index: number) => {
    setPrizeIndex(index); // Met à jour l'index du gain dans l'état
    setShouldSpin(true);  // Déclenche la rotation de la roue
  };
 
  // Fonction pour arrêter la rotation de la roue (optionnelle)
  const stopSpin = () => {
    setShouldSpin(false); // Arrête la rotation
  };
 
  return (
    <div
      id="wheel-section"
      className="flex flex-col items-center justify-center w-full p-4 md:w-full md:flex-row md:justify-around md:items-center md:my-48 mt-20 mx-auto"
    >
      <div className="w-full md:w-1/2 flex justify-center items-center mb-4 md:mb-0">
        {/* Passer l'état, l'index du gain et la fonction pour arrêter la rotation */}
        <SpinWheel shouldSpin={shouldSpin} onSpinEnd={stopSpin} prizeIndex={prizeIndex} />
      </div>
      <div className="w-full md:w-1/2 flex my-10 md:my-0 justify-center">
        {/* Passer la fonction de déclenchement avec l'index du gain au composant du formulaire */}
        <WheelText triggerSpin={triggerSpin} />
      </div>
    </div>
  );
}
 