import React from 'react';
import { Card, CardContent, CardDescription, CardHeader } from "./ui/card";
import { Input } from "./ui/input";
import { Label } from "./ui/label";
import { Checkbox } from "./ui/checkbox";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "./ui/select";
import { ChangeEvent } from "react";

interface RegistrationFormProps {
  formData: {
    fullName: string;
    email: string;
    password: string;
    confirmPassword: string;
    address: string;
    dateOfBirth: string;
    gender: string;
    termsAccepted: boolean;
  };
  handleInputChange: (e: ChangeEvent<HTMLInputElement | HTMLSelectElement>) => void;
  handleCheckboxChange: (e: ChangeEvent<HTMLInputElement>) => void;
}



export default function RegistrationForm({
  formData,
  handleInputChange,
  handleCheckboxChange,
}: RegistrationFormProps) {

  return (
    <div className="border border-none">
      <CardHeader>
        <CardDescription>
          Remplissez le formulaire ci-dessous pour créer votre compte.
        </CardDescription>
      </CardHeader>
      <CardContent className="grid gap-4">
        <div className="grid grid-cols-2 gap-4">
          <div className="space-y-2">
            <Label htmlFor="fullName">Nom complet</Label>
            <Input
              id="fullName"
              value={formData.fullName}
              onChange={handleInputChange}
              placeholder="Entrez votre nom complet"
              required
            />
          </div>
          <div className="space-y-2">
            <Label htmlFor="email">Adresse e-mail</Label>
            <Input
              id="email"
              type="email"
              value={formData.email}
              onChange={handleInputChange}
              placeholder="Entrez votre adresse e-mail"
              required
            />
          </div>
        </div>
        <div className="grid grid-cols-2 gap-4">
          <div className="space-y-2">
            <Label htmlFor="password">Mot de passe</Label>
            <Input
              id="password"
              type="password"
              value={formData.password}
              onChange={handleInputChange}
              placeholder="Entrez un mot de passe sécurisé"
              required
            />
          </div>
          <div className="space-y-2">
            <Label htmlFor="confirmPassword">Confirmez le mot de passe</Label>
            <Input
              id="confirmPassword"
              type="password"
              value={formData.confirmPassword}
              onChange={handleInputChange}
              placeholder="Confirmez votre mot de passe"
              required
            />
          </div>
        </div>
        <div className="space-y-2">
          <Label htmlFor="address">Adresse postale</Label>
          <Input
            id="address"
            value={formData.address}
            onChange={handleInputChange}
            placeholder="Entrez votre adresse postale"
            required
          />
        </div>
        <div className="grid grid-cols-2 gap-4 pb-4">
          <div className="space-y-2">
            <Label htmlFor="dateOfBirth">Date de naissance</Label>
            <Input
              id="dateOfBirth"
              type="date"
              value={formData.dateOfBirth}
              onChange={handleInputChange}
              required
            />
          </div>
          <div className="space-y-2">
            <Label htmlFor="gender">Genre</Label>
            <Select
              value={formData.gender}
              onValueChange={(value) => handleInputChange({
                target: { id: "gender", value } as HTMLSelectElement,
              } as ChangeEvent<HTMLSelectElement>)}
            >
              <SelectTrigger id="gender">
                <SelectValue placeholder="Sélectionnez votre genre" />
              </SelectTrigger>
              <SelectContent className="bg-white">
                <SelectItem value="male">Homme</SelectItem>
                <SelectItem value="female">Femme</SelectItem>
                <SelectItem value="unspecified">Non spécifié</SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>
        <div className="items-top flex gap-2">
          <div>
          <input
          type="checkbox"
          id="termsAndConditions"
          checked={formData.termsAccepted}
          onChange={handleCheckboxChange}
        />
          </div>
        
          <div className="grid gap-1.5 leading-none"> 
            <label
              htmlFor="termsAndConditions"
              className="text-sm font-medium leading-none"
            >
              J'accepte les Conditions Générales et la Politique de Confidentialité
            </label>
            <p className="text-xs text-muted-foreground">
              En cochant cette case, vous acceptez nos Conditions d'Utilisation et notre Politique de Confidentialité.
            </p>
          </div>
        </div>
      </CardContent>
    </div>
  );
}
