import React, { useEffect, useRef } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';

interface BackgroundCaptchaProps {
  siteKey: string;
  onVerify: (token: string | null) => void;
}

const BackgroundCaptcha: React.FC<BackgroundCaptchaProps> = ({ siteKey, onVerify }) => {
  const captchaRef = useRef<ReCAPTCHA>(null);

  useEffect(() => {
    const executeReCaptcha = async () => {
      if (captchaRef.current) {
        const token = await captchaRef.current.executeAsync();
        onVerify(token);
      }
    };

    executeReCaptcha();
  }, [onVerify]);

  return (
    <div style={{ position: 'fixed', bottom: 0, right: 0, opacity: 0.1 }}>
      <ReCAPTCHA
        ref={captchaRef}
        size="invisible"
        sitekey={siteKey}
      />
    </div>
  );
};

export default BackgroundCaptcha;