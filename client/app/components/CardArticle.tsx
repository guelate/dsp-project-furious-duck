import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import Image from "next/image";

interface ArticleProps {
  id: string;
  title: string;
  content: string;
  imageUrl: any;
}
import { Pencil, Trash2 } from "lucide-react";

import { Button } from "@/components/ui/button";

export default function CardArticle({
  id,
  title,
  content,
  imageUrl,
}: ArticleProps) {
  return (
    <Card className=" rounded .rounded-lg shadow-md md:w-5/12 max-w-2xl mx-auto bg-gray-200 text-white">
      <CardHeader>
        <CardTitle className="text-[#445d4d]">{title}</CardTitle>
      </CardHeader>
      <CardContent className="space-y-4">
        <div className="relative w-full h-64">
          <Image
            src={imageUrl}
            alt={title}
            fill
            className="object-cover rounded-md bg-white"
          />
        </div>
        <p className="text-muted-foreground">{content}</p>
      </CardContent>
      <CardFooter className="flex justify-end gap-4 b">
        <Button
          variant="destructive"
          className="border border-gray-500 bg-[#445d4d] rounded .rounded-lg shadow-sm hover:shadow-md  hover:bg-[#445d4d]"
        >
          <Pencil className="w-4 h-4 mr-2" />
          Éditer
        </Button>
        <Button
          variant="destructive"
          className="border bg-[#445d4d] rounded .rounded-lg shadow-sm hover:shadow-md  hover:bg-[#445d4d] "
        >
          <Trash2 className="w-4 h-4 mr-2" />
          Supprimer
        </Button>
      </CardFooter>
    </Card>
  );
}
