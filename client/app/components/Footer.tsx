"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";
import { Phone, MapPin } from "lucide-react";

export default function Footer() {
  const pathname = usePathname();
  return (
    <footer
      className={`shadow-md w-full top-0 left-0 z-20 bg-[#30593E] text-white pt-3 ${
        pathname.startsWith("/admin") || pathname.startsWith("/connexion")
          ? "hidden"
          : ""
      }`}
    >
      <div className="flex flex-col md:flex-row items-center md:pr-10 justify-around">
        <div className="flex flex-col  text-center">
          <h2 className="text-lg font-semibold my-4">Contact</h2>

          <p className="flex items-center">
            <Phone className="w-5" />
            &nbsp;
            <span>
              <a href="tel:++33123456789">+33 1 23 45 67 89</a>
            </span>
          </p>
        </div>

        <div className="flex flex-col text-center">
          <h2 className="text-lg font-semibold my-4">Localisation</h2>
          <p className="flex items-center">
            <MapPin className="w-5" />
            &nbsp;
            <span>Siège Social : 18 rue Léon Frot, 75011 Paris</span>
          </p>
        </div>

        <div className="flex flex-col text-center">
          <h2 className="text-lg font-semibold my-4">Liens utiles</h2>
          <ul className="flex flex-row gap-3">
            <Link href="/" className="hover:text-gray-400">
              Accueil
            </Link>
            |
            <Link href="/jeuConcours" className="hover:text-gray-400">
              Jeu Concours
            </Link>
            |
            <Link href="/support" className="hover:text-gray-400">
              Support
            </Link>
            |
            <Link href="/Docs" className="hover:text-gray-400">
              Documentation
            </Link>
          </ul>
        </div>

        <div className="flex flex-col">
          <h2 className="text-lg font-semibold my-4">Suivez Nous</h2>
          <div className="flex flex-row justify-center">
            <a href="https://www.instagram.com/the_tiptop_paris?igsh=dzdqZTNhNmJyYmtu">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="lucide lucide-instagram"
              >
                <rect width="20" height="20" x="2" y="2" rx="5" ry="5" />
                <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z" />
                <line x1="17.5" x2="17.51" y1="6.5" y2="6.5" />
              </svg>
            </a>
            <a href="https://www.facebook.com/people/Killian-Guelate/pfbid0qJ1YzqkwmbEQHmfdrxGi1SRZqwyikBDaMrKhxkMEvtBZDRcAdUr1KLUe1XSqycB6l/">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="lucide lucide-facebook"
              >
                <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z" />
              </svg>
            </a>
          </div>
        </div>
      </div>

      <div className="mt-8 py-8 border-t border-gray-300 text-center flex flex-col gap-1">
        <p className="text-base">
          &copy; {new Date().getFullYear()} Furious Ducks. Tous droits réservés.
        </p>
        <div className="text-sm">
          <a href="/cgv-cgu/cgv">Conditions Générales de Vente (CGV)</a> |&nbsp;
          <a href="/cgv-cgu/cgu">
            Conditions Générales d'Utilisation (CGU)
          </a>{" "}
          |&nbsp;
          <a href="/cgv-cgu/mentions">Mentions légales</a>
        </div>
        <p>
          Ce site est un projet étudiant fictif. Les achats et réservations
          effectués ici ne sont pas réels.
        </p>
      </div>
    </footer>
  );
}
