"use client";
import Link from "next/link";

import {
  MutableRefObject,
  useEffect,
  forwardRef,
  useRef,
  useState,
} from "react";
import { User } from "lucide-react";
import clsx from "clsx";
import { Badge } from "./ui/badge";
import { usePathname } from "next/navigation";
import { getSession, signOut } from "next-auth/react";
import { Button } from "./ui/button";

interface MenuProps {
  align: "left" | "right";
  children: React.ReactNode;
  [x: string]: any;
}



interface MenuItemProps {
  href: string;
  active?: boolean;
  children: React.ReactNode;
}

interface MenuItemProps {
  href: string;
  active?: boolean;
  children: React.ReactNode;
}

// Définir un type plus générique pour la session utilisateur retournée par next-auth
interface SessionUser {
  name?: string;
  email?: string;
}



const Menu = forwardRef<HTMLDivElement, MenuProps>(function Menu(
  { align = "right", children, ...rest },
  ref
) {
  return (
    <div
      ref={ref}
      role="menu"
      className={clsx(
        "absolute z-10 w-48 mt-3 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none flex flex-col gap-4",
        { "left=0": align === "left", "right-0": align === "right" }
      )}
      aria-orientation="vertical"
      tabIndex={-1}
      {...rest}
    >
      {children}
    </div>
  );
});

function MenuItem({ href, active, children }: MenuItemProps) {
  return (
    <Link
      href={href}
      tabIndex={-1}
      role="menuitem"
      className={clsx(
        "block px-4 py-2 text-sm text-gray-700 hover:bg-gray-200",
        { "bg-gray-200": active }
      )}
    >
      {children}
    </Link>
  );
}

const useOutsideClick = (
  ref: MutableRefObject<HTMLElement | null>,
  callback: () => void
) => {
  useEffect(() => {
    const handleClick = (e: MouseEvent) => {
      if (ref.current && !ref.current.contains(e.target as Node)) {
        callback();
      }
    };

    document.addEventListener("click", handleClick);

    return () => document.removeEventListener("click", handleClick);
  }, [ref, callback]);
};

export default function NavbarAdmin() {
  const appMenuRef = useRef(null);
  const userMenuRef = useRef(null);
  const [appMenuOpen, setAppMenuOpen] = useState(false);
  const [userMenuOpen, setUserMenuOpen] = useState(false);

  useOutsideClick(appMenuRef, () => {
    setAppMenuOpen(false);
  });

  const handleAppMenuClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    setAppMenuOpen(!appMenuOpen);
  };

  const handleUserMenuClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    setUserMenuOpen(!userMenuOpen);
  };

  const pathname = usePathname();
  const [user, setUser] = useState<SessionUser | null>(null);

  useEffect(() => {
    const handleSession = async () => {
      const session = await getSession();
      console.log("session:", session);
  
      if (session && session.user) {
        setUser(session.user as SessionUser); // Assurez-vous que le type est compatible
      }
    };
  
    handleSession();
  }, []);

  return (
    <div>
      <div className="flex items-center mx-2 h-16 bg-[#445d4d] justify-between md:justify-end pr-5">
        {/* <div className="md:w-full md:text-center hidden"> */}
        <div className="hidden md:block md:w-4/5 md:text-center w-full">
          <h1 className="text-2xl text-white">Tableau de Bord</h1>
        </div>
        <div className="relative mx-1 lg:hidden">
          <button
            type="button"
            className="rounded-full p-1 text-gray-500 hover:text-gray-600 focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-600"
            id="app-menu-button"
            aria-haspopup="true"
            aria-expanded={appMenuOpen}
            onClick={handleAppMenuClick}
            >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="size-6"
              >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                />
            </svg>
          </button>

          {appMenuOpen && (
            <Menu
            ref={appMenuRef}
            aria-labelledby="app-menu-button"
            align="left"
            >
              <MenuItem href="/admin">
                <h1>Accueil admin</h1>
              </MenuItem>
              <MenuItem href="/admin/documentation">
                <h1>Documentation</h1>
              </MenuItem>
              <MenuItem href="/admin/users">
                <h1>Gestion des utilisateurs</h1>
              </MenuItem>
              <MenuItem href="/admin/tickets">
                <h1>Gestion des tickets</h1>
              </MenuItem>
              <MenuItem href="/admin/awards">
                <h1>Gestion des prix</h1>
              </MenuItem>
              <MenuItem href="/admin/articles">
                <h1>Gestion des articles</h1>
              </MenuItem>
              <MenuItem href="/admin/campaingns">
                <h1>Campagne de courriel</h1>
              </MenuItem>
            </Menu>
          )}
        </div>
          <h1 className="md:hidden text-white text-2xl">Tableau de bord</h1>

        <div className="flex items-center mx-2">
          <div className="relative mx-1">
            <button
              type="button"
              className="rounded-full p-1 focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-600"
              id="user-menu-button"
              aria-haspopup="true"
              aria-expanded={userMenuOpen}
              onClick={handleUserMenuClick}
            >
              <User className="text-white" size={28} />
            </button>

            {userMenuOpen && (
              <Menu ref={userMenuRef} aria-labelledby="user-menu-button">
                <div className="flex justify-center items-center">
                  <div className="border rounded .rounded-lg bg-green-100">
                    <Badge variant="secondary" className="text-center">
                      {user?.name}
                    </Badge>
                  </div>
                </div>

                <Button 
                  onClick={() => signOut({ callbackUrl: "/" })}
                  className="bg-[#445d4d] hover:bg-white hover:text-[#445d4d] rounded-lg duration-500 text-white"
                >
                  Déconnexion
                </Button>
              </Menu>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
