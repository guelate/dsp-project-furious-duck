'use client';

import React, { useState } from 'react';

interface CommandBlockProps {
  command: string;
  explanation: string;
}

const CommandBlock: React.FC<CommandBlockProps> = ({ command, explanation }) => {
  const [copied, setCopied] = useState(false);

  const copyToClipboard = () => {
    navigator.clipboard.writeText(command);
    setCopied(true);
    setTimeout(() => setCopied(false), 2000); // Réinitialise l'état après 2 secondes
  };

  return (
    <div className="bg-gray-900 text-white rounded-lg p-4 relative shadow-md">
      <div className="mb-2 flex justify-between items-center text-gray-400">
        <span className="text-sm">Terminal</span>
        <button 
          onClick={copyToClipboard} 
          className="bg-gray-700 text-gray-200 hover:bg-gray-600 rounded px-2 py-1 text-xs flex items-center"
        >
          <svg 
            xmlns="http://www.w3.org/2000/svg" 
            fill="none" 
            viewBox="0 0 24 24" 
            strokeWidth="1.5" 
            stroke="currentColor" 
            className="w-4 h-4 mr-1"
          >
            <path 
              strokeLinecap="round" 
              strokeLinejoin="round" 
              d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 007.5 18h4.5M15.75 15H18M15.75 12H21M7.5 9l1.5 1.5L7.5 12"
            />
          </svg>
          {copied ? 'Copied!' : explanation}
        </button>
      </div>
      <pre className="bg-gray-800 p-3 rounded-md text-sm text-green-400">
        <code>{command}</code>
      </pre>
    </div>
  );
};

export default CommandBlock;
