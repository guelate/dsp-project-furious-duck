import React from "react";
import { Button } from "./ui/button";

interface PopupProps {
  message: string;
  onclose:any;

}

export default function PopupConnection({ message,onclose}: PopupProps) {
  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
      <div className="bg-white p-6 rounded-lg shadow-lg text-center">
        <p className="text-lg font-semibold mb-4">{message}</p>
        <Button
          className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded"
          onClick={onclose}
        >
          Fermer
        </Button>
      </div>
    </div>
  );
}
