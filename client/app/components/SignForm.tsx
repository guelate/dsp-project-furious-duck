/* eslint-disable react-hooks/rules-of-hooks */
"use client";

import { GoogleSignInButton } from "./authButton/GoogleSignButton";
import { FacebookSignInButton } from "./authButton/FacebookSignButton";
import { Button } from "./ui/button";
import { useRouter } from "next/navigation";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { SignInSchema, SignUpSchema } from "@/lib/zod/SignInSchema";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { z } from "zod";
import { SignFormProps } from "@/lib/type/type";
import { Label } from "./ui/label";
import { CardContent, CardDescription, CardHeader } from "./ui/card";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "./ui/select";
import { signIn, getSession } from "next-auth/react";
import { useEffect, useState } from "react";


export default function SignInForm({ signType }: SignFormProps) {
  // State du formulaire pour react-hook-form
  const router = useRouter();
  // Ajoute un état pour gérer l'état de soumission
  const [isSubmitting, setIsSubmitting] = useState(false);

  const formIn = useForm<z.infer<typeof SignInSchema>>({
    resolver: zodResolver(SignInSchema),
    defaultValues: {
      firstname: "",
      password: "",
    },
  });


  async function onSubmitIn(values: z.infer<typeof SignInSchema>) {
    setIsSubmitting(true);

    try {
      const result = await signIn("credentials", {
        redirect: false,
        ...values,
        provider: "user",
      });

      const response = await fetch("/api/SignIn", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
        body: JSON.stringify(values),
      });

      //TODO: if result && result.firstname != useSession()
      if (result?.ok && !result.error) {
        console.log("Result:", result);

        const session = await getSession();  // Récupérer la session actuelle
        if (session) {
          console.log("Session après connexion:", session);   
          location.assign("/");
        }
      } else {
        console.error("Erreur lors de la connexion:", result?.error);
      }
      
    } catch (error) {
      console.error("L'authentification a échoué", error);
    } finally {
      setIsSubmitting(false);  // Réinitialiser l'état de soumission à la fin
    }
  }
  

  //TODO: DECOMPOSE COMPONENT // SPLIT IT // GIVE BETTER STRCUTURE
  //rename formUp and export this
  const formUp = useForm<z.infer<typeof SignUpSchema>>({
    resolver: zodResolver(SignUpSchema),
    defaultValues: {
      firstname: "",
      lastname: "",
      email: "",
      password_hash: "",
      confirmPassword: "",
      date_of_birth: "",
      gender: "Homme",
      address: "",
      city: "",
      postal_code: "",
      phone_number: "",
    },
  });

  async function onSubmitUp(values: z.infer<typeof SignUpSchema>) {
    setIsSubmitting(true);
    try {
      const response = await fetch("/api/SignUp", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      });

      if (response.ok) {
        const data = await response.json();
        console.log("Inscription réussie:", data);
        location.assign("/jeuConcours");
        // Rediriger après succès
        // router.push("/login");  // décommenter si utilisation de router pour redirection
      } else {
        const errorData = await response.json();
        console.error("Erreur d'inscription:", errorData.message);
        // Afficher un message d'erreur à l'utilisateur (par exemple avec un toast)
      }
    } catch (error) {
      console.error("Erreur lors de la soumission du formulaire:", error);
      // Gérer les erreurs inattendues
    } finally {
      setIsSubmitting(false);  // Réinitialiser l'état de soumission à la fin
    }
  }

  const onError = (errors: any) => {
    console.log("Form errors: ", errors);
  };

  return (
    <div className="">
      {signType === "signIn" ? (
        <div className="">
          <GoogleSignInButton />
          <FacebookSignInButton />
          <div className=" py-3">
            <span className="text-2xl font-semibold text-center mx-40">ou</span>
          </div>

          <Form {...formIn}>
            <form onSubmit={formIn.handleSubmit(onSubmitIn)}>
              <div className="flex flex-col justify-center gap-5">
                <div className="flex flex-col gap-1">
                  <FormField
                    control={formIn.control}
                    name="firstname"
                    render={({ field }) => (
                      <FormItem>
                        <FormControl>
                          <Input placeholder="Prénom" {...field} />
                        </FormControl>

                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </div>

                <div className="flex flex-col gap-1">
                  <FormField
                    control={formIn.control}
                    name="password"
                    render={({ field }) => (
                      <FormItem>
                        <FormControl>
                          <Input
                            placeholder="Mot de passe"
                            type="password"
                            {...field}
                          />
                        </FormControl>

                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </div>
              </div>
              <div className="flex justify-center items-center p-4">
                <Button
                  type="submit"
                  variant="secondary"
                  disabled={isSubmitting}  // Désactiver le bouton pendant la soumission
                  className={`border rounded .rounded-lg ${isSubmitting ? 'bg-gray-400' : 'bg-slate-200 hover:bg-slate-300'} cursor-pointer duration-500`}
                >
                  {isSubmitting ? "Traitement en cours..." : "Valider"}  {/* Afficher un texte alternatif */}
                </Button>
              </div>
            </form>
          </Form>
        </div>
      ) : (
        <Form {...formUp}>
          <form onSubmit={formUp.handleSubmit(onSubmitUp, onError)}>
            <div className="border border-none bg-white">
              <CardHeader>
                <CardDescription>
                  Remplissez le formulaire ci-dessous pour créer votre compte.
                </CardDescription>
              </CardHeader>

              <CardContent className="flex flex-col gap-5">
                {/* Prénom et Nom */}
                <div className="flex md:flex-row md:justify-between">
                  <div className="space-y-2">
                    <FormField
                      control={formUp.control}
                      name="firstname"
                      render={({ field }) => (
                        <FormItem>
                          <FormControl>
                            <Input placeholder="Prénom" {...field} />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </div>
                  <div className="">
                    <FormField
                      control={formUp.control}
                      name="lastname"
                      render={({ field }) => (
                        <FormItem>
                          <FormControl>
                            <Input placeholder="Nom" {...field} />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </div>
                </div>

                {/* Email et Genre */}
                <div className="space-y-2">
                  <div className="space-y-2">
                    <FormField
                      control={formUp.control}
                      name="address"
                      render={({ field }) => (
                        <FormItem>
                          <FormControl>
                            <Input placeholder="Adresse" {...field} />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </div>
                </div>

                {/* Mot de passe et Confirmation */}
                <div className="flex md:flex-row md:justify-between">
                  <div className="space-y-2">
                    <FormField
                      control={formUp.control}
                      name="password_hash"
                      render={({ field }) => (
                        <FormItem>
                          <FormControl>
                            <Input
                              type="password"
                              placeholder="Mot de passe"
                              {...field}
                            />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </div>
                  <div className="space-y-2">
                    <FormField
                      control={formUp.control}
                      // name="password_hash"
                      name="confirmPassword"
                      render={({ field }) => (
                        <FormItem>
                          <FormControl>
                            <Input
                              type="password"
                              placeholder="Confirmez"
                              {...field}
                            />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </div>
                </div>

                {/* Email et Ville */}
                <div className="flex md:flex-row md:justify-between">
                  <FormField
                    control={formUp.control}
                    name="email"
                    render={({ field }) => (
                      <FormItem>
                        <FormControl>
                          <Input placeholder="Email" {...field} />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />

                  <div className="space-y-2">
                    <FormField
                      control={formUp.control}
                      name="city"
                      render={({ field }) => (
                        <FormItem>
                          <FormControl>
                            <Input placeholder="Ville" {...field} />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </div>
                </div>

                {/* Date de naissance et code postal */}
                <div className="flex md:flex-row md:justify-between">
                  <div className="space-y-2">
                    <FormField
                      control={formUp.control}
                      name="phone_number"
                      render={({ field }) => (
                        <FormItem>
                          <FormControl>
                            <Input
                              placeholder="Numéro de téléphone"
                              {...field}
                            />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </div>
                  <div className="space-y-2">
                    <FormField
                      control={formUp.control}
                      name="postal_code"
                      render={({ field }) => (
                        <FormItem>
                          <FormControl>
                            <Input placeholder="Code postal" {...field} />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </div>
                </div>
                <div className="flex md:flex-row md:justify-between md:items-end">
                  <div className="space-y-2 md:w-2/5">
                    <Label htmlFor="date_of_birth">Date de naissance</Label>
                    <FormField
                      control={formUp.control}
                      name="date_of_birth"
                      render={({ field }) => (
                        <FormItem>
                          <FormControl>
                            <Input type="date" {...field} />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </div>
                  {/* Genre */}
                  <div className="space-y-2">
                    <FormField
                      control={formUp.control}
                      name="gender"
                      render={({ field }) => (
                        <FormItem>
                          <FormControl>
                            <Select
                              onValueChange={field.onChange}
                              defaultValue={field.value}
                            >
                              <SelectTrigger>
                                <SelectValue placeholder="Sélectionnez votre genre" />
                              </SelectTrigger>
                              <SelectContent>
                                <SelectItem value="Homme">Homme</SelectItem>
                                <SelectItem value="Femme">Femme</SelectItem>
                                <SelectItem value="Autre">Autre</SelectItem>
                              </SelectContent>
                            </Select>
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </div>
                </div>

                {/* Terms and Conditions */}
                <div className="items-top flex gap-2">
                  <div>
                    <input type="checkbox" id="termsAndConditions" required />
                  </div>
                  <div className="grid gap-1.5 leading-none">
                    <label
                      htmlFor="termsAndConditions"
                      className="text-sm font-medium leading-none"
                    >
                      J'accepte les Conditions Générales et la Politique de
                      Confidentialité
                    </label>
                    <p className="text-xs text-muted-foreground">
                      En cochant cette case, vous acceptez nos Conditions
                      d'Utilisation et notre Politique de Confidentialité.
                    </p>
                  </div>
                </div>
              </CardContent>
            </div>
            <div className="flex justify-center items-center p-4">
                <Button
                  type="submit"
                  variant="secondary"
                  disabled={isSubmitting}  // Désactiver le bouton pendant la soumission
                  className={`border rounded .rounded-lg ${isSubmitting ? 'bg-gray-400' : 'bg-slate-200 hover:bg-slate-300'} cursor-pointer duration-500`}
                >
                  {isSubmitting ? "Traitement en cours..." : "Valider"}  {/* Afficher un texte alternatif */}
                </Button>
            </div>
          </form>
        </Form>
      )}
    </div>
  );
}
