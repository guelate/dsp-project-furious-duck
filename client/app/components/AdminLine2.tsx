"use client"

import { useEffect, useState } from "react";
import Statistique from "./Statistique";

export default function AdminLine2() {

  const [totalTickets, setTotalTickets] = useState<number>(0);
  const [receive, setReceive] = useState<number>(0);
  const [notReceive, setNotReceive] = useState<number>(0);
  const [totalPrice, setTotalPrice] = useState<number>(0);


  const fetchPrice = async () => {
    try {
      const response = await fetch("/api/prizeClaim");
      const data = await response.json();
      setReceive(data.receive);
      setNotReceive(data.NotReceive);
      setTotalPrice(data.totalItems)
    } catch (error) {
      console.error("Erreur lors de la récupération des participants:", error);
    }
  };


  // Fetch des participants et non-participants
  const fetchParticipants = async () => {
    try {
      const response = await fetch("/api/tickets");
      const data = await response.json();
      setTotalTickets(data.ticketsRestant);
    } catch (error) {
      console.error("Erreur lors de la récupération des participants:", error);
    }
  };

  useEffect(() => {
    fetchPrice();
    fetchParticipants();
  }, []);

  return (
    <div className="flex flex-col ml-10 h-auto  md:pl-5 md:pt-2">
      <div className="flex flex-col md:flex-row md:justify-between gap-10">
        <Statistique variant="long" title="Récompense" style="pb-4">
          <div className="flex md:gap-4 md:justify-center">
            <p className="text-sm text-gray-500">
              Nombre total de récompense:
              <span className=" text-gray-500 px-1">{totalPrice}</span>
            </p>

            <p className="text-sm text-gray-500">
              Récompense non remis:
              <span className="text-gray-500 px-1">{notReceive}</span>
            </p>

            <p className="text-sm text-gray-500">
              Récompense remis:
              <span className="text-gray-500 px-1">{receive}</span>
            </p>
          </div>
        </Statistique>

        <Statistique variant="short" title="Ticket" style="">

          
            <p className="text-sm text-gray-500 text-center">
              Nombre total de ticket:
              <span className="text-gray-500 px-1">{totalTickets} / 500000</span>
            </p>
          

        </Statistique>
      </div>
    </div>
  );
}
