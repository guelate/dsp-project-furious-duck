import { prizes } from "@/lib/constant/constant";
import React, { useEffect, useState } from "react";
interface PopupProps {
  stoppedNumber: number | null; 
  onClose: () => void; 
}


export default function Popup({ stoppedNumber, onClose }: PopupProps) {

  const [gain, setGain] = useState<string | null>(null); 


  useEffect(() => {
    // Vérifie si stoppedNumber est valide et met à jour le gain
    if (stoppedNumber !== null && stoppedNumber > 0 && stoppedNumber <= prizes.length) {
      setGain(prizes[stoppedNumber - 1].prize_name);
    } else {
      setGain("Aucun gain disponible"); 
    }
  }, [stoppedNumber]);

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
      <div className="bg-white p-6 rounded .rounded-lg shadow-lg text-center">
        <p className="text-lg font-semibold mb-4">
          Bravo ! vous remportez le gain {stoppedNumber}:  {gain}
        </p>
        <button
          onClick={onClose}
          className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded"
        >
          Valider
        </button>
      </div>
    </div>
  );
}



