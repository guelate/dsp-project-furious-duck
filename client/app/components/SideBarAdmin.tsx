import Image from "next/image";
import logo from "../../public/logo.png";

import { Award,Newspaper, Send, Ticket, Users, Home, BookText } from "lucide-react";
import SidebarLinkAdmin from "./SidebarLinkAdmin";
import { Avatar } from "./ui/avatar";

export default function SidebarAdmin() {
  return (
    <div className="fixed top-0 left-0 z-50 w-60 h-screen bg-[#445d4d] hidden lg:block">
      <div className="flex items-center justify-start h-16 w-[calc(100%-2rem)] mx-4 gap-4">
        <div className="text-xl text-gray-200">Thé Tip Top</div>
        <Avatar className="border border-gray-300">
          <Image
            src={logo}
            alt="Picture of the author"
            width={90}
            height={90}
          />
        </Avatar>
      </div>

      <hr className="h-px border-t-0 bg-transparent bg-gradient-to-r from-transparent via-neutral-500 to-transparent opacity-25" />

      <div className="h-[calc(100vh-65px)] overflow-y-auto ">
        <div className="flex flex-col gap-5 md:h-3/4 py-12">
        <SidebarLinkAdmin href="/admin/">
           <Home />

            <div className="pr-10">
              <h1>Accueil admin</h1>
            </div>
          </SidebarLinkAdmin>
          <SidebarLinkAdmin href="/admin/documentation">
            <BookText />

            <div className="pr-10">
              <h1>Documentation</h1>
            </div>
          </SidebarLinkAdmin>
          <SidebarLinkAdmin href="/admin/users">
            <Users />

            <div className="pr-2">
              <h1>Gestion des utilisateurs</h1>
            </div>
          </SidebarLinkAdmin>
          <SidebarLinkAdmin href="/admin/tickets">
            <Ticket />

            <div className="pr-4">
              <h1>Gestion des tickets</h1>
            </div>
          </SidebarLinkAdmin>

          <SidebarLinkAdmin href="/admin/awards">
            <Award />

            <div className="pr-10">
              <h1>Gestion des prix</h1>
            </div>
          </SidebarLinkAdmin>

          <SidebarLinkAdmin href="/admin/articles">
            <Newspaper />

            <div className="pr-4">
              <h1>Gestion des articles</h1>
            </div>
          </SidebarLinkAdmin>

          <SidebarLinkAdmin href="/admin/campaingns">
            <Send />

            <div>
              <h1>Campagne de courriel</h1>
            </div>
          </SidebarLinkAdmin>
        </div>
      </div>
        </div>


  );
}
