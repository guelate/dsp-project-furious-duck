import { Card } from "./ui/card";

type Variant = "long" | "short";

interface StatistiqueProps {
  variant: Variant;
  title: string;
  children: any;
  style: any;
}

export default function Statistique({
  variant,
  title,
  children,
  style,
}: StatistiqueProps) {
  const containerStyle =
    variant === "long"
      ? `md:w-7/12 border border-gray-400 shadow hover:shadow-lg hover:border-gray-500 rounded .rounded-lg  ${style}`
      : `md:w-4/12 border border-gray-400 shadow hover:shadow-lg hover:border-gray-500 rounded .rounded-lg ${style}`;

  return (
    <Card className={containerStyle}>
      <h1 className="text-center md:text-3xl p-5 text-[#445d4d]">{title}</h1>
      {children}
    </Card>
  );
}
