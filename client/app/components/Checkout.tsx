'use client'

import React, { useState } from 'react';
import { useSession } from 'next-auth/react';

export default function Checkout() {
  const [amount, setAmount] = useState(0);
  const [ticketCode, setTicketCode] = useState('');
  const { data: session } = useSession();

  const handlePayment = async () => {
    if (!session?.user?.id) {
      alert("Utilisateur non connecté");
      return;
    }

    try {
      const response = await fetch('/api/Paiment-Caisse', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ amount, userId: session.user.id })
      });

      const data = await response.json();

      if (data.ticketCode) {
        setTicketCode(data.ticketCode);
      }
    } catch (error) {
      console.error("Erreur lors du paiement:", error);
    }
  };

  return (
    <div>
      <h2>Caisse</h2>
      <input 
        type="number" 
        value={amount} 
        onChange={(e) => setAmount(Number(e.target.value))}
        placeholder="Montant"
      />
      <button onClick={handlePayment}>Payer</button>
      {ticketCode && (
        <div>
          <h3>Félicitations !</h3>
          <p>Voici votre code ticket : {ticketCode}</p>
        </div>
      )}
    </div>
  );
}
