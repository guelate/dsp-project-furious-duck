import React from "react";
import Link from "next/link";
import Image from "next/image";
import { Card } from "./ui/card";
import { Badge } from "./ui/badge";

type BlogCardProps = {
  title: string;
  accroche: string;
  slug: string;
  image: any;
  createdAt: string;
  source: string;
};

export default function BlogCard({
  title,
  accroche,
  slug,
  image,
  createdAt,
  source,
}: BlogCardProps) {
  return (
    <Card className="border rounded .rounded-lg shadow-lg p-6 overflow-hidden w-4/5 bg-white">
      <div className="relative w-full h-48 mb-4 rounded .rounded-lg border border-gray-500">
        <Image
          src={image}
          width={50}
          height={50}
          alt={title}
          className="absolute inset-0 w-full h-full object-cover rounded-md"
        />
      </div>
      <h2 className="text-2xl font-bold text-[#445d4d]">{title}</h2>
      <p className="text-gray-500 mb-4">{accroche}</p>
      <div className="flex flex-col gap-2">
      <p className="text-xs text-gray-400">
        Créé le : {new Date(createdAt).toLocaleDateString()}
      </p>
      <div className="flex items-center">
        <p className="mr-2 text-[#445d4d]">Source : </p>
        <Badge className="secondary text-sx text-white bg-green-300 hover:bg-green-500">{source}</Badge>
      </div>
    
      <Link
        href={`/article/${slug}`}
        className="text-gray-300 hover:underline text-sm"
      >
        Lire la suite
      </Link>

      </div>
    </Card>
  );
}
