/* eslint-disable react/no-unescaped-entities */

import "./WelcomSection.css";

import { Button } from "./ui/button";
import type { User } from "next-auth";

type Props = {
  user: User,
  pagetye: string,
}

export default function WelcomSection() {

  const scrollToWheelSection = () => {
    const section = document.getElementById("wheel-section");
    if (section) {
      section.scrollIntoView({ behavior: "smooth" }); // Défilement fluide vers la section Wheel
    }
  };

  return (
    <div className="flex md:p-10 justify-end mt-20 md:w-3/4 border shadow rounded .rounded-lg photo">
      <div className="flex flex-col md:w-3/5 w md:gap-10 w-3/5 mr-2">
        <h1 className="styleImporter text-start">
          Bienvenue dans notre jeu-concours !
        </h1>
        <p className="text-[#30593E] md:text-xl text-sm">
          Pour célébrer l'ouverture de notre 10ème boutique à Nice, Thé Tip Top
          vous invite à découvrir nos thés spéciaux et participer à notre grand
          jeu-concours ! Saisissez l'occasion de gagner de superbes lots, avec
          en prime un tirage au sort pour remporter une année de thé gratuite.
          Ne manquez pas cette opportunité unique !
        </p>
        <div className="flex justify-end p-3 md:px-10">
          <Button className="text-[#30593E] w-3/6 border border-gray-300 hover:shadow-md hover:bg-gray-300 hover:border-gray-400 rounded .rounded-lg bg-gray-300" onClick={scrollToWheelSection}>
            Participez 🚀
          </Button>
        </div>
      </div>
    </div>
  );
}
