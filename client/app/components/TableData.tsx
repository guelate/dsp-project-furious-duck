"use client";

import { useState, useEffect, useMemo, useImperativeHandle, forwardRef } from "react";
import TableContent from "./TableContent";
import { TableProps } from "@/lib/type/type";
import { TICKET_TABLE_VARIANT, TOTAL_VALUES_PER_PAGE, USER_TABLE_VARIANT, PRICE_TABLE_VARIANT, GAINT_HISTORY_VARIANT, GLOBAL_GAIN_HISTORY_VARIANT } from "@/lib/constant/constant";
import { Input } from "@/components/ui/input";
import { Button } from "./ui/button";
import { debounce } from "@/lib/utilities/debounce";

interface TableDataProps {
  variantProps: string;
  checkbox: boolean;
  onSelectionChange: (ids: (string | number)[]) => void;
  selectedIds: (string | number)[];
}

// Utilisation de forwardRef pour permettre au parent de déclencher le rechargement
const TableData = forwardRef(function TableData({ variantProps, checkbox, onSelectionChange, selectedIds }: TableDataProps, ref) {
  const [data, setData] = useState<TableProps[]>([]);
  const [totalItems, setTotalItems] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const [currentPageNumber, setCurrentPageNumber] = useState(1);
  const [searchTerm, setSearchTerm] = useState("");
  const [placeholder, setPlaceholder] = useState("");

  // Fonction de fetch des données
  const fetchData = async (searchTerm: string = "") => {
    setIsLoading(true);
    setError(null);

    try {
      let url = "";

      if (variantProps === USER_TABLE_VARIANT) {
        url = `/api/users?page=${currentPageNumber}&size=${TOTAL_VALUES_PER_PAGE}&searchTerm=${searchTerm}`;
        setPlaceholder("Rechercher par prénom");
      } else if (variantProps === TICKET_TABLE_VARIANT) {
        url = `/api/tickets?page=${currentPageNumber}&size=${TOTAL_VALUES_PER_PAGE}&searchTerm=${searchTerm}`;
        setPlaceholder("Rechercher par code");
      } else if (variantProps === PRICE_TABLE_VARIANT || variantProps === GLOBAL_GAIN_HISTORY_VARIANT) {
        url = `/api/prizeClaim?page=${currentPageNumber}&size=${TOTAL_VALUES_PER_PAGE}&searchTerm=${searchTerm}`;
        setPlaceholder("Rechercher par code de ticket");
      } else if (variantProps === GAINT_HISTORY_VARIANT) {
        url = `/api/getUserGains?page=${currentPageNumber}&size=${TOTAL_VALUES_PER_PAGE}&searchTerm=${searchTerm}`;
        setPlaceholder("Rechercher par code de ticket");
      }

      const response = await fetch(url);

      if (!response.ok) {
        throw new Error("Erreur lors du chargement des données.");
      }

      const result = await response.json();
      setData(result.data);
      setTotalItems(result.totalItems);
    } catch (error) {
      console.error("Erreur lors du fetch :", error);
      setError("Erreur lors du chargement des données.");
    } finally {
      setIsLoading(false);
    }
  };

  // Utiliser useImperativeHandle pour exposer la méthode de fetch au parent
  useImperativeHandle(ref, () => ({
    refetch() {
      fetchData(searchTerm);  // Rechargement des données
    }
  }));

  const debouncedFetchData = useMemo(() => debounce(fetchData, 500), [currentPageNumber, variantProps]);

  useEffect(() => {
    debouncedFetchData(searchTerm);
  }, [searchTerm]);

  useEffect(() => {
    fetchData(searchTerm);
  }, [currentPageNumber, variantProps]);

  const handleCheckboxChange = (id: string | number) => {
    const updatedSelected = selectedIds.includes(id)
      ? selectedIds.filter((e) => e !== id)
      : [...selectedIds, id];

    onSelectionChange(updatedSelected);
  };

  if (isLoading) return <div>Chargement...</div>;
  if (error) return <div>Erreur: {error}</div>;

  const totalPages = Math.ceil(totalItems / TOTAL_VALUES_PER_PAGE);

  return (
    <div className="flex flex-col w-3/4 md:w-full">
      <div className="flex md:flex-col flex-col gap-10">
        {variantProps && (
          <div className="w-3/4 md:w-1/2">
            <Input
              placeholder={placeholder}
              value={searchTerm}
              onChange={(e) => setSearchTerm(e.target.value)}
              className="md:w-3/6 border border-gray-400"
            />
          </div>
        )}
        <TableContent
          dataToDisplay={data}
          variantProps={variantProps}
          checkboxState={selectedIds}
          onCheckboxChange={handleCheckboxChange}
  
        />
      </div>
  
      <div className="flex flex-col md:flex-row gap-2 border justify-between p-4 mt-4">
        <Button
          onClick={() =>
            setCurrentPageNumber((prev) => Math.max(prev - 1, 1))
          }
          className="w-full md:w-auto border"
        >
          Précédent
        </Button>
        <select
          name="page-number"
          onChange={(e) => setCurrentPageNumber(parseInt(e.target.value, 10))}
          value={currentPageNumber}
          className="w-full md:w-auto border px-2 py-1 focus:outline-none"
        >
          {Array.from({ length: totalPages }, (_, index) => index + 1).map(
            (val) => (
              <option key={val} value={val}>
                {val}
              </option>
            )
          )}
        </select>
        <Button
          onClick={() =>
            setCurrentPageNumber((prev) => Math.min(prev + 1, totalPages))
          }
          className="w-full md:w-auto border"
        >
          Suivant
        </Button>
      </div>
    </div>
  );
  
});

export default TableData;















