'use client';

import { useState, useEffect } from 'react';
import BackgroundCaptcha from './BackgroundCaptcha';

interface ClientWrapperProps {
  children: React.ReactNode;
}

export default function ClientWrapper({ children }: ClientWrapperProps) {
  const [isVerified, setIsVerified] = useState(false);

  const handleCaptchaVerify = async (token: string | null) => {
    if (!token) return;

    try {
      const response = await fetch('/api/verify-captcha', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ token }),
      });

      const data = await response.json();

      if (data.success) {
        setIsVerified(true);
        console.log('Captcha verified successfully');
      } else {
        console.error('Captcha verification failed');
      }
    } catch (error) {
      console.error('Error verifying captcha:', error);
    }
  };

  useEffect(() => {
    if (isVerified) {
      // Add any logic you want to execute when verified
    }
  }, [isVerified]);

  return (
    <>
      {children}
      <BackgroundCaptcha
        siteKey="6LdyDVYqAAAAAKsyle6p8DrcPpJSK8LQFC-wENN_"
        onVerify={handleCaptchaVerify}
      />
    </>
  );
}