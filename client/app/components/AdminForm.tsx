"use client"; // Assurez-vous que le composant est côté client

import { useRouter } from 'next/navigation'; // Import depuis next/navigation
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { AdminSchema, SignInSchema } from "@/lib/zod/SignInSchema";
import { Input } from "@/components/ui/input";
import { Button } from "./ui/button";
import { Form, FormField, FormItem, FormControl, FormMessage } from "@/components/ui/form";
import { z } from "zod";
import { signIn } from "next-auth/react"; // Importer signIn

export default function AdminForm() {
  const router = useRouter(); // Initialiser useRouter depuis next/navigation

  const formIn = useForm<z.infer<typeof AdminSchema>>({
    resolver: zodResolver(AdminSchema),
    defaultValues: {
      email: "",
      password: "",
    },
  });

  // Gérer la soumission du formulaire
  async function onSubmit(values: z.infer<typeof AdminSchema>) {
    try {
      const result = await signIn("credentials", {
        redirect: false, // Empêche la redirection automatique, on va la gérer nous-mêmes
        email: values.email,
        password: values.password,
        provider: "admin",
      });

      if (result?.error) {
        // Gérer l'erreur (vous pouvez afficher un message d'erreur ici)
        console.error(result.error);
        // Par exemple : afficher une alerte ou un message d'erreur dans l'UI
      } else {
        // Rediriger vers la page admin
        router.push("/admin");
      }
    } catch (error) {
      console.error("L'authentification a échoué", error);
    }
  }

  return (
    <div className="">
      <div className="flex flex-col gap-8 text-center text-lg">
        <h2 className="">Connexion</h2>
        <p>Lire le README pour les accès: <a href="https://gitlab.com/guelate/dsp-project-furious-duck" className='text-gray-400 underline'>ici</a></p>
        {/* <p>Accès admin: <a href="/admin" style={{ color: 'blue', textDecoration: 'underline' }}>ici</a></p> */}


        <Form {...formIn}>
          <form onSubmit={formIn.handleSubmit(onSubmit)}>
            <div className="flex flex-col justify-center gap-5">
              <FormField
                control={formIn.control}
                name="email"
                render={({ field }) => (
                  <FormItem>
                    <FormControl>
                      <Input placeholder="Identifiant" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={formIn.control}
                name="password"
                render={({ field }) => (
                  <FormItem>
                    <FormControl>
                      <Input type="password" placeholder="Mot de passe" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
            <div className="flex justify-center items-center p-4">
              <Button type="submit" variant="secondary" className="border rounded bg-slate-200 hover:bg-slate-300 cursor-pointer duration-500">
                Valider
              </Button>
            </div>
          </form>
        </Form>
      </div>
    </div>
  );
}
