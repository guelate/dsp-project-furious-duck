import { TableContentProps } from "@/lib/type/type";
import { Table, TableBody, TableHead, TableHeader, TableRow, } from "./ui/table";
import { useState } from "react";
import {
  TICKET_TABLE_VARIANT,
  USER_TABLE_VARIANT,
  PRICE_TABLE_VARIANT,
  GAINT_HISTORY_VARIANT,
  GLOBAL_GAIN_HISTORY_VARIANT
} from "@/lib/constant/constant";
import { renderRows } from "@/lib/utilities/renderRows";

const getHeaders = (variant: string) => {
  switch (variant) {
    case USER_TABLE_VARIANT:
      return ["Prénom", "Email", "Genre", "Statut", "Sélection"];
    case TICKET_TABLE_VARIANT:
      return ["Prénom", "Code", "ID Gain","Statut"];
    case PRICE_TABLE_VARIANT:
      return ["ID du prix", "Code ticket", "Utilisateur", "Récompense", "Statut", "Sélection"];
    case GAINT_HISTORY_VARIANT:
      return ["ID du prix", "Code ticket", "Utilisateur", "Récompense", "Statut"];
    case GLOBAL_GAIN_HISTORY_VARIANT:
      return ["ID du prix", "Code ticket", "Utilisateur", "Récompense", "Statut"];
    default:
      return [];
  }
};

export default function TableContent({
  dataToDisplay,
  variantProps,
  checkboxState,
  onCheckboxChange,
}: TableContentProps & { checkboxState?: (string | number)[], onCheckboxChange?: (email: string) => void }) {
  const headers = getHeaders(variantProps);

  return (
    <Table className="w-full">
      <TableHeader>
        <TableRow>
          {headers.map((header) => (
            <TableHead key={header} className="px-4 py-2 text-center">
              {header}
            </TableHead>
          ))}
        </TableRow>
      </TableHeader>
      <TableBody>
        {renderRows({
          variantProps,
          dataToDisplay,
          checkboxState,
          onCheckboxChange,
          headers,
        })}
      </TableBody>
    </Table>
  );
}
