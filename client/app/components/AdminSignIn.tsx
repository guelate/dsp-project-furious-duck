import AdminForm from "./AdminForm";
import { Card } from "./ui/card";

export default function AdminSignIn() {
  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 backdrop-blur-sm z-10">
      <Card className="bg-white p-8 shadow-lg w-full max-w-md pt-10 rounded .rounded-lg">
        <AdminForm />
      </Card>
    </div>
  );
}
