"use client";

import { Button } from "./ui/button";
import { Input } from "./ui/input";
import { Label } from "./ui/label";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "@/components/ui/form";
import { useState } from "react";
import { useSession } from "next-auth/react";
import { prizes } from "@/lib/constant/constant";

// Définition du schéma de validation du code avec zod
const CodeSchema = z.object({
  code: z.string().length(10, { message: "Le code doit avoir 10 caractères" }),
});

interface WheelTextProps {
  triggerSpin: (index: number) => void; // Propriété pour déclencher la rotation de la roue avec l'index du gain
}

export default function WheelText({ triggerSpin }: WheelTextProps) {
  // États pour gérer l'état du ticket et les messages d'erreur
  const [isValidCode, setIsValidCode] = useState<boolean | null>(null);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [ticketNotFound, setTicketNotFound] = useState<string | null>(null);

  // Utilisation de useSession pour récupérer la session de l'utilisateur
  const { data: session } = useSession();
  const user = session?.user || null;

  // Initialisation du formulaire avec react-hook-form et zod
  const formCode = useForm<z.infer<typeof CodeSchema>>({
    resolver: zodResolver(CodeSchema),
    defaultValues: {
      code: "",
    },
  });

  // Fonction appelée lors de la soumission du code
  async function onSubmitCode(values: z.infer<typeof CodeSchema>) {

    
    setErrorMessage(null);

    // Conditions à vérifier avant de jouer
    if (!user) {
      setTicketNotFound(
        "Veuillez vous connecter pour participer au jeu concours."
      );
      return;
    }

    if (user.role === "admin") {
      setTicketNotFound("Un admin ne peut pas jouer");
      return;
    }
    console.log("sortie de condition avant jeu");
    try {
      console.log("entrer dans le jeuxxxxxxxxxxxxxxxxxx");
      // Requête API pour vérifier le ticket
      const response = await fetch("/api/verifyTicket", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      });

      if (response.ok) {
        const data = await response.json();
        console.log("Le ticket est :", data);

        const gain = data.ticket.prize.prize_name;
        console.log("Le gain est :", gain);

        // Trouver l'index correspondant au nom du gain
        const prizeIndex = prizes.findIndex(
          (prize) => prize.prize_name === gain
        );

        // Vérifier si le ticket est trouvé
        if (
          data.message ===
          "Ce ticket a déjà été utilisé et ne peut plus être réutilisé."
        ) {
          setTicketNotFound("Ce ticket a déjà été utilisé");
          setIsValidCode(false);
        } else if (prizeIndex !== -1) {
          setTicketNotFound(null);
          setIsValidCode(true);

          // Lancer la rotation de la roue avec l'index du gain
          triggerSpin(prizeIndex + 1); // Passer l'index + 1 pour correspondre à la roue
        } else {
          setTicketNotFound("Gain inconnu. Veuillez réessayer.");
        }
      } else {
        // Gérer les erreurs de réponse du serveur
        const errorData = await response.json();
        setErrorMessage(errorData.message || "Erreur serveur.");
        setTicketNotFound("Ticket non trouvé"); // Définir le message d'erreur
        setIsValidCode(false);
      }
    } catch (error) {
      // Gérer les erreurs de réseau ou autres
      console.error("Erreur: ticket introuvable ou problème réseau", error);
      setErrorMessage("Erreur réseau. Veuillez réessayer plus tard.");
      setTicketNotFound("Ticket non ntrouvé");
      setIsValidCode(false);
    }
  }

  // Gestion des erreurs du formulaire
  const onError = (errors: any) => {
    console.log("Form errors: ", errors);
  };

  return (
    <div className="md:flex-col md:gap-3 md:text-end">
      <h2 className="text-lg md:text-4xl text-left text-[#445d4d]">
        Ne manquez pas votre chance !
      </h2>

      <div className="flex mt-4 flex-row md:gap-3">
        <h2 className="text-left pr-10 md:px-0 text-[#445d4d]">
          Faites tourner la roue !!
        </h2>
        <p className="md:items-end">
          <a href="/jeuConcours" className="text-[#445d4d] bg-yellow-200">
            Voir les récompenses! 👉
          </a>
        </p>
      </div>

      <div className="flex md:flex-col mt-6 md:text-start md:gap-3">
        <Form {...formCode}>
          <form onSubmit={formCode.handleSubmit(onSubmitCode, onError)}>
            <div>
              <Label htmlFor="code" className="text-gray-600">
                Entrer votre code ticket
              </Label>
              <div className="flex md:flex-col md:gap-6 md:justify-center mt-2">
                <FormField
                  control={formCode.control}
                  name="code"
                  render={({ field }) => (
                    <FormItem>
                      <FormControl>
                        <Input
                          type="text"
                          className="hover:shadow-sm border border-gray-400 hover:border-gray-600 md:w-2/4"
                          placeholder="Code à 10 caractères"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <Button className="text-[#30593E] w-2/6 border border-gray-300 hover:shadow-md hover:bg-gray-300 hover:border-gray-400 rounded bg-gray-300 md:ml-0 ml-5">
                  Valider
                </Button>
              </div>
            </div>
          </form>
        </Form>
      </div>

      {/* Condition avant de jouer */}
      {ticketNotFound && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 ">
          <div className="bg-white p-6 rounded-lg shadow-lg text-center">
            <p className="text-lg font-semibold mb-4">{ticketNotFound}</p>
            <button
              onClick={() => setTicketNotFound(null)}
              className="bg-red-400 hover:bg-red-600 text-white font-bold py-2 px-4 rounded"
            >
              Fermer
            </button>
          </div>
        </div>
      )}
    </div>
  );
}
