'use client'

import React, { useState } from 'react';

export default function TicketVerification() {
  const [code, setCode] = useState('');
  const [result, setResult] = useState<{ message?: string; prize?: string; error?: string } | null>(null);

  const handleVerification = async () => {
    try {
      const response = await fetch(`/api/verify-Borne?code=${code}`);
      const data = await response.json();

      setResult(data);
    } catch (error) {
      console.error("Erreur lors de la vérification:", error);
      setResult({ error: "Une erreur est survenue lors de la vérification" });
    }
  };

  return (
    <div>
      <h2>Vérification de ticket</h2>
      <input 
        type="text" 
        value={code} 
        onChange={(e) => setCode(e.target.value)}
        placeholder="Entrez le code du ticket"
      />
      <button onClick={handleVerification}>Vérifier</button>
      {result && (
        <div>
          {result.prize ? (
            <>
              <h3>Félicitations !</h3>
              <p>Vous avez gagné : {result.prize}</p>
            </>
          ) : (
            <p>{result.error || result.message}</p>
          )}
        </div>
      )}
    </div>
  );
}
