export type Sign = "signIn" | "signUp";

export interface SignFormProps {
  signType: Sign;
}

// Interface for User
export interface UserTable {
  id?: string;
  firstname?: string;
  email?: string;
  compte?: boolean;
  récompense?: string;
  is_active?: boolean;
  gender?: string;
}

// Alias for UserTable
export type UserType = UserTable;

// Interface for Ticket
export interface TicketTable {
  ticket_id?: string;
  ticket_code?: string;
  user?: any;
  valeur?: string;
  status?: string;
  prize_id?:string;
  prize_name?:string;
}

// Interface for Price
export interface PriceTable {
  claim_id?: string;
  prize_id?: string;
  user?: any;
  prize?: any;
  ticket?: any;
  status?: string;
  code?: string;
}

// Interface for Gain History
export interface GainHistoryTable {
  claim_id?: string;
  prize_id?: string;
  user?: any;
  prize?: any;
  ticket?: any;
  status?: string;
}

// Interface for Gain History
export interface GlobalGainHistoryTable {
  claim_id?: string;
  prize_id?: string;
  user?: any;
  prize?: any;
  ticket?: any;
  status?: string;
}


// Type for handling all tables
export type TableProps = UserTable | TicketTable | PriceTable | GainHistoryTable;

// Props for TableContent component
export interface TableContentProps {
  dataToDisplay: TableProps[];
  variantProps: string;
  checkbox?: string[];
}
