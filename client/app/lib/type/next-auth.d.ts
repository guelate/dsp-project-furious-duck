import { Session } from "next-auth";
import { JWT } from "next-auth/jwt";

declare module "next-auth" {
  interface User {
    id: string;
    name: string;
    lastname: string;
    email: string;
    role: string;
    provider:string;
    is_active?: boolean;
  }
  
  interface Session {
    user: User & {
      id: string; 
      name: string;
      is_active?: boolean; 
    };
  }
}


// declare module "next-auth" {

  
//     interface User {
//       id: string;
//       role: number;
//     }
//   }
  
  // declare module "next-auth/jwt" {
  //   interface JWT {
  //     id: string;
  //     role: number;
  //     provider:string;
  //   }
  // }