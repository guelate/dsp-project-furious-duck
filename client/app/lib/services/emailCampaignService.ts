// lib/services/emailCampaignService.ts
import { prisma } from "../db/prisma/prisma"; // Assurez-vous que le chemin est correct

// Fonction pour créer une nouvelle campagne d'emailing
export async function createEmailingCampaign(name: string, content: string, subject: string, targetAudience: string[]) {
  await prisma.emailingCampaign.create({
    data: {
      name,
      content,
      subject,
      target_audience: targetAudience,
    },
  });
}
