import { prisma } from "@/lib/db/prisma/prisma";

// Fonction pour enregistrer une action dans les logs
export const logActivity = async (actionType: string) => {
    try {
      console.log("Tentative d'enregistrement du log:", actionType);
      const log = await prisma.log.create({
        data: {
          action_type: actionType,
        },
      });
      console.log("Log créé avec succès:", log);
      return log;
    } catch (error) {
      console.error("Erreur lors de l'enregistrement du log:", error);
      throw new Error("Impossible d'enregistrer le log");
    }
};
  
