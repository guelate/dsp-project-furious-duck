import { prisma } from "../db/prisma/prisma";

// Decrement prize quantity
export async function decrementPrizeQuantity(prize_id: string) {
    try {
      await prisma.prize.update({
        where: { prize_id },
        data: {
          remaining_quantity: {
            decrement: 1, 
          },
        },
      });
      console.log(`Quantité de la récompense ${prize_id} mise à jour avec succès.`);
    } catch (error) {
      console.error("Erreur lors de la mise à jour de la quantité de la récompense :", error);
    }
  }
  