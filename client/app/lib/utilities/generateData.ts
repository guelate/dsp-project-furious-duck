import { PRICE_TABLE_VARIANT, TICKET_TABLE_VARIANT, USER_TABLE_VARIANT } from "../constant/constant";
import { TableProps } from "../type/type";

export const generateData = (rows: number, variant: string): TableProps[] => {
  const data: TableProps[] = [];

  for (let i = 0; i < rows; i++) {
    let item: any = {
      id: `TablePros ${i + 1}`,
    };

    switch (variant) {
      case USER_TABLE_VARIANT:
        item = {
          ...item,
          prénom: "Killian",
          email: "guelateseyo@gmail.com",
          compte: "true",
          récomponse: "Cheque 500 euros",
        };
        break;
      case TICKET_TABLE_VARIANT:
        item = {
          ...item,
          code: "1024EEDF35",
          prénom: "Killian",
          valeur: "Cheque 500euros",
          statut: "utilisé",
        };
        break;
      case PRICE_TABLE_VARIANT:
        item = {
          ...item,
          prénom: i + 1,
          récompense: i + 1,
          statut: i % 2 === 0,
          code: "wded",
        };
        break;
      default: // GainHistoryTable
        item = {
          ...item,
          prénom: i + 1,
          date: i + 1,
          récompense: "we",
        };
        break;
    }

    data.push(item);
  }

  console.log(data);
  return data;
};


