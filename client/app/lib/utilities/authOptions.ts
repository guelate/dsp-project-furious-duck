import NextAuth, { AuthOptions } from "next-auth";
import { JWT } from "next-auth/jwt"; 
import CredentialsProvider from "next-auth/providers/credentials";
import GoogleProvider from "next-auth/providers/google";
import FacebookProvider from "next-auth/providers/facebook";
import { prisma } from "@/lib/db/prisma/prisma"; 
import bcrypt from "bcryptjs";


export const authOptions = {
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        email: { label: "Email", type: "email", placeholder: "Email" },
        firstname: { label: "Prénom", type: "text", placeholder: "Prénom" },
        password: { label: "Mot de passe", type: "password", placeholder: "Mot de passe" },
      },
      async authorize(credentials, req) {

        console.log("entrer dans le credentials")

      
            console.log("Requête reçue dans authorize:", req.body);
            const provider = req.body?.provider || "user";
            console.log("Provider utilisé:", provider);
    
            if (provider === "admin") {
              if (!credentials?.email || !credentials.password) {
                throw new Error("Identifiants administrateurs nécessaires pour la connexion");
              }
    
              const admin = await prisma.admin.findFirst({
                where: { email: credentials.email },
              });
    
              if (!admin) {
                throw new Error("Aucun administrateur trouvé avec cet email");
              }
    
              const isValidPassword = await bcrypt.compare(credentials.password, admin.password_hash);
              if (!isValidPassword) {
                throw new Error("Mot de passe incorrect pour l'administrateur");
              }
    
              console.log("Admin connecté:", admin);
              return {
                id: admin.admin_id,
                name: admin.firstname,
                lastname: admin.lastname,
                email: admin.email,
                role: admin.role,
                is_active: true,
                provider: "admin",
              };
            }

        if (!credentials?.firstname || !credentials.password) {
          throw new Error("Identifiants nécessaires pour la connexion des utilisateurs");
        }

        const user = await prisma.user.findFirst({
          where: { firstname: credentials.firstname },
        });

        if (!user) {
          throw new Error("Aucun utilisateur trouvé avec ce prénom");
        }

        const isValidPassword = await bcrypt.compare(credentials.password, user.password_hash);
        if (!isValidPassword) {
          throw new Error("Mot de passe incorrect");
        }

        return {
          id: user.user_id,
          name: user.firstname,
          lastname: user.lastname,
          email: user.email,
          role: user.role,
          is_active: user.is_active,
          provider: "user", 
        };
      },
    }),
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID || "",
      clientSecret: process.env.GOOGLE_CLIENT_SECRET || "",

      profile: async (profile) => {
        // console.log("Google profile:", profile);
 
        // Vérifier si l'utilisateur existe déjà
        const existingUser = await prisma.user.findUnique({
          where: { email: profile.email },
        });
 
        // Si l'utilisateur n'existe pas, on le crée
        const newUser = existingUser || await prisma.user.create({
            data: {
                firstname: profile?.given_name,
                lastname: profile?.family_name || "",
                email: profile?.email,
                password_hash: "", // Laisser vide pour les providers OAuth
                auth_provider: "google",
                date_of_birth: new Date(), // Mettre une valeur par défaut
                gender: "", // À définir si possible
                address: "",
                city: "",
                postal_code: "",
                phone_number: "",
                role: "user",
                is_active: true,
            },
        });

          
        if (existingUser) { console.log("trouvé !!!!!!")}
      

        return {
          id: newUser.user_id,
          name: `${newUser.firstname} ${newUser.lastname}`,
          email: newUser.email,
          image: profile.picture || "",
          lastname: newUser.lastname || "",
          role: newUser.role || "",
          provider: newUser.auth_provider || "",
        };
      },


     
      
    }),
    FacebookProvider({
      clientId: process.env.FACEBOOK_CLIENT_ID || "",
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET || "",
    }),
  ],
  callbacks: {
    // Ajout des types pour le JWT token
    async jwt({ token, user, account }: { token: JWT, user?: any, account?: any, profile?: any }) {

        // Ajout d'informations utilisateur au token lors de la connexion
      if (account && user) {
        token.id = user.id;
        token.name = user.name;
        token.email = user.email;
        token.provider = account.provider;
        token.is_active = user.is_active;
        token.role = user.role;

        if(account.provider === "google" || account.provider === "facebook"){
          token.role = "user";
          token.is_active = true;
        }
 
      }
      console.log("le token:",token)
      return token;
    },
    // Ajout des types pour la session
    async session({ session, token }: { session: any, token: JWT }) {
        // Ajout du rôle dans la session côté client
      console.log("le token dans session est",token)
      if (token) {
        session.user.id = token.id;
        session.user.role = token.role;  
        session.user.provider = token.provider;
        session.user.is_active = token.is_active;
      }
      return session;
    },
  },
  
};
console.log("sortie du authOptions")


