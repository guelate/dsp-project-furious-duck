import { TableCell } from "@/components/ui/table";
import {
  GAINT_HISTORY_VARIANT,
  PRICE_TABLE_VARIANT,
  TICKET_TABLE_VARIANT,
  USER_TABLE_VARIANT,
  GLOBAL_GAIN_HISTORY_VARIANT
} from "../constant/constant";
import {
  GainHistoryTable,
  PriceTable,
  TableContentProps,
  TicketTable,
  UserTable,
  GlobalGainHistoryTable,
} from "../type/type";
import { Badge } from "@/components/ui/badge";

export const renderRows = ({
  variantProps,
  dataToDisplay,
  checkboxState,
  onCheckboxChange,
  headers,
}: TableContentProps & {
  checkboxState?: (string | number)[];
  onCheckboxChange?: (email: string) => void;
  headers: string[];
}) => {
  switch (variantProps) {
    case USER_TABLE_VARIANT:
      return (dataToDisplay as UserTable[]).map((obj) => {
        const email = obj.email ?? ""; // Assurez-vous que email n'est jamais undefined
        const isChecked = checkboxState?.includes(email) ?? false;

        return (
          <tr key={email} className="text-center">
            <TableCell>{obj.firstname}</TableCell>
            <TableCell>{email}</TableCell>
            <TableCell>{obj.gender}</TableCell>
            <TableCell>
              <Badge
                className={`rounded hover:bg-green-400 ${
                  obj.is_active ? "bg-green-200" : "bg-red-400"
                }`}
              >
                {obj.is_active ? "Compte activé" : "Compte bloqué"}
              </Badge>
            </TableCell>
            <TableCell>
              <input
                type="checkbox"
                id={`checkbox-${email}`} // ID unique
                checked={isChecked}
                onChange={() => {
                  console.log("Checkbox clicked:", email);
                  onCheckboxChange && onCheckboxChange(email);
                }}
              />
            </TableCell>
          </tr>
        );
      });
    case TICKET_TABLE_VARIANT:
      return (dataToDisplay as TicketTable[]).map((obj) => (
        <tr key={obj.ticket_id} className="text-center">
          <TableCell>
              {obj.status=="non utilisé" ? "Pas attribué" : obj.user.firstname}  
          </TableCell>
          <TableCell>{obj.ticket_code}</TableCell>
          <TableCell>{obj.prize_id}</TableCell>
          <TableCell>
              <Badge
                className={`rounded hover:bg-green-400 ${
                  obj.status=="non utilisé" ? "bg-red-400" : "bg-green-200"
                }`}
              >
                {obj.status=="non utilisé" ? "Non utilisé" : "utilisé"}
              </Badge>
            </TableCell>
          
        </tr>
      ));
      case PRICE_TABLE_VARIANT:
      return (dataToDisplay as PriceTable[]).map((obj) => {
        const claimId = obj.claim_id ?? ""; // Utilisation de claim_id
        const isChecked = checkboxState?.includes(claimId) ?? false; // Vérifie si le claim_id est sélectionné

      return (
        <tr key={claimId} className="text-center">
          <TableCell>{obj.prize_id}</TableCell> {/* ID du prix */}
          <TableCell>{obj.ticket.ticket_code}</TableCell>
          <TableCell>{obj.user.email}</TableCell> {/* Email de l'utilisateur */}
          <TableCell>{obj.prize.prize_name}</TableCell> {/* Nom du prix */}
          <TableCell>
            <Badge
              className={`rounded hover:bg-green-400 ${
                obj.status === "récupéré" ? "bg-green-200 hover:bg-green-300" : "bg-red-400 hover:bg-red-500"
              }`}
            >
              {obj.status === "récupéré" ? "Récupéré" : "Non récupéré"}
            </Badge>
          </TableCell>
          <TableCell>
            <input
              type="checkbox"
              id={`checkbox-${claimId}`} // ID unique basé sur claim_id
              checked={isChecked} // Contrôle la case à cocher en fonction du claim_id
              onChange={() => {
                console.log("Checkbox clicked:", claimId);
                onCheckboxChange && onCheckboxChange(claimId); // Appelle la fonction onCheckboxChange avec le claim_id
              }}
            />
          </TableCell>
        </tr>
      );
    });
    case GAINT_HISTORY_VARIANT:
      return (dataToDisplay as GainHistoryTable[]).map((obj) => (
        <tr key={obj.claim_id} className="text-center">
          <TableCell>{obj.prize_id}</TableCell>
          <TableCell>{obj.ticket.ticket_code}</TableCell>
          <TableCell>{obj.user.email}</TableCell>
          <TableCell>{obj.prize.prize_name}</TableCell>
          <TableCell>
            <Badge
              className={`rounded hover:bg-green-400 ${
                obj.status === "récupéré" ? "bg-green-200 hover:bg-green-300" : "bg-red-400 hover:bg-red-500"
              }`}
            >
              {obj.status === "récupéré" ? "Récupéré" : "Non récupéré"}
            </Badge>
          </TableCell>
        </tr>
      ));
      case GLOBAL_GAIN_HISTORY_VARIANT:
      return (dataToDisplay as GlobalGainHistoryTable[]).map((obj) => (
        <tr key={obj.claim_id} className="text-center">
          <TableCell>{obj.prize_id}</TableCell>
          <TableCell>{obj.ticket.ticket_code}</TableCell>
          <TableCell>{obj.user.email}</TableCell>
          <TableCell>{obj.prize.prize_name}</TableCell>
          <TableCell>
            <Badge
              className={`rounded hover:bg-green-400 ${
                obj.status === "récupéré" ? "bg-green-200 hover:bg-green-300" : "bg-red-400 hover:bg-red-500" 
              }`}
            >
              {obj.status === "récupéré" ? "Récupéré" : "Non récupéré"}
            </Badge>
          </TableCell>
        </tr>
      ));
    default:
      return (
        <tr>
          <TableCell colSpan={headers.length} className="text-center">
            Tableau non défini
          </TableCell>
        </tr>
      );
  }
};
