import { prisma } from "../db/prisma/prisma";

type prizeClaim = "non récupéré" | "récupéré";

interface createPrizeClaimProps{
  user_id:string;
  ticket_id:string;
  prize_id:string;
  status?: prizeClaim;
  claim_date?:any;

}
// Create prize claim
export async function createPrizeClaim({user_id,ticket_id,prize_id,status,claim_date}:createPrizeClaimProps) {
    try {
      await prisma.prizeClaim.create({
        data: {
          user_id,    
          ticket_id, 
          prize_id,  
          status: "non récupéré", 
          claim_date: new Date(), 
        },
      });
      console.log(`Nouvelle entrée dans la table prize_claims pour le ticket ${ticket_id} et la récompense ${prize_id}.`);
    } catch (error) {
      console.error("Erreur lors de la création de l'entrée dans prize_claims :", error);
    }
  }
  