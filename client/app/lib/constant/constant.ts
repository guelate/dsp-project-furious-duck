export const CARACTERE_10 = "Le code du ticket doit comporter exactement 10 caractères."
export const CARACTERE_8 = "Le mot de passe doit contenir au moins 8 caractères."
export const CARACTERE_2 = "Le prénom doit contenir au moins 2 caractères."
export const CARACTERE_5 = "L'adresse doit contenir au moins 5 caractères."
export const CARACTERE_255 = "Le nom ne doit pas dépasser 255 caractères."
export const IDENTIFIANT_ERROR = "L'identifiant doit contenir entre 5 et 15 caractères"
export const CARACTERE_100 = "Le nom de la ville ne doit pas dépasser 100 caractères."
export const PASSWORD_ERROR = "le mot de passe doit contenir entre 5 et 15 caractères"
export const PASSWORD_NOT_EQUAL = "Les mots de passe ne correspondent pas."
export const FIRSTNAME_ERROR = "Le prénom doit contenir entre 5 et 15 caractères"
export const LASTNAME_ERROR = "le nom doit contenir entre 5 et 15 caractères"
export const GENDER_ERROR = "Le genre est invalide."
export const ADRESS_ERROR = "L'adresse doit contenir entre 5 et 15 caractères"
export const EMAIL_ERROR = "L'adresse e-mail est invalide."
export const CITY_ERROR = "La ville est incorrecte"
export const POSTAL_CODE_ERROR = "Le code postal doit être un nombre à 5 chiffres."
export const PHONE_NUMBER_ERROR = "Le numéro de téléphone doit contenir entre 10 et 15 chiffres."
export const TICKET_ERROR = "Le code ticket contenir 10 charactères"
export const AGE_OF_BIRTH = "La date de naissance doit être au format AAAA-MM-JJ."
export const TOTAL_VALUES_PER_PAGE = 10;
export const USER_TABLE_VARIANT = "userTable"
export const TICKET_TABLE_VARIANT = "ticketTable"
export const PRICE_TABLE_VARIANT = "priceTable"
export const GAINT_HISTORY_VARIANT = "gaintHistoryTable"
export const GLOBAL_GAIN_HISTORY_VARIANT = "globalGainHistoryTable"


export const prizes = [
    { prize_name: "Infuseur à thé", initial_quantity: 300000, percentage: 60 },
    { prize_name: "Boîte de 100g de thé détox ou infusion", initial_quantity: 100000, percentage: 20 },
    { prize_name: "Boîte de 100g de thé signature", initial_quantity: 50000, percentage: 10 },
    { prize_name: "Coffret découverte 39€", initial_quantity: 30000, percentage: 6 },
    { prize_name: 'Coffret découverte 69€', initial_quantity: 20000, remaining_quantity: 20000, percentage: 4 },
  ];

 