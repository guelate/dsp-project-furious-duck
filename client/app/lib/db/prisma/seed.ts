import { PrismaClient } from '@prisma/client';
import bcrypt from "bcryptjs";

const prisma = new PrismaClient();

// Fonction pour générer un code de ticket unique de 10 caractères alphanumériques
function generateTicketCode(): string {
  return Array.from({ length: 10 }, () => 
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.charAt(Math.floor(Math.random() * 36))
  ).join('');
}

async function main() {

  const plainPassword1 = "dspTea1";
  const plainPassword2 = "dspTea2";
  const plainPassword3 = "dspTea3";
  const plainPassword4 = "dspTea4";

  const hashedPassword1 = await bcrypt.hash(plainPassword1, 10);
  const hashedPassword2 = await bcrypt.hash(plainPassword2, 10);
  const hashedPassword3 = await bcrypt.hash(plainPassword3, 10);
  const hashedPassword4 = await bcrypt.hash(plainPassword4, 10);

  // Création ou mise à jour des comptes admin
  const admins = [
    { firstname: 'Yann', lastname: 'AKELE', email: 'emmanuelyannakele@etu-digitalschool.paris', password_hash: hashedPassword1, role: 'admin' },
    { firstname: 'Joachim', lastname: 'DEGBOE', email: 'degboejoachim-willy@etu-digitalschool.paris', password_hash: hashedPassword2, role: 'admin' },
    { firstname: 'Enzo', lastname: 'Morin', email: 'enzomorin@etu-digitalschool.paris', password_hash: hashedPassword3, role: 'admin' },
    { firstname: 'Killian', lastname: 'Seyo', email: 'guelatekillianseyo@etu-digitalschool.paris', password_hash: hashedPassword4, role: 'admin' },
  ];

  for (const admin of admins) {
    await prisma.admin.upsert({
      where: { email: admin.email },
      update: {},
      create: { ...admin },
    });
  }

  // Création des prix avec les quantités respectives
  const prizeData = [
    { prize_name: 'Infuseur à thé', initial_quantity: 300000, remaining_quantity: 300000, percentage: 60 },
    { prize_name: 'Boîte de 100g de thé détox ou infusion', initial_quantity: 100000, remaining_quantity: 100000, percentage: 20 },
    { prize_name: 'Boîte de 100g de thé signature', initial_quantity: 50000, remaining_quantity: 50000, percentage: 10 },
    { prize_name: 'Coffret découverte 39€', initial_quantity: 30000, remaining_quantity: 30000, percentage: 6 },
    { prize_name: 'Coffret découverte 69€', initial_quantity: 20000, remaining_quantity: 20000, percentage: 4 },
  ];

  await prisma.prize.createMany({
    data: prizeData,
  });

  // Récupérer tous les prix après la création
  const allPrizes = await prisma.prize.findMany();

  // Générer les tickets avec les prix
  let tickets = [];
  for (const prize of allPrizes) {
    const quantity = prize.initial_quantity;
    for (let i = 0; i < quantity; i++) {
      tickets.push({
        ticket_code: generateTicketCode(), // Génère un code de 10 caractères alphanumériques
        prize_id: prize.prize_id,
        status: 'non utilisé',
        created_at: new Date(), 
        used_at: null,
      });
    }
  }

  // Insertion des tickets dans la base de données
  for (let i = 0; i < tickets.length; i += 10000) {
    const batch = tickets.slice(i, i + 10000);
    await prisma.ticket.createMany({
      data: batch,
      skipDuplicates: true, 
    });
  }

  console.log('Tickets générés et insérés avec succès.');
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
