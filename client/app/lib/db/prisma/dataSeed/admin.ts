import { v4 as uuidv4 } from 'uuid';

export const admins = [
  {
    admin_id: uuidv4(),
    first_name: 'Admin1',
    last_name: 'Last1',
    email: 'admin1@example.com',
    password_hash: 'hashedPassword1',
    role: 'superadmin',
    created_at: new Date(),
  },
  {
    admin_id: uuidv4(),
    first_name: 'Admin2',
    last_name: 'Last2',
    email: 'admin2@example.com',
    password_hash: 'hashedPassword2',
    role: 'moderator',
    created_at: new Date(),
  },
  {
    admin_id: uuidv4(),
    first_name: 'Admin3',
    last_name: 'Last3',
    email: 'admin3@example.com',
    password_hash: 'hashedPassword3',
    role: 'moderator',
    created_at: new Date(),
  },
  {
    admin_id: uuidv4(),
    first_name: 'Admin4',
    last_name: 'Last4',
    email: 'admin4@example.com',
    password_hash: 'hashedPassword4',
    role: 'moderator',
    created_at: new Date(),
  },
];
