import { v4 as uuidv4 } from 'uuid';

const prizes = [
  { prize_id: 'prize-1-uuid', quantity: 3000 },
  { prize_id: 'prize-2-uuid', quantity: 1000 },
  { prize_id: 'prize-3-uuid', quantity: 500 },
  { prize_id: 'prize-4-uuid', quantity: 300 },
  { prize_id: 'prize-5-uuid', quantity: 200 },
];

export const tickets = prizes.flatMap(prize =>
  Array.from({ length: prize.quantity }, (_, i) => ({
    ticket_id: uuidv4(),
    ticket_code: `TICKET${(i + 1).toString().padStart(6, '0')}`.slice(0, 10), // Assurer que le code fait moins de 10 caractères
    prize_id: prize.prize_id,
    user_id: null, // Pas encore associé à un utilisateur
    status: 'non utilisé',
    created_at: new Date(),
    used_at: new Date() // Peut être null si non utilisé
  }))
);
