import { z } from "zod";
import {
  PASSWORD_ERROR,
  GENDER_ERROR,
  POSTAL_CODE_ERROR,
  PHONE_NUMBER_ERROR,
  IDENTIFIANT_ERROR,
  EMAIL_ERROR,
  PASSWORD_NOT_EQUAL,
  CARACTERE_10,
  CARACTERE_8,
  CARACTERE_2,
  CARACTERE_5,
  CARACTERE_255,
  CARACTERE_100,
  AGE_OF_BIRTH,
} from "../constant/constant";

//TO VALIDATE TICKET 
export const TicketSchema = z.object({
  code: z
  .string()
  .length(10, { message: CARACTERE_10 }),
})

//TO ADMIN SIGNIN FORM
export const AdminSchema = z.object({
  email: z.string().email({ message:EMAIL_ERROR }),
  password: z
    .string()
    .min(5, { message: PASSWORD_ERROR })
    .max(20, { message: PASSWORD_ERROR }),
});

//TO VALIDATE SIGNIN FORM
export const SignInSchema = z.object({
  firstname: z
    .string()
    .min(2, { message: IDENTIFIANT_ERROR })
    .max(20, { message: IDENTIFIANT_ERROR }),
  password: z
    .string()
    .min(5, { message: PASSWORD_ERROR })
    .max(20, { message: PASSWORD_ERROR }),
});



export const SignUpSchema = z
  .object({
    firstname: z
      .string()
      .min(2, { message: CARACTERE_2 })
      .max(255, { message: CARACTERE_255 }),

    lastname: z
      .string()
      .min(2, { message: CARACTERE_2 })
      .max(255, { message: CARACTERE_255 }),

    email: z.string().email({ message: "L'adresse e-mail est invalide." }),

    password_hash: z
      .string()
      .min(8, {
        message: CARACTERE_8,
      })
      .max(255, {
        message:CARACTERE_255,
      }),

    confirmPassword: z.string().min(5, {
      message: PASSWORD_ERROR,
    }),

    date_of_birth: z.string().regex(/^\d{4}-\d{2}-\d{2}$/, {
      message: AGE_OF_BIRTH,
    }),

    gender: z.enum(["Homme", "Femme", "Autre"], {
      message: GENDER_ERROR,
    }),

    address: z
      .string()
      .min(5, { message: CARACTERE_5 })
      .max(255, { message: CARACTERE_255}),

    city: z
      .string()
      .min(2, {
        message: CARACTERE_2,
      })
      .max(100, {
        message: CARACTERE_100,
      }),
    postal_code: z
      .string()
      .regex(/^\d{5}$/, {
        message: POSTAL_CODE_ERROR,
      }),

    phone_number: z
      .string()
      .regex(/^\d{10,15}$/, {
        message:
        PHONE_NUMBER_ERROR,
      }),
  })

  .refine((data) => data.password_hash === data.confirmPassword, {
    path: ["confirmPassword"],
    message: PASSWORD_NOT_EQUAL,
  });
