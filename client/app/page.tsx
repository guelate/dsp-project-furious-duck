/* eslint-disable react/no-unescaped-entities */

"use client";
import WelcomSection from "./components/WelcomSection";
import WheelSection from "./components/WheelSection";
import CookieNotice from './components/CookieNotice';
import { Suspense, useEffect, useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";
import PopupConnection from "./components/PopupConnection";
import ProductsCaroussel from "./components/ProductsCaroussel";


export default function Home() {
  const router = useRouter();
  const [showPopup, setShowPopup] = useState(false);


  return (
    <div className="flex flex-col items-center relative z-0 md:py-20">
      <WelcomSection />
      <WheelSection />

      {/* Utilisation de Suspense pour gerer les requêtes asynchrones avec useSearchParams */}
      <Suspense fallback={<div>Chargement en cours...</div>}>
        <SearchParamsHandler
          setShowPopup={setShowPopup}
          showPopup={showPopup}
          router={router}
        />
      </Suspense>

      {showPopup && (
        <PopupConnection
          message="Seul les admin peuvent accéder à l'espace admin"
          onclose={() => setShowPopup(false)}
        />
      )}

      <section>
        <CookieNotice />
      </section>
      <ProductsCaroussel />
    </div>
  );
}

// Composant séparé pour gérer useSearchParams avec Suspense
function SearchParamsHandler({ setShowPopup, showPopup, router }: any) {
  const searchParams = useSearchParams();

  useEffect(() => {
    if (searchParams.get("error") === "not_authorized") {
      setShowPopup(true);
    }
  }, [searchParams, setShowPopup]);

  const handleClose = () => {
    setShowPopup(false);
    router.replace("/");
  };

  return null;
}
//preproduction main