"use client";

import React, { useEffect, useState } from 'react';
import { Badge } from "@/components/ui/badge";
import TableData from "@/components/TableData";
import { PRICE_TABLE_VARIANT, GAINT_HISTORY_VARIANT, GLOBAL_GAIN_HISTORY_VARIANT } from "@/lib/constant/constant";
import Link from 'next/link';
import { useSession } from "next-auth/react"; // Importation de useSession pour gérer l'authentification

// Déclare le type pour les données utilisateur
interface UserData {
  // Définir les propriétés de l'utilisateur en fonction de la structure de tes données
  ticketCode: string; 
  rewards: string; 
  email: string; 
}

export default function Gains() {
  const { data: session } = useSession(); 
  const user = session?.user || null;
  

  return (
    <div className="grid gap-6 md:max-w-4xl mx-auto  py-8">
      <div className="flex justify-end gap-3 p-3 mt-12">
        <Link href="/jeuConcours" passHref>
          <Badge
            variant="secondary"
            className="bg-green-300 hover:bg-green-400 hover:drop-shadow cursor-pointer"
          >
            Jeu Concours
          </Badge>
        </Link>
      </div>

      {user ? (
        <div className="flex flex-col h-auto w-screen md:w-full md:pt-2">
          <div className='pt-5'>
            <h2 className="text-center text-black-500 font-bold text-xl pb-10">Historique des gains de tous les participants</h2>
            <div className="flex flex-col mx-auto w-3/4 h-auto md:w-full md:pt-2">
              <TableData variantProps={GLOBAL_GAIN_HISTORY_VARIANT} checkbox={true} onSelectionChange={() => { }} selectedIds={[]} />
            </div>
          </div>
          <div className='pt-10'>
            <h2 className="text-center text-black-500 font-bold text-xl py-10">Historique de vos gains</h2>
            <div className="flex flex-col mx-auto w-3/4 h-auto md:w-full md:pt-2">
              <TableData variantProps={GAINT_HISTORY_VARIANT} checkbox={true} onSelectionChange={() => { }} selectedIds={[]} />
            </div>
          </div>
        </div>
      ) : (
        
        <div className="flex flex-col h-auto w-screen md:w-full md:pt-2">
          <h2 className="text-center text-black-500 font-bold text-xl pb-10">Historique des gains de tous les participants</h2>
          <div className="flex flex-col mx-auto w-3/4 h-auto md:w-full md:pt-2">
            <TableData variantProps={GLOBAL_GAIN_HISTORY_VARIANT} checkbox={true} onSelectionChange={() => { }} selectedIds={[]} />
          </div>
          <div className="py-16 text-center text-red-500 text-md font-bold">
            Aucune donnée disponible vous concernant. Il faut être authentifié et avoir participé au jeu concours !!
          </div>
        </div>
      )}

    </div>
  );
}
