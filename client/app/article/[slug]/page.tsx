import { Card } from "@/components/ui/card";

const articles = {
  "tout-les-jours": {
    title: "Est-il bon de boire du thé tous les jours ?",
    content:
      "Oui, le thé en vrac plutôt que le thé en sachets peut être consommé quotidiennement dans le cadre d'un régime sain et équilibré. C’est d’ailleurs la clef pour bénéficier des bienfaits de ses substances. Cependant, bien qu'un apport modéré soit sain pour la plupart des gens, en boire trop peut entraîner des effets secondaires négatifs, tels que l'anxiété, des maux de tête, des problèmes digestifs et des troubles du sommeil. La plupart des gens peuvent boire 3 à 4 tasses de thé par jour sans effets indésirables, mais certains peuvent ressentir des effets indésirables à des doses plus faibles.",
  },
  "the-noir": {
    title: "Antioxydants & Digestion : les secrets du thé noir révélés",
    content:
      "Pour celles et ceux qui préfèrent une boisson plus corsée pour bien démarrer la journée, le thé noir se révèle être un excellent choix. À l’instar du thé vert, il est riche en antioxydants, ce qui le rend bénéfique pour la santé intestinale. Le thé noir présente de nombreux avantages pour la santé similaires au thé vert, et il est généralement très bien toléré par ceux qui gonflent facilement, précise le gastro-entérologue. En plus de ses vertus antioxydantes, cette boisson offre une stimulation douce sans les effets indésirables que d'autres boissons plus riches en FODMAP pourraient causer. Pour les amateurs de thé cherchant un compromis entre goût puissant et confort digestif, celui-ci est un choix à envisager sérieusement.",
  },
  "les-biens-fait-du-the": {
    title: "Les biens fait du thé",
    content:
      "Selon une littérature scientifique foisonnante, le thé présente de multiples vertus pour la santé grâce à ses nombreux composés bioactifs. Parmi ses effets bénéfiques, il abaisse la mortalité cardio-vasculaire (diminution du cholestérol et de la formation de la plaque d’athérome, dilatation des artères…), diminue le risque de diabète, consolide les os et améliore l’arthrose, freine le déclin cognitif (démence et maladies neuro-dégénératives) et pourrait prévenir certains cancers (sphère digestive, poumon, sein, prostate, peau). Il renforce aussi l’immunité, possède des qualités antibactériennes et anti-inflammatoires et améliore la circulation sanguine.",
  },
  "deficit-en-Fer": {
    title: "Faut-il éviter le thé lorsque l’on a des carences en fer ?",
    content:
      "Les tanins du thé contribuent à une moins bonne absorption du fer. Ainsi, lorsqu’il est consommé pendant le repas, le thé peut empêcher 60 à 70 % de l’absorption du fer. Cela est valable pour le thé noir et le thé vert sans grande différence. Pour les personnes souffrant de déficit en fer, il faut donc éviter de boire du thé pendant les repas afin que les aliments riches en fer et le thé ne se retrouvent pas dans le même bol alimentaire. Attendez au moins 2/3 heures pour consommer votre thé après avoir mangé.",
  },
};

export default function Article({ params }: { params: { slug: string } }) {
  const { slug } = params;

  if (!(slug in articles)) {
    return (
      <div>
        <main className="container mx-auto p-6 bg-[#FCFCFC] text-[#30593E]">
          <p>Article non trouvé</p>
        </main>
      </div>
    );
  }

  const article = articles[slug as keyof typeof articles];

  return (
    <div className="h-screen flex items-center justify-center md:px-48 bg-gray-100">
      <Card className="shadow-md text-center border p-5 rounded .rounded-lg bg-white">
        <h1 className="text-4xl font-bold mb-6 text-[#30593E]">
          {article.title}
        </h1>
        <p className="leading-relaxed text-left">{article.content}</p>
      </Card>
    </div>
  );
}
