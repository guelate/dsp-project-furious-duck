import BlogCard from "@/components/BlogCard";
import { Card } from "@/components/ui/card";
// import thenoir from "../../public/thenoir.png"
// import troisemeArticle from "../../public/troisiemeArticle.png"
const articles = [
  {
    id: 1,
    title: "Est-il bon de boire du thé tous les jours ?",
    slug: "tout-les-jours",
    accroche: "Thé tous les jours : oui, mais...",
    image: "/article2.png",
    createdAt: "2024-08-01T10:00:00Z",
    source: "le Guide Santé",
  },
  {
    id: 2,
    title: "Antioxydants & Digestion : les secrets du thé noir révélés",
    slug: "the-noir",
    accroche:
      "Et si le thé noir était la boisson idéale pour les intestins sensibles ?",
    image: "/article1.png",
    createdAt: "2024-07-25T09:30:00Z",
    source: "Au féminin",
  },
  {
    id: 3,
    title: "Les biens fait du thé",
    slug: "les-biens-fait-du-the",
    accroche: "une boisson aux vertus insoupçonnées",
    image: "/article5.png",
    createdAt: "2024-06-15T11:00:00Z",
    source: "Palais des thés",
  },
  {
    id: 4,
    title: "Déficit en Fer ? Pourquoi Éviter le Thé à Table",
    slug: "deficit-en-Fer",
    accroche: "Thé et fer ne font pas bon ménage : découvrez pourquoi !",
    image: "/article8.png",
    createdAt: "2024-05-20T13:30:00Z",
    source: "yuka.io",
  },
 
];

export default function ArticleList() {
  return (
    <div className="flex md:mt-32 md:p-10 md:py-10 md:justify-center md:mb-24 mb-5 bg-gray-50">
      <div className="flex flex-col md:gap-20 md:w-4/5">
        <h1 className="text-[#445d4d] text-center text-3xl ">
          Nos articles sur le thés
        </h1>
        <div className="md:pl-10">

        <div className="grid grid-cols-1 md:grid-cols-2 gap-20 px-10 md:px-20 ">
          {articles.map((article) => (
            <BlogCard
            key={article.id}
            title={article.title}
            accroche={article.accroche}
            slug={article.slug}
            image={article.image}
            createdAt={article.createdAt}
            source={article.source}
            />
          ))}
        </div>
          </div>
    
      </div>
    </div>
  );
}
