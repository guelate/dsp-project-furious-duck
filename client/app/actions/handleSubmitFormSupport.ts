// src/actions/handleSubmitFormSupport.ts
import { resend } from '../../public/src/lib/resend'; // Assurez-vous que ce chemin est correct
import { AdminMail } from '../../emails/AdminMail'; // Vérifiez que le chemin est correct
import { MagicLinkEmail } from '../../emails/MagicLink'; // Vérifiez que le chemin est correct

interface FormData {
  email: string;
  message: string;
  objet: string;
}

interface HandleSubmitResponse {
  success: boolean;
  error?: string;
}

export async function handleSubmitFormSupport(formData: FormData): Promise<HandleSubmitResponse> {
  const { email, message, objet: object } = formData;

  try {
    // Validation basique de l'adresse email
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
      throw new Error("L'adresse email fournie n'est pas valide.");
    }

    // Envoi de l'email à votre entreprise
    await resend.emails.send({
      from: "TheTipTop Support <not-reply-support@dsp5-archi-o22a-4-5-g3.fr>",
      to: ["dsparchi022g3@gmail.com"],
      subject: object,
      react: AdminMail({
        magicLink: "https://dsp5-archi-o22a-4-5-g3.fr/",
        message: message,
        email: email,
      }),
    });

    // Envoi de l'email de confirmation au client
    await resend.emails.send({
      from: "TheTipTop Support <not-reply-support@dsp5-archi-o22a-4-5-g3.fr>",
      to: email,
      subject: "Confirmation de réception de votre message",
      react: MagicLinkEmail({
        magicLink: "https://dsp5-archi-o22a-4-5-g3.fr/",
        message: "Merci de nous avoir contacté. Nous avons bien reçu votre message et nous vous répondrons sous peu.",
      }),
    });

    return { success: true };
  } catch (error) {
    console.error("Erreur lors de l'envoi des emails :", error);
    return { success: false, error: error instanceof Error ? error.message : 'Erreur inconnue' };
  }
}
