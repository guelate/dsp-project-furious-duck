import React from 'react';
import CommandBlock from '@/components/CommandBlock';

export default function DockerCommandsPage() {
  const commands = [
    {
        command: "Git Clone https://gitlab.com/guelate/dsp-project-furious-duck.git",
        explanation: "Récupérer le projet sur Git"
      },
      {
        command: "cd client",
        explanation: "Aller dans le chemin /client"
      },
    {
        command: "Docker compose up",
        explanation: "Éxécuter l'ensemble des services de l'application (dans le chemin /client)"
      },
    {
        command: "npm install --legacy-peer-deps",
        explanation: "Installer des dépendances du projet (dans le chemin /client)"
    },    
    {
      command: "npm run dev",
      explanation: "Éxecuter le projet (dans le chemin /client)"
  },
  
  ];

  return (
    <div className="flex flex-col items-center relative z-0 md:py-20">
      <h1 className="text-3xl font-bold mb-6">Bien démarrer le projet</h1>
      <section>
      <p className="mb-6 text-center" >Les Commandes de base du projet:</p>
      {commands.map((cmd, index) => (
        <CommandBlock key={index} command={cmd.command} explanation={cmd.explanation} />
      ))}
      </section>
    </div>
  );
}