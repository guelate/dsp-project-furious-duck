import React from 'react';

interface VideoProps {
  src: string;
  title: string;
}

const VideoBlock: React.FC<VideoProps> = ({ src, title }) => {
  return (
    <div className="mb-12 flex flex-col items-center w-full ">
      <h2 className="text-2xl font-semibold mb-4 text-center">{title}</h2>
      <div className="w-full max-w-4xl">
        <video
          controls
          className="w-full rounded-xl shadow-xl"
        >
          <source src={src} type="video/mp4" />
          Votre navigateur ne supporte pas la lecture de vidéos.
        </video>
      </div>
    </div>
  );
};

const VideoPage: React.FC = () => {
  const videos: VideoProps[] = [
    {
      src: "https://primes-renovation.net/wp-content/uploads/2024/10/Client.mp4",
      title: "Partie Client"
    },
    {
      src: "https://primes-renovation.net/wp-content/uploads/2024/10/Admin.mp4",
      title: "Partie Admin"
    },
    {
      src: "https://primes-renovation.net/wp-content/uploads/2024/10/Envoie-de-mail.mp4",
      title: "Envoie de Mail"
    },
  ];

  return (
    <div className="flex flex-col items-center relative z-0 md:py-20">
      <h1 className="text-4xl font-bold mb-10 text-center">Nos Vidéos</h1>
      <section className="w-full max-w-5xl flex flex-col items-center">
        {videos.map((video, index) => (
          <VideoBlock key={index} {...video} />
        ))}
      </section>
    </div>
  );
};

export default VideoPage;