import CommandBlock from "@/components/CommandBlock";
import Image from "next/image";
import supabase from "../../../public/supabase 1.png";
import project from "../../../public/selection all Project 2.png";
import Furious from "../../../public/selection Furious Ducks bdd 3.png";
import Table from "../../../public/selection Table Editor 4.png";

export default function Base() {
  return (
    <div className="flex flex-col items-center relative z-0 md:py-20">
      <h1 className="text-3xl font-bold mb-6">Acceder à la base de donnée</h1>
      <section className="w-full max-w-6xl">
        <table className="table-auto w-full bg-white text-black shadow-lg rounded-lg overflow-hidden">
          <thead className="bg-[#445d4d] text-white">
            <tr>
              <th className="py-4 px-6 text-left text-lg font-semibold">
                Hébergement
              </th>
              <th className="py-4 px-6 text-left text-lg font-semibold">
                Copier et utiliser
              </th>
            </tr>
          </thead>
          <tbody className="divide-y divide-gray-300">
            <tr className="hover:bg-gray-200 transition-colors duration-200">
              <td className="py-4 px-6">
                Supabase:{" "}
                <a
                  href="https://supabase.com/dashboard/sign-in?returnTo=%2Forg%2Fwoergwnbdkruaqumfyqy%2Fteam"
                  className="text-blue-600 underline"
                >
                  lien
                </a>
              </td>
              <td className="py-4 px-6">
                <ul>
                  <li>Identifiant: dsparchi022g3@gmail.com</li>
                  <li>Mot de passe: Gq)_97^zkH2H8w</li>
                </ul>
              </td>
            </tr>
            <tr className="flex items-center justify-center">
              <td className="py-4 px-6">
                <p>
                  https://supabase.com/dashboard/sign-in?returnTo=%2Forg%2Fwoergwnbdkruaqumfyqy%2Fteam
                </p>
              </td>
            </tr>
          </tbody>
        </table>
        <div className="flex flex-col text-center gap-12 mt-10">
          <h1 className="text-2xl">Supabase</h1>

          <Image src={supabase} alt="supabase db" />
          <h1 className="text-2xl">Selection: all projects</h1>

          <Image src={project} alt="selection project" />
          <h1 className="text-2xl">
            Selection de la base de donnée: Furious Ducks bdd
          </h1>

          <Image src={Furious} alt="selection Furious Duck" />
          <h1 className="text-2xl">Table des tickets</h1>

          <Image src={Table} alt="Selection Table" />
        </div>
      </section>
    </div>
  );
}
