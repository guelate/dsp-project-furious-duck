import React from 'react';

export default function DocsPage() {
  const tickets = [
    '4I32QNHCS8', 'NY1YZJCFUW', 'ICBAKI333T', '6RS72HS0HV', '3U99T3Y9BS',
    'ACA4AE9JIC', 'C5DG0C456F', 'LIS8H58VYM', 'ZSGZH22V3M', 'FNXYMAF5VC'
  ];

  return (
    <div className="min-h-screen bg-[#445d4d] text-white flex flex-col items-center relative z-0 py-10 md:py-20">
      {/* Title */}
      <h1 className="text-5xl md:text-6xl font-extrabold mb-6 animate-pulse">Introduction</h1>
      <h2 className="text-2xl mb-4">Bienvenue Chez Furious Duck Documentation!</h2>

      {/* Section Description */}
      <section className="bg-white text-black rounded-lg shadow-xl p-6 max-w-4xl mb-8">
        <p className="text-lg font-medium">Qu'est Furious Duck?</p>
        <p className="mt-4 leading-relaxed">
          Le Projet Furious Duck lance un jeu-concours interactif pour promouvoir la marque de thé. Ce concours, destiné aux clients, leur permet de gagner des lots variés en lien avec l'univers du thé. L'objectif principal est de tester et valider le workflow technique mis en place pour l'entreprise, en l'appliquant à ce concours. Cela inclut la gestion des inscriptions, la récupération et le traitement des données, l'intégration des résultats avec la plateforme de gestion et la synchronisation avec les points de vente physiques et en ligne. Ce projet sert ainsi de test grandeur nature pour l'ensemble des processus.
        </p>
      </section>

      {/* Main Features */}
      <h1 className="text-4xl md:text-5xl font-bold mb-6">Main Features</h1>
      <h2 className="text-xl mb-4">Quelques caractéristiques pour bien essayé le projet:</h2>

      {/* Features Table */}
      <section className="w-full max-w-6xl">
        <table className="table-auto w-full bg-white text-black shadow-lg rounded-lg overflow-hidden">
          <thead className="bg-[#445d4d] text-white">
            <tr>
              <th className="py-4 px-6 text-left text-lg font-semibold">Caractéristique</th>
              <th className="py-4 px-6 text-left text-lg font-semibold">Copier et utiliser</th>
            </tr>
          </thead>
          <tbody className="divide-y divide-gray-300">
            <tr className="hover:bg-gray-200 transition-colors duration-200">
              <td className="py-4 px-6">Connexion utilisateur</td>
              <td className="py-4 px-6">
                <ul>
                  <li>Identifiant: Mario</li>
                  <li>Mot de passe: MarioMario</li>
                </ul>
              </td>
            </tr>
            <tr className="hover:bg-gray-200 transition-colors duration-200">
              <td className="py-4 px-6">Connexion Admin</td>
              <td className="py-4 px-6">
                <ul>
                  <li>Identifiant: enzomorin@etu-digitalschool.paris</li>
                  <li>Mot de passe: dspTea3</li>
                </ul>
              </td>
            </tr>
            <tr className="hover:bg-gray-200 transition-colors duration-200">
              <td className="py-4 px-6">Ticket Jeux Concours</td>
              <td className="py-4 px-6">
                <div className="flex">
                  <ul className="mr-8">
                    {tickets.slice(0, 5).map((ticket, index) => (
                      <li key={index}>{ticket}</li>
                    ))}
                  </ul>
                  <ul>
                    {tickets.slice(5).map((ticket, index) => (
                      <li key={index + 5}>{ticket}</li>
                    ))}
                  </ul>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </section>
    </div>
  );
}