"use client";
import React, { useRef, useState } from 'react';
import ReCAPTCHA from "react-google-recaptcha";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";
import { Button } from "@/components/ui/button";
import {
  Collapsible,
  CollapsibleTrigger,
  CollapsibleContent,
} from "@/components/ui/collapsible";
import { useRouter } from "next/navigation";
import support3 from "/public/support3.svg"
import Image from "next/image";

export default function Component() {
  const router = useRouter();
  const formRef = useRef<HTMLFormElement>(null);
  const recaptchaRef = useRef<ReCAPTCHA>(null);
  const [isCaptchaVerified, setIsCaptchaVerified] = useState(false);

  const handleCaptchaChange = (value: string | null) => {
    setIsCaptchaVerified(!!value);
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (!isCaptchaVerified) {
      alert("Veuillez valider le captcha avant de soumettre le formulaire.");
      return;
    }

    const formData = new FormData(e.currentTarget);
    const formDataObject = Object.fromEntries(formData.entries());

    try {
      const captchaValue = recaptchaRef.current?.getValue();
      const res = await fetch("/api/handleSubmitFormSupport", {
        method: "POST",
        body: JSON.stringify({ ...formDataObject, captchaResponse: captchaValue }),
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const result = await res.json();

      if (result.success) {
        if (formRef.current) {
          formRef.current.reset();
        }
        recaptchaRef.current?.reset();
        setIsCaptchaVerified(false);
        router.push("/support");
      } else {
        console.error("Erreur lors de l'envoi des emails :", result.error);
      }
    } catch (error) {
      console.error("Erreur réseau ou serveur :", error);
    }
  }

  const [openIndex, setOpenIndex] = useState<number | null>(null);

  const handleToggle = (index: number) => {
    setOpenIndex(openIndex === index ? null : index);
  };

  return (
    <div className="flex flex-col items-center px-10 py-20 relative z-0 md:px-10 text-start">
      <div className="w-full max-w-10xl mx-auto space-y-12 py-12 md:py-16 lg:py-20 sm:px-10 content-center">
        <div>
          <h2 className="text-4xl font-bold tracking-tight text-[#445d4d] text-center mb-20">
            Centre d'aide
          </h2>
          <div className="flex flex-col md:flex-row justify-between items-start gap-20 content-center">
            <div className="flex-grow w-full mt-10 md:w-1/2">
              <Image
                src={support3}
                alt="support"
                className="w-full"
              />        
            </div>
            <div className="flex-grow w-full md:w-1/2">
              <p className="text-muted-foreground">
                Vous avez des questions concernant le jeu concours ?{" "}
              </p>
              <p className="mt-2 text-muted-foreground">
                Nous sommes disponible pour vous répondre{" "}
              </p>

              <form   
                ref={formRef}
                onSubmit={handleSubmit}
                className="mt-5 space-y-4"
              >
                <div className="grid grid-cols-1 gap-4 sm:grid-cols-1 text-start">
                  <div>
                    <Label htmlFor="text" className="text-[#445d4d]">
                      Objet
                    </Label>
                    <Input
                      id="text"
                      name="objet"
                      placeholder="Quelle est votre préocupation ?"
                      className="rounded shadow"
                    />
                  </div>
                </div>
                <div className="grid grid-cols-1 gap-4 sm:grid-cols-1">
                  <div>
                    <Label htmlFor="email" className="text-[#445d4d]">
                      Email
                    </Label>
                    <Input
                      id="email"
                      name="email"
                      type="email"
                      placeholder="Entrer votre email"
                      className="rounded shadow"
                    />
                  </div>
                </div>
                <div className="grid grid-cols-1 gap-4 sm:grid-cols-1">
                  <div>
                    <Label htmlFor="message" className="text-[#445d4d]">
                      Message
                    </Label>
                    <Textarea
                      id="message"
                      name="message"
                      placeholder="Entrer votre message"
                      className="min-h-[200px] rounded shadow resize-none"
                    />
                  </div>
                </div>
                <div className="flex justify-center">
          <ReCAPTCHA
            ref={recaptchaRef}
            sitekey="6LdyDVYqAAAAAKsyle6p8DrcPpJSK8LQFC-wENN_"
            onChange={handleCaptchaChange}
          />
        </div>
        <div className="flex justify-center">
          <Button
            type="submit"
            className="sm:w-auto bg-[#445d4d] hover:bg-white hover:text-[#445d4d] text-white font-semibold py-2 px-4 border border-gray-600 rounded shadow"
            disabled={!isCaptchaVerified}
          >
                    Valider
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div>
          <h2 className="text-3xl font-bold tracking-tight text-[#445d4d] text-center md:mt-20">
            FAQ
          </h2>
          <div className="mt-6 space-y-4">
          <Collapsible>
            <CollapsibleTrigger className="flex w-full items-center justify-between rounded-md bg-muted px-4 py-3 text-left text-lg font-medium transition-colors hover:bg-muted/50 focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-ring">
              <span>Comment puis-je participer au jeu concours ThéTipTop ?</span>
              <ChevronDownIcon className="h-5 w-5 transition-transform [&[data-state=open]]:rotate-180" />
            </CollapsibleTrigger>
            <CollapsibleContent className="px-4 pt-2 text-muted-foreground">
              Pour participer, il vous suffit de vous inscrire sur notre site web et de suivre les instructions. Vous devrez peut-être remplir un formulaire ou répondre à quelques questions. Plus vous jouez, plus vous avez de chances de gagner !
            </CollapsibleContent>
          </Collapsible>

          <Collapsible>
            <CollapsibleTrigger className="flex w-full items-center justify-between rounded-md bg-muted px-4 py-3 text-left text-lg font-medium transition-colors hover:bg-muted/50 focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-ring">
              <span>Quels sont les prix gagner à l'issu de ce jeu concours?</span>
              <ChevronDownIcon className="h-5 w-5 transition-transform [&[data-state=open]]:rotate-180" />
            </CollapsibleTrigger>
            <CollapsibleContent className="px-4 pt-2 text-muted-foreground">
              Nous avons plusieurs prix excitants à gagner, y compris des coffrets cadeaux de ThéTipTop, des accessoires pour le thé, des bons d'achat et bien plus encore ! Consultez notre page de prix pour plus de détails.
            </CollapsibleContent>
          </Collapsible>

            <Collapsible>
              <CollapsibleTrigger className="flex w-full items-center justify-between rounded-md bg-muted px-4 py-3 text-left text-lg font-medium transition-colors hover:bg-muted/50 focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-ring">
                <span>Pourquoi devez-vous participer à ce jeu concours ?</span>
                <ChevronDownIcon className="h-5 w-5 transition-transform [&[data-state=open]]:rotate-180" />
              </CollapsibleTrigger>
              <CollapsibleContent className="px-4 pt-2 text-muted-foreground">
                Ce concours est une façon pour nous de célébrer notre amour pour le thé et de partager cette passion avec nos clients. C'est aussi l'occasion de vous remercier pour votre fidélité !
              </CollapsibleContent>
            </Collapsible>
          </div>
        </div>
      </div>
    </div>
  );
}

function ChevronDownIcon(props: any) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="m6 9 6 6 6-6" />
    </svg>
  );
}

function XIcon(props: any) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M18 6 6 18" />
      <path d="m6 6 12 12" />
    </svg>
  );
}
