import TicketVerification from '../components/TicketVerification';

export default function VerificationTicketPage() {
  return (
    <div>
      <h1>Vérification des tickets</h1>
      <TicketVerification />
    </div>
  );
}