"use client"

/* eslint-disable react/no-unescaped-entities */
import React from "react";

import { CardHeader, CardTitle, CardContent } from "@/components/ui/card";
import {
  Table,
  TableHeader,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
} from "@/components/ui/table";
import { Badge } from "@/components/ui/badge";
import { Button } from "@/components/ui/button";
import { Progress } from "@/components/ui/progress";
import { Input } from "@/components/ui/input";
import Link from 'next/link';
import { useRouter } from 'next/navigation';

export default function GameHomePage() {
  //TODO: call only game component and reward component
  //TODO: text change depending on the user if it client we have text A otherwise if it visitor we have text B Bienvenue ou Chers Membre for title

  const router = useRouter();

  const handleButtonClick = () => {
    // Rediriger vers la page d'accueil et ajouter l'ancre pour scroller
    router.push('/#wheelSection');
  };

  return (
    <div className="grid gap-6 max-w-4xl mx-auto px-4 py-8">
      <div className="flex justify-end gap-3 p-3 mt-12 md:mr-8">
        <Link href="/gains" passHref>
          <Badge
            variant="secondary"
            className="bg-green-300 hover:bg-green-400 hover:drop-shadow cursor-pointer"
          >
            Historique des gains
          </Badge>
        </Link>
      </div>

      {/* Jeu concours explication */}
      <div className="flex flex-col">
        {/* <Card className="flex flex-col"> */}
        <CardHeader className="text-center">
          <CardTitle>Bienvenue !</CardTitle>
        </CardHeader>
        <CardContent className="">
          <div className="md:flex md:flex-col md:gap-3 md:py-2 md:items-center">
            <div className="">
              <p>
                Je suis ravi de vous accueillir sur notre site, où nous
                célébrons l&apos;ouverture de notre 10ème boutique à Nice avec
                un jeu-concours exceptionnel. Nos thés de grande qualité
                incluent des mélanges signatures uniques, des thés détox,
                blancs, aux légumes, et diverses infusions, tous certifiés bios
                et faits à la main. Profitez de cette opportunité et tentez
                votre chance pour remporter des prix exceptionnels.Bonne chance
                à tous !
              </p>
            </div>
            <div className="">
              <p>
                Pendant 30 jours, chaque achat de plus de 49€ vous donnera droit
                à un ticket contenant un code unique de 10 caractères.Chaque
                ticket est gagnant, avec un pourcentage de récompense indiqué
                dans le tableau ci-dessous. Information importante: pour le
                tirage au sort du gros lot, le nombre de participations
                n'augmente pas ses chances de gagner.
              </p>
              <p>
                Pour participer au jeu-concours, suivez ces étapes simples :
              </p>
              <ul className="p-3">
                <li className="list-disc">S'inscrire sur le site</li>

                <li className="list-disc">Entrer votre code et valider</li>

                <li className="list-disc">Récupérer son lot</li>
              </ul>
            </div>
          </div>
        </CardContent>
      </div>
      {/* Table */}
      <div className="px-5 md:flex md:flex-row md:items-center md:justify-center">
        <div className="">
          <Table className="border">
            <TableHeader className="bg-green-200">
              <TableRow>
                <TableHead>Lots</TableHead>
                <TableHead className="">Pourcentage de chance</TableHead>
              </TableRow>
            </TableHeader>

            <TableBody>
              <TableRow>
                <TableCell>infuseur à thé</TableCell>
                <TableCell className="text-center">60 %</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>100g de thé détox ou d&apos;infusion</TableCell>
                <TableCell className="text-center">20 %</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>100g de thé signature</TableCell>
                <TableCell className="text-center">10 %</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>coffret découverte d'une valeur de 39€</TableCell>
                <TableCell className="text-center">6 %</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  coffret découverte d&apos;une valeur de 69€
                </TableCell>
                <TableCell className="text-center">4 %</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </div>
      </div>

      {/* Participation jeu */}
      <div className="flex flex-col justify-center items-center gap-5 md:gap-10">
        <div className="flex justify-center w-full gap-3 mt-2 md:p-3 md:w-1/5 md:p-2 md:mt-6">
          {/* <Label htmlFor="email">Entrer votre code</Label> */}

          <Button className="text-[#30593E] w-4/6 border border-gray-300 hover:shadow-md hover:bg-gray-300 hover:border-gray-400 rounded .rounded-lg bg-gray-300 items-center" onClick={handleButtonClick}>
            Participez 🚀
          </Button>
        </div>
      </div>
    </div>
  );
}

{
  /* <div className="flex p-2 mb-2 justify-center items-center">
<Progress value={0} className="border border-black w-2/4" />
</div> */
}

// 100% des tickets seront gagnants, voici la répartition des gains :
// ● 60% des tickets offrent un infuseur à thé 1
// ● 20% des tickets offrent une boite de 100g d’un thé détox ou d’infusion 2
// ● 10% des tickets offrent une boite de 100g d’un thé signature 3
// ● 6% des tickets offrent un coffret découverte d’une valeur de 39€ 4
// ● 4% des tickets offrent un coffret découverte d’une valeur de 69€ 5

// un code à 10 caractères comprenant des chiffres et des lettres

// Le jeu-concours aura lieu sur une période de 30 jours durant lesquels 500 000
//  tickets maximums pourront être distribués. Les joueurs auront les 30 jours
// du jeu concours ainsi que 30 jours supplémentaires à compter de la
// date de clôture du jeu pour aller sur le site internet afin
// tester le code de leur(s) ticket(s) et réclamer leur lot en magasin ou en ligne.

// Attention : Les codes qui seront présents sur les tickets de caisse devront
// être générés dès le début du jeu concours afin de pouvoir respecter obligatoirement les pourcentages de gains.
// Vous devrez donc avoir en base de données 500 000 codes qui pourront être utilisés avec leur gains déjà associé.

// À l’issue du jeu-concours, un tirage au sort sera effectué parmi tous les participants
// afin de déterminer le gagnant d’un an de thé d’une valeur de 360€.
// Pour le tirage au sort du gros lot, le nombre de participations
// d’un client n'augmente pas ses chances de gagner.

// Le gérant de la société Thé Tip Top souhaite un site dédié au jeu-concours qui devra :
// 1. Présenter aux clients le jeu-concours et les lots à gagner
// 2. Permettre aux clients de s’inscrire au jeu-concours via un compte Google/Facebook et via un
// formulaire d’inscription “classique”
// 3. Permettre de participer au jeu-concours via leur(s) numéro(s) de ticket(s)
// 4. Permettre aux clients de visualiser l’historique de leurs gains
// 5. Permettre aux administrateurs de visualiser les statiques du jeu-concours (nombres de tickets
// fournis, nombres de tickets utilisés, nombre de lots déjà gagnés, statistiques sur les gagnants
// en termes de sexe, âge, etc.)
// 6. Permettre aux administrateurs d’utiliser les données récoltées à des fins d’emailing
// 7. Permettre aux employés en boutique de visualiser les gains d’un client afin de lui fournir son
// gain et de noter le gain comme remis
// 8. Évidemment le site devra être accessible à tous devices, respecter les bonnes pratiques
// d’accessibilité et de référencement et être RGPD friendly
