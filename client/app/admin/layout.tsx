import NavbarAdmin from "@/components/NavBarAdmin";
import SidebarAdmin from "@/components/SideBarAdmin";


export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <div>
      <NavbarAdmin />
      <div className="flex justify-center md:w-5/6 md:ml-60">
        {children}
      </div>
      <SidebarAdmin />
    </div>
  );
}
