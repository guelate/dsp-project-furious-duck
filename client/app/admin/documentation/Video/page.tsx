import React from 'react';

interface VideoProps {
  src: string;
  title: string;
}

const VideoBlock: React.FC<VideoProps> = ({ src, title }) => {
  return (
    <div className="mb-12 flex flex-col items-center w-full ">
      <h2 className="text-2xl font-semibold mb-4 text-center">{title}</h2>
      <div className="w-full max-w-4xl">
        <video
          controls
          className="w-full rounded-xl shadow-xl"
        >
          <source src={src} type="video/mp4" />
          Votre navigateur ne supporte pas la lecture de vidéos.
        </video>
      </div>
    </div>
  );
};

const VideoPage: React.FC = () => {
  const videos: VideoProps[] = [
    {
      src: "/chemin/vers/video1.mp4",
      title: "Vidéo 1"
    },
    {
      src: "/chemin/vers/video2.mp4",
      title: "Vidéo 2"
    },
    {
      src: "/chemin/vers/video3.mp4",
      title: "Vidéo 3"
    },
    {
      src: "/chemin/vers/video4.mp4",
      title: "Vidéo 4"
    }
  ];

  return (
    <div className="flex flex-col items-center relative z-0 md:py-20">
      <h1 className="text-4xl font-bold mb-10 text-center">Nos Vidéos</h1>
      <section className="w-full max-w-5xl flex flex-col items-center">
        {videos.map((video, index) => (
          <VideoBlock key={index} {...video} />
        ))}
      </section>
    </div>
  );
};

export default VideoPage;