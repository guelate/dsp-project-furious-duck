import React from 'react';
import Link from 'next/link';

export default function DocsLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
<div className="flex h-screen">
  <nav className="w-64 p-4 border-r bg-[#445d4d] text-white flex flex-col justify-center">
    <h2 className="text-xl font-bold mb-4">
      <Link href="./">Commencer</Link>
    </h2>
    <ul>
      <li className="mb-4">
        <Link
          href="/Docs/installation"
          className="block text-lg hover:bg-white/20 px-4 py-2 rounded-lg transition-all transform hover:translate-x-1 hover:shadow-lg"
        >
          Installation
        </Link>
      </li>
      <li>
        <Link
          href="/Docs/Video"
          className="block text-lg hover:bg-white/20 px-4 py-2 rounded-lg transition-all transform hover:translate-x-1 hover:shadow-lg"
        >
          Vidéo
        </Link>
      </li>
    </ul>
  </nav>

      <main className="flex-1 p-8 overflow-y-auto">
        {children}
      </main>
    </div>
  );
}