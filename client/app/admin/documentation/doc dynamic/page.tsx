import React from 'react';
import { notFound } from 'next/navigation';

// This would typically come from a CMS or file system
const docs = {
  'getting-started': { title: 'Getting Started', content: 'This is the getting started guide...' },
  'features': { title: 'Features', content: 'Here are the main features of our product...' },
  // Add more documentation pages here
};

export default function DocPage({ params }: { params: { slug?: string[] } }) {
  // Vérification que params.slug est bien un tableau avant de l'utiliser
  if (!params.slug || !Array.isArray(params.slug)) {
    return <div>Aucun document disponible.</div>;
  }

  // Concaténation des slug pour former la clé
  const slug = params.slug.join('/');
  const doc = docs[slug as keyof typeof docs];

  // Si aucun document n'existe pour le slug fourni, on renvoie une page 404
  if (!doc) {
    notFound();
  }

  // Rendu de la page avec le contenu du document
  return (
    <div className="flex md:py-50">
      <h1 className="text-3xl font-bold mb-4">{doc.title}</h1>
      <div>{doc.content}</div>
    </div>
  );
}
