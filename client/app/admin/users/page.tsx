"use client";

import TableData from "@/components/TableData";
import { USER_TABLE_VARIANT } from "@/lib/constant/constant";
import { useState, useRef } from "react";
import { Button } from "@/components/ui/button";

export default function Users() {
  const [selectedEmails, setSelectedEmails] = useState<string[]>([]);
  const [isProcessing, setIsProcessing] = useState(false);
  const tableDataRef = useRef<any>(null);

  const handleSelectionChange = (ids: (string | number)[]) => {
    const filteredEmails = ids.filter((id): id is string => typeof id === 'string');
    setSelectedEmails(filteredEmails);
  };

  const handleBlockAccounts = async () => {
    if (selectedEmails.length === 0) return;
  
    setIsProcessing(true);
    try {
      const response = await fetch("/api/blockUsers", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ emails: selectedEmails }),
      });
      const data = await response.json();
  
      if (response.ok && data.success) {
        console.log("Comptes bloqués avec succès");
        setSelectedEmails([]);
        tableDataRef.current?.refetch();
      } else {
        console.error("Erreur lors du blocage :", data.message);
        throw new Error(data.message);
      }
    } catch (error) {
      console.error("Erreur lors du blocage :", error);
    } finally {
      setIsProcessing(false);
    }
  };

  const handleUnblockAccounts = async () => {
    if (selectedEmails.length === 0) return;
  
    setIsProcessing(true);
    try {
      const response = await fetch("/api/unblockUsers", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ emails: selectedEmails }),
      });
      const data = await response.json();
  
      if (response.ok && data.success) {
        console.log("Comptes débloqués avec succès");
        setSelectedEmails([]);
        tableDataRef.current?.refetch();
      } else {
        console.error("Erreur lors du déblocage :", data.message);
        throw new Error(data.message);
      }
    } catch (error) {
      console.error("Erreur lors du déblocage :", error);
    } finally {
      setIsProcessing(false);
    }
  };

  return (
    <div className="flex mt-10 flex-col w-3/4 mx-auto h-auto md:w-11/12 md:pl-5 md:pt-2">

      {selectedEmails.length > 0 && (
        <div className="flex md:flex-row flex-col gap-4 justify-center md:mt-4 mb-4">
          <Button onClick={handleBlockAccounts} className="bg-red-500 text-white hover:bg-red-400">
            {isProcessing ? "Blocage en cours..." : "Bloquer les comptes"}
          </Button>
          <Button onClick={handleUnblockAccounts} className="bg-green-500 text-white md:ml-2 hover:bg-green-400">
            {isProcessing ? "Déblocage en cours..." : "Débloquer les comptes"}
          </Button>
        </div>
      )}
      <TableData
        ref={tableDataRef}
        variantProps={USER_TABLE_VARIANT}
        checkbox={true}
        onSelectionChange={handleSelectionChange} // Utilise la nouvelle fonction ici
        selectedIds={selectedEmails}
      />
    </div>
  );
}
