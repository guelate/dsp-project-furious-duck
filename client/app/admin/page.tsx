"use client";

import AdminLine1 from "@/components/AdminLine1";
import AdminLine2 from "@/components/AdminLine2";
import Statistique from "@/components/Statistique";

export default function Admin() {
  return (
    <div className="flex flex-col gap-14 pr-10 md:pr-0">
      <AdminLine1 />
      <AdminLine2 />
    </div>
  );
}


