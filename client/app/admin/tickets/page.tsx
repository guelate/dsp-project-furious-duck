"use client"

import TableData from "@/components/TableData";
import { TICKET_TABLE_VARIANT } from "@/lib/constant/constant";

export default function tabb(){
    return(
        <div className="flex mt-10 flex-col w-3/4 mx-auto h-auto md:w-11/12 md:pl-5 md:pt-2">
            <TableData variantProps={TICKET_TABLE_VARIANT} checkbox={true}  onSelectionChange={() => {}} selectedIds={[]} />
        </div>
    );
}

{/* <div className="flex mt-10 flex-col ml-10 md:ml-72 h-auto w-3/4 md:pl-5 md:pt-2"> */}
