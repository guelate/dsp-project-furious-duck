"use client";

import TableData from "@/components/TableData";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";
import { USER_TABLE_VARIANT } from "@/lib/constant/constant";
import { useState } from "react";

export default function Campaign() {
  const [subject, setSubject] = useState("");
  const [campaignName, setCampaignName] = useState("");
  const [message, setMessage] = useState("");
  const [isSending, setIsSending] = useState(false);
  const [selectedEmails, setSelectedEmails] = useState<(string | number)[]>([]); // Changer le type ici

  const handleSelectionChange = (ids: (string | number)[]) => {
    const emails = ids.map(id => String(id)); // Convertir en string si nécessaire
    setSelectedEmails(emails); // Appeler setSelectedEmails avec les emails
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    setIsSending(true);

    try {
      const response = await fetch("/api/sendCampaignEmail", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          campaignName,
          subject,
          message,
          emails: selectedEmails,
        }),
      });

      if (!response.ok) {
        throw new Error("Erreur réseau ou serveur");
      }

      const data = await response.json();

      if (data.success) {
        console.log("Emails envoyés avec succès");
        setSubject("");
        setMessage("");
        setCampaignName("");
        setSelectedEmails([]);
      } else {
        console.error("Erreur lors de l'envoi des emails", data.error);
      }
    } catch (error) {
      console.error("Erreur réseau ou serveur :", error);
    } finally {
      setIsSending(false);
    }
  };

  return (
    <div className="flex mt-10 flex-col w-3/4 mx-auto h-auto md:w-11/12 md:pl-5 md:pt-2">
      <div className="flex flex-col md:flex-row">
        <TableData 
          variantProps={USER_TABLE_VARIANT} 
          checkbox={true} 
          onSelectionChange={handleSelectionChange} // Utiliser la fonction wrapper
          selectedIds={selectedEmails}
        />
        <form onSubmit={handleSubmit} className="space-y-4 w-full max-w-md mx-auto p-4">
          <div className="space-y-2">
            <label htmlFor="campaign" className="text-sm font-medium">Nom de la campagne</label>
            <Input
              id="campaign"
              value={campaignName}
              onChange={(e) => setCampaignName(e.target.value)}
              placeholder="Entrez le  nom de la campagne"
              required
            />
          </div>
          <div className="space-y-2">
            <label htmlFor="subject" className="text-sm font-medium">Sujet</label>
            <Input
              id="subject"
              value={subject}
              onChange={(e) => setSubject(e.target.value)}
              placeholder="Entrez le sujet de l'email"
              required
            />
          </div>
          <div className="space-y-2">
            <label htmlFor="message" className="text-sm font-medium">Message</label>
            <Textarea
              id="message"
              value={message}
              onChange={(e) => setMessage(e.target.value)}
              placeholder="Tapez votre message ici"
              required
              className="min-h-[150px]"
            />
          </div>
          <Button type="submit" className="w-full" disabled={isSending || selectedEmails.length === 0}>
            {isSending ? "Envoi en cours..." : "Envoyer les emails"}
          </Button>
        </form>
      </div>
    </div>
  );
}
