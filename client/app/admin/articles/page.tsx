"use client";

import CardArticle from "@/components/CardArticle";
import PASFINIT from "../../../public/PASFINIT.png";

export default function Article() {
  //TODO: FETCH DATA FROM ARTICLE TABLE TO SHOW THEM.

  return (
    <div className="flex md:flex-col w-full gap-10 py-5">
      <div className="flex md:flex-row md:justify-between">
        <CardArticle
          id="ARTICLE 1"
          title="ARTICLE 1"
          content="PAS FINIT"
          imageUrl={PASFINIT}
        />
        <CardArticle
          id="ARTICLE 2"
          title="ARTICLE 2"
          content=""
          imageUrl={PASFINIT}
        />
      </div>

      <div className="flex md:flex-row">
        <CardArticle
          id="ARTICLE 3"
          title="ARTICLE 3"
          content="PAS FINIT"
          imageUrl={PASFINIT}
        />
        <CardArticle
          id="ARTICLE 4"
          title="ARTICLE 4"
          content="PAS FINIT"
          imageUrl={PASFINIT}
        />
      </div>
    </div>
  );
}
