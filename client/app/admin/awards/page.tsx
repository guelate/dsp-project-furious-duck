"use client";

import TableData from "@/components/TableData"; // Utilise ton composant TableData
import { PRICE_TABLE_VARIANT } from "@/lib/constant/constant"; // Constante spécifique pour le tableau des prix
import { useState, useRef } from "react";
import { Button } from "@/components/ui/button";

export default function Awards() {
  const [selectedClaimIds, setSelectedClaimIds] = useState<(string | number)[]>([]); // Accepte les deux types
  const [isProcessing, setIsProcessing] = useState(false);
  const tableDataRef = useRef<any>(null);

  const handleMarkAsClaimed = async () => {
    if (selectedClaimIds.length === 0) return;

    setIsProcessing(true);
    try {
      const response = await fetch("/api/markPrizesAsClaimed", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ claimIds: selectedClaimIds }), // Envoie les claim_ids
      });
      const data = await response.json();

      if (response.ok && data.success) {
        console.log("Prix marqués comme remis avec succès");
        setSelectedClaimIds([]); // Réinitialise la sélection après le succès
        tableDataRef.current?.refetch(); // Rafraîchit les données du tableau
      } else {
        console.error("Erreur lors de la mise à jour :", data.message);
        throw new Error(data.message);
      }
    } catch (error) {
      console.error("Erreur lors de la mise à jour des prix :", error);
    } finally {
      setIsProcessing(false);
    }
  };

  return (
    <div className="flex mt-10 flex-col mx-auto w-3/4 h-auto md:w-11/12 md:pl-5 md:pt-2">
      {/* Boutons de traitement des prix sélectionnés */}
      {selectedClaimIds.length > 0 && (
        <div className="flex md:flex-row flex-col gap-4 justify-center md:mt-4 mb-4">
          <Button
            onClick={handleMarkAsClaimed}
            className="bg-green-500 text-white hover:bg-green-400"
          >
            {isProcessing ? "Traitement en cours..." : "Marquer comme remis"}
          </Button>
        </div>
      )}

      {/* TableData pour afficher les prix */}
      <TableData
        ref={tableDataRef}
        variantProps={PRICE_TABLE_VARIANT} // Variante du tableau pour les prix
        checkbox={true} // Active les cases à cocher pour la sélection
        onSelectionChange={setSelectedClaimIds} // Gère la sélection des claim_ids
        selectedIds={selectedClaimIds} // Passe les IDs sélectionnés
      />
    </div>
  );
}
