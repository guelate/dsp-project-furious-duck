import Checkout from '@/components/Checkout';

export default function CaissePage() {
  return (
    <div>
      <h1>Page de caisse</h1>
      <Checkout />
    </div>
  );
}