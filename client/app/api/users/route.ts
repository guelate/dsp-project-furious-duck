export const dynamic = 'force-dynamic'; // Forcer le rendu dynamique

import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export async function GET(req: Request) {
  try {
    const { searchParams } = new URL(req.url);
    const page = parseInt(searchParams.get('page') || '1', 10);
    const size = parseInt(searchParams.get('size') || '10', 10);
    const searchTerm = searchParams.get('searchTerm') || ''; // Récupère le terme de recherche
    const skip = (page - 1) * size;

    // Appliquer le filtre de recherche//
    const [users, totalCount] = await Promise.all([
      prisma.user.findMany({
        where: {
          firstname: {
            contains: searchTerm, // Filtrer par prénom contenant le terme de recherche
            mode: 'insensitive',
          },
        },
        skip,
        take: size,
      }),
      prisma.user.count({
        where: {
          firstname: {
            contains: searchTerm,
            mode: 'insensitive',
          },
        },
      }),
    ]);

    return NextResponse.json({
      data: users,
      totalItems: totalCount,
    });
  } catch (error) {
    console.error('Erreur lors de la récupération des utilisateurs:', error);
    return NextResponse.json({ message: 'Erreur serveur' }, { status: 500 });
  }
}
