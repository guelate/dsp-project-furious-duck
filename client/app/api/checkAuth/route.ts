import { NextResponse } from 'next/server';
import { prisma } from "@/lib/db/prisma/prisma";
import bcrypt from 'bcryptjs';

export async function POST(request: Request) {
  try {
    const { firstname, password } = await request.json();

    if (!firstname || !password) {
      return NextResponse.json({ message: 'Identifiant ou mot de passe manquant.' }, { status: 400 });
    }

    // Recherche dans la table "user"
    const user = await prisma.user.findFirst({
      where: {
        firstname: {
          equals: firstname,
          mode: "insensitive",
        },
      },
    });

    // Recherche dans la table "admin"
    const admin = await prisma.admin.findFirst({
      where: {
        firstname: {
          equals: firstname,
          mode: "insensitive",
        },
      },
    });

    let isValidPassword;
    let userData;

    if (user) {
      isValidPassword = await bcrypt.compare(password, user.password_hash);
      if (isValidPassword) {
        userData = {
          id: user.user_id,
          firstname: user.firstname,
          lastname: user.lastname,
          email: user.email,
          role: user.role,
          is_active: user.is_active,
        };
      }
    }

    if (admin) {
      isValidPassword = await bcrypt.compare(password, admin.password_hash);
        userData = {
          id: admin.admin_id,
          firstname: admin.firstname,
          lastname: admin.lastname,
          email: admin.email,
          role: "admin",
      }
    }

    if (!userData) {
      return NextResponse.json({ message: 'Identifiant ou mot de passe incorrect.' }, { status: 401 });
    }

    return NextResponse.json(userData, { status: 200 });
    
  } catch (error) {
    console.error('Erreur lors de la connexion :', error);
    return NextResponse.json({ message: 'Erreur serveur.' }, { status: 500 });
  }
}
