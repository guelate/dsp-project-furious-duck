import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';

// Crée une instance de PrismaClient
const prisma = new PrismaClient();

export async function POST(request: Request) {
  try {
    const body = await request.json();
    const { emails } = body; // Récupère les emails envoyés dans la requête

    // Vérifie que la liste des emails n'est pas vide
    if (!emails || emails.length === 0) {
      return NextResponse.json({ message: "Aucun utilisateur sélectionné.", success: false }, { status: 400 });
    }

    // Met à jour le statut des utilisateurs sélectionnés
    const updateResult = await prisma.user.updateMany({
      where: {
        email: {
          in: emails,
        },
      },
      data: {
        is_active: false, // Bloque les comptes
      },
    });

    // Vérifie le résultat de la mise à jour
    if (updateResult.count === 0) {
      return NextResponse.json({ message: "Aucun utilisateur trouvé à mettre à jour.", success: false }, { status: 404 });
    }

    return NextResponse.json({ message: `${updateResult.count} utilisateur(s) bloqué(s).`, success: true }, { status: 200 });
  } catch (error) {
    console.error("Erreur lors du blocage des utilisateurs :", error);
    return NextResponse.json({ message: "Erreur interne du serveur", success: false }, { status: 500 });
  } finally {
    await prisma.$disconnect(); // Déconnecte Prisma pour libérer les ressources
  }
}
