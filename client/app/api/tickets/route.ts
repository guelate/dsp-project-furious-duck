export const dynamic = 'force-dynamic'; // Forcer le rendu dynamique

import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export async function GET(req: Request) {
  try {
    const { searchParams } = new URL(req.url);
    const page = parseInt(searchParams.get('page') || '1', 10);
    const size = parseInt(searchParams.get('size') || '10', 10);
    const searchTerm = searchParams.get('searchTerm') || '';
    const skip = (page - 1) * size;

    // Récupérer les tickets paginés
    const [tickets, totalCount] = await Promise.all([
      prisma.ticket.findMany({
        skip,
        take: size,
        include: {
          user: {
            select: {
              firstname: true, // Récupérer uniquement le prénom de l'utilisateur
            },
          },
        },
        where: {
          ticket_code: {
            contains: searchTerm, // Filtrer par prénom contenant le terme de recherche
            mode: 'insensitive',
          },
        },
      }),
      prisma.ticket.count(),
    ]);

    // Récupérer les utilisateurs distincts ayant un ticket avec un statut autre que 'unused'
    const distinctParticipants = await prisma.ticket.findMany({
      where: {
        status: {
          not: 'non utilisé',
        },
      },
      select: {
        user_id: true,
      },
      distinct: ['user_id'],
    });

    const participantsCount = distinctParticipants.length;
    

    // Total des utilisateurs inscrits
    const totalUsers = await prisma.user.count();

    // Utilisateurs non-participants
    const nonParticipantsCount = totalUsers - participantsCount;

    // Tickets restants avec le statut 'unused'
    const ticketsRestant = await prisma.ticket.count({
      where: { status: 'non utilisé' },
    });

    return NextResponse.json({
      participants: participantsCount,
      nonParticipants: nonParticipantsCount,
      ticketsRestant,
      totalUsers,
      data: tickets,
      totalItems: totalCount,
    });
  } catch (error) {
    console.error('Erreur lors de la récupération des tickets:', error);
    return NextResponse.json({ message: 'Erreur serveur' }, { status: 500 });
  }
}
