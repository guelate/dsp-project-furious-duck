import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export async function POST(request: Request) {
  try {
    const body = await request.json();
    const { emails } = body;

    if (!emails || emails.length === 0) {
      return NextResponse.json({ message: "Aucun utilisateur sélectionné." }, { status: 400 });
    }

    const updateResult = await prisma.user.updateMany({
      where: {
        email: {
          in: emails,
        },
      },
      data: {
        is_active: true,
      },
    });

    if (updateResult.count === 0) {
      return NextResponse.json({ message: "Aucun utilisateur trouvé." }, { status: 404 });
    }

    return NextResponse.json({ success: true, message: `${updateResult.count} utilisateur(s) débloqué(s).` }, { status: 200 });
  } catch (error) {
    console.error("Erreur lors du déblocage :", error);
    return NextResponse.json({ message: "Erreur interne du serveur" }, { status: 500 });
  } finally {
    await prisma.$disconnect();
  }
}
