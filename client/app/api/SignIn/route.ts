import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';
import { logActivity } from "../../lib/services/logService";

const prisma = new PrismaClient();

export async function POST(request: Request) {
    try {


        const body = await request.json();
        const { firstname, password } = body;


        const user = await prisma.user.findFirst({
            where: {
                firstname: firstname, // ou identifiant si tu utilises un autre champ
            },
        });

        if (!user) {
            // Si l'utilisateur n'est pas trouvé, enregistrer une tentative échouée
            return NextResponse.json({ message: 'Utilisateur non reconnu. Veuillez réessayer.' }, { status: 401 });
        }
  
        await logActivity("Tentative de connexion");

        // Logique de traitement ici (par exemple, enregistrer dans une base de données)
        // ...
        return NextResponse.json({ message: `User ${firstname} with email ${password} created successfully!` }, { status: 200 });
    } catch (error) {
        return NextResponse.json({ message: 'Something went wrong' }, { status: 500 });
    }
}

//si user est reconnu -> update view with this section
//si user est reconnu -> with status false = banni -> Votre compte à ete banni
//si user no reconnu -> message user non reconnu veillez reessayer


// try {
//     // Lire le corps de la requête
//     const body = await request.json();

//     const { identifiant, password } = body;


//     // Logique de traitement ici (par exemple, enregistrer dans une base de données)
//     // ...

//     // Répondre avec un message de succès
//     return NextResponse.json({ message: `User ${identifiant} with email ${password} created successfully!` }, { status: 200 });
// } catch (error) {
//     // En cas d'erreur, retourner une réponse avec un message d'erreur
//     return NextResponse.json({ message: 'Something went wrong' }, { status: 500 });
// }