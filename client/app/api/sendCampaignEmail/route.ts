import { NextResponse } from "next/server";
import { resend } from "../../../public/src/lib/resend"; // Assurez-vous que ce chemin est correct
import { MagicLinkEmail } from "../../../emails/MagicLink"; // Vérifiez que le chemin est correct
import { logActivity } from "../../lib/services/logService";
import { createEmailingCampaign } from "../../lib/services/emailCampaignService";

export async function POST(req: Request) {
  console.log("Requête HTTP reçue avec la méthode POST");

  const { campaignName, subject, message, emails } = await req.json(); // Utilisez req.json() pour obtenir le body dans Next.js 13

  try {

    // Crée une campagne d'emailing
    await createEmailingCampaign(campaignName, message, subject, emails);

    // Envoie des emails avec Resend
    const result = await sendEmails(subject, message, emails);

    // Log l'activité d'envoi d'email
    await logActivity("Envoie mail admin");

    return NextResponse.json({ success: true, result});
  } catch (error: unknown) {
    console.error("Erreur lors de l'envoi des emails :", error);
    return NextResponse.json(
      { success: false, error: (error as Error).message },
      { status: 500 }
    );
  }
}

// Fonction pour l'envoi d'emails avec Resend
async function sendEmails(subject: string, message: string, emails: string[]) {
  try {
    const sendPromises = emails.map((email) =>
      resend.emails.send({
        from: "TheTipTop Support <not-reply-support@dsp5-archi-o22a-4-5-g3.fr>",
        to: email,
        subject: subject,
        react: MagicLinkEmail({
          magicLink: "https://dsp5-archi-o22a-4-5-g3.fr/",
          message: message,
        }),
      })
    );

    // Attend que tous les emails soient envoyés
    const results = await Promise.all(sendPromises);

    return results;
  } catch (error) {
    console.error("Erreur lors de l'envoi via Resend :", error);
    throw error;
  }
}
