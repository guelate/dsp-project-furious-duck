import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

// Fonction pour sélectionner un ticket non utilisé de manière aléatoire
async function selectRandomUnusedTicket() {
  const count = await prisma.ticket.count({ where: { status: 'unused' } });
  const skip = Math.floor(Math.random() * count);
  const ticket = await prisma.ticket.findFirst({
    where: { status: 'unused' },
    skip: skip,
    include: { prize: true },
  });
  return ticket;
}

export async function POST(request: Request) {
  try {
    const { amount, userId } = await request.json();

    if (amount < 49) {
      return NextResponse.json({ message: "Montant insuffisant pour obtenir un ticket" }, { status: 200 });
    }

    // Sélectionner un ticket non utilisé aléatoirement
    const ticket = await selectRandomUnusedTicket();
    if (!ticket) {
      return NextResponse.json({ error: "Aucun ticket disponible" }, { status: 400 });
    }

    // Mettre à jour le ticket avec l'ID de l'utilisateur
    const updatedTicket = await prisma.ticket.update({
      where: { ticket_id: ticket.ticket_id },
      data: { 
        user_id: userId,
        status: 'assigned',
      },
      include: { prize: true },
    });

    return NextResponse.json({
      message: "Ticket attribué avec succès",
      ticketCode: updatedTicket.ticket_code,
      prizeName: updatedTicket.prize.prize_name
    }, { status: 200 });

  } catch (error) {
    console.error("Erreur lors de l'attribution du ticket:", error);
    return NextResponse.json({ error: "Erreur interne du serveur" }, { status: 500 });
  }
}