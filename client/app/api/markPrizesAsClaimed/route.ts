import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';

// Crée une instance de PrismaClient
const prisma = new PrismaClient();

export async function POST(request: Request) {
  try {
    const body = await request.json();
    const { claimIds } = body; // Récupère les IDs de réclamation envoyés dans la requête

    // Vérifie que la liste des claimIds n'est pas vide
    if (!claimIds || claimIds.length === 0) {
      return NextResponse.json({ message: "Aucun prix sélectionné.", success: false }, { status: 400 });
    }

    // Met à jour le statut des récompenses pour les réclamations sélectionnées
    const updateResult = await prisma.prizeClaim.updateMany({
      where: {
        claim_id: {
          in: claimIds, // Vérifie si les IDs correspondent
        },
      },
      data: {
        status: 'récupéré', // Met à jour le statut de la récompense en "remis"
      },
    });

    // Vérifie le résultat de la mise à jour
    if (updateResult.count === 0) {
      return NextResponse.json({ message: "Aucune réclamation trouvée ou aucune récompense à mettre à jour.", success: false }, { status: 404 });
    }

    return NextResponse.json({ message: `${updateResult.count} récompense(s) mise(s) à jour comme remise(s).`, success: true }, { status: 200 });
  } catch (error) {
    console.error("Erreur lors de la mise à jour des récompenses :", error);
    return NextResponse.json({ message: "Erreur interne du serveur", success: false }, { status: 500 });
  } finally {
    await prisma.$disconnect(); // Déconnecte Prisma pour libérer les ressources
  }
}
