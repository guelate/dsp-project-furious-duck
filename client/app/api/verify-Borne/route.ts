import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export async function GET(request: Request) {
  try {
    const { searchParams } = new URL(request.url);
    const code = searchParams.get('code');

    if (!code) {
      return NextResponse.json({ error: "Code manquant" }, { status: 400 });
    }

    const ticket = await prisma.ticket.findUnique({
      where: { ticket_code: code },
      include: { prize: true, user: true },
    });

    if (!ticket) {
      return NextResponse.json({ error: "Code non trouvé" }, { status: 404 });
    }

    if (ticket.status === 'used') {
      return NextResponse.json({ error: "Ce ticket a déjà été utilisé" }, { status: 400 });
    }

    // Marquer le ticket comme utilisé
    const updatedTicket = await prisma.ticket.update({
      where: { ticket_id: ticket.ticket_id },
      data: { 
        status: 'used',
        used_at: new Date(),
      },
    });

    // Créer une réclamation de prix
    await prisma.prizeClaim.create({
      data: {
        user_id: ticket.user_id!,
        ticket_id: ticket.ticket_id,
        prize_id: ticket.prize_id,
        status: 'CLAIMED',
      },
    });

    // Mettre à jour la quantité restante du prix
    await prisma.prize.update({
      where: { prize_id: ticket.prize_id },
      data: { remaining_quantity: { decrement: 1 } },
    });

    return NextResponse.json({
      message: "Code valide",
      prize: ticket.prize.prize_name,
      user: ticket.user ? {
        firstname: ticket.user.firstname,
        lastname: ticket.user.lastname,
      } : null,
    });

  } catch (error) {
    console.error("Erreur lors de la vérification du code:", error);
    return NextResponse.json({ error: "Erreur interne du serveur" }, { status: 500 });
  }
}