import { NextResponse } from "next/server";
import { prisma } from "@/lib/db/prisma/prisma"; // Assure-toi que le chemin est correct

export async function GET(req: Request) {
  try {
    const countEmailAdmin = await prisma.log.count({
      where: {
        action_type: "Envoie mail admin",
      },
    });
    const countEmailClient = await prisma.log.count({
      where: {
        action_type: "Envoie mail client",
      },
    });
    const countTI = await prisma.log.count({
        where: {
          action_type: "Tentative d'inscription",
        },
    });
    const countTC = await prisma.log.count({
        where: {
          action_type: "Tentative de connexion",
        },
    });
    const countTP = await prisma.log.count({
      where: {
        action_type: "Tentative de participation",
      },
  });
    
    return NextResponse.json({ countEmailAdmin, countEmailClient, countTI, countTC, countTP });
  } catch (error) {
    console.error("Erreur lors de la récupération du nombre de logs d'emailing:", error);
    return NextResponse.json({ message: "Erreur serveur" }, { status: 500 });
  }
}
