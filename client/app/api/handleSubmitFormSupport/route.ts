import { NextRequest, NextResponse } from 'next/server';
import { handleSubmitFormSupport } from '@/actions/handleSubmitFormSupport';
import { logActivity } from "../../lib/services/logService";


export async function POST(req: Request) {
  try {
    const formData = await req.json(); // Utilisez req.json() pour des données JSON
    const result = await handleSubmitFormSupport(formData);
    await logActivity("Envoie mail client");
    return new Response(JSON.stringify(result), { status: 200, headers: { 'Content-Type': 'application/json' } });
  } catch (error) {
    console.error("Erreur dans le traitement de la requête :", error);
    return new Response(JSON.stringify({ success: false, error: "Erreur serveur" }), { status: 500, headers: { 'Content-Type': 'application/json' } });
  }
}


