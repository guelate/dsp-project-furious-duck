export const dynamic = 'force-dynamic'; // Forcer le rendu dynamique

import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';
import { getServerSession } from 'next-auth/next'; // Importer getServerSession
import { authOptions } from '@/lib/utilities/authOptions'; // Assurez-vous que votre configuration d'authentification est correcte

const prisma = new PrismaClient();

export async function GET(req: Request) {
  try {
    // Utiliser getServerSession pour récupérer la session utilisateur
    const session = await getServerSession(authOptions);

    if (!session || !session.user.id) {
      return NextResponse.json({ message: 'Unauthorized' }, { status: 401 });
    }

    const { searchParams } = new URL(req.url);
    const page = parseInt(searchParams.get('page') || '1', 10);
    const size = parseInt(searchParams.get('size') || '10', 10);
    const searchTerm = searchParams.get('searchTerm') || '';
    const skip = (page - 1) * size;

    // Récupérer les réclamations de prix associées à l'utilisateur connecté
    const [prizeClaims, totalCount] = await Promise.all([
      prisma.prizeClaim.findMany({
        skip,
        take: size,
        include: {
          user: {
            select: {
              email: true,
            },
          },
          prize: {
            select: {
              prize_name: true,
            },
          },
          ticket: {
            select: {
              ticket_code: true,
            },
          },
        },
        where: {
          user_id: session.user.id, // Filtrer par ID de l'utilisateur connecté
          ticket: {
            ticket_code: {
              contains: searchTerm, // Filtrer par ticket_code contenant le terme de recherche
              mode: 'insensitive', // Pour une recherche insensible à la casse
            },
          },
        },
      }),
      prisma.prizeClaim.count({
        where: {
          user_id: session.user.id, // Compter uniquement les réclamations de l'utilisateur connecté
        },
      }),
    ]);

    const NotReceive = await prisma.prizeClaim.count({
      where: {
        user_id: session.user.id, // Compter les réclamations non reçues de l'utilisateur connecté
        status: 'Non remis',
      },
    });

    const receive = totalCount - NotReceive;

    return NextResponse.json({
      receive,
      NotReceive,
      data: prizeClaims,
      totalItems: totalCount,
    });
  } catch (error) {
    console.error('Erreur lors de la récupération des réclamations de prix:', error);
    return NextResponse.json({ message: 'Erreur serveur' }, { status: 500 });
  }
}
