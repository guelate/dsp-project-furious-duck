//TODO: bycript, prisma, nextResponse
//TODO: -> request json, console.log(body)
//TODO: exist user
//TODO: haspassword
//TODO: create user
//TODO: return user -> json()

// import { NextResponse } from 'next/server';
// import { PrismaClient } from '@prisma/client';
// import bcrypt from "bcryptjs"; // Recommande bcryptjs pour éviter les problèmes liés à certaines plateformes

// const prisma = new PrismaClient();

// export async function POST(request: Request) {
//   try {
//     const body = await request.json();
//     const { firstname, lastname, email, password, date_of_birth, gender, address, city, postal_code, phone_number } = body;

//     // Vérification des duplicatas par email ou numéro de téléphone uniquement
//     const existingUser = await prisma.user.findFirst({
//       where: {
//         OR: [
//           { email },
//           { phone_number },
//         ],
//       },
//     });

//     if (existingUser) {
//       return NextResponse.json(
//         { message: "Un compte avec cet email ou numéro de téléphone existe déjà." },
//         { status: 409 }
//       );
//     }

//     // Hachage du mot de passe
//     const hashedPassword = await bcrypt.hash(password, 10);

//     // Création du nouvel utilisateur
//     const newUser = await prisma.user.create({
//       data: {
//         firstname,
//         lastname,
//         email,
//         password_hash: hashedPassword, // Utilisation du mot de passe haché
//         date_of_birth,
//         gender,
//         address,
//         city,
//         postal_code,
//         phone_number,
//         is_active: true,
//       },
//     });

//     return NextResponse.json(
//       { message: "Inscription réussie", user: newUser },
//       { status: 201 }
//     );
//   } catch (error) {
//     console.error("Erreur lors de l'inscription :", error);
    

//     return NextResponse.json(
//       { message: "Erreur interne du serveur" }, 
//       { status: 500 }
//     );
//   }
// }



import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';
import { logActivity } from "../../lib/services/logService";

import bcrypt from 'bcryptjs';

const prisma = new PrismaClient();

export async function POST(request: Request) {
  try {
    const body = await request.json();
    const {
      firstname,
      lastname,
      email,
      password_hash,
      date_of_birth,
      gender,
      address,
      city,
      postal_code,
      phone_number,
    } = body;

    // Conversion de date_of_birth en Date
    const parsedDateOfBirth = new Date(date_of_birth);

    // Vérification des duplicatas par email, prénom, ou numéro de téléphone
    const existingUser = await prisma.user.findFirst({
      where: {
        OR: [
          { email },
          { firstname },
          { phone_number },
        ],
      },
    });

    // Si l'utilisateur existe déjà avec les mêmes informations
    if (existingUser) {
      return NextResponse.json({
        message: "Un compte avec cet email, prénom ou numéro de téléphone existe déjà.",
      }, { status: 409 });
    }

    // Hachage du mot de passe
    const hashedPassword = await bcrypt.hash(password_hash, 10);

    // Création d'un nouvel utilisateur
    const newUser = await prisma.user.create({
      data: {
        firstname,
        lastname,
        email,
        password_hash: hashedPassword,
        date_of_birth: parsedDateOfBirth,
        gender,
        address,
        city,
        postal_code,
        phone_number,
        is_active: true,
        auth_provider: "local", // Par défaut, à ajuster selon tes besoins
        role: "user", // Par défaut, à ajuster selon tes besoins
      },
    });

    // Log l'activité d'envoi d'email
    await logActivity("Tentative d'inscription");

    // Répondre avec les informations de l'utilisateur nouvellement créé
    return NextResponse.json({ message: "Inscription réussie", user: newUser }, { status: 201 });
  } catch (error) {
    console.error("Erreur lors de l'inscription :", error);
    return NextResponse.json({ message: "Erreur interne du serveur" }, { status: 500 });
  }
}



//CONDITION
//si user exist (by: email, name, phone) -> un utilisateur contient de x(valeur par by)
//si user not exist -> create and update session like signIn with user value 
