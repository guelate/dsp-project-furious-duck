import { NextResponse } from "next/server";

import { prisma } from "@/lib/db/prisma/prisma";

export async function GET(req: Request) {
  try {
    const prizes = await prisma.prize.findMany();
    return NextResponse.json(prizes);
  } catch (error) {
    console.error("Erreur lors de la récupération des prix:", error);
    return NextResponse.json({ message: "Erreur serveur" }, { status: 500 });
  }
}

