export const dynamic = 'force-dynamic'; // Forcer le rendu dynamique

import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export async function GET(req: Request) {
  try {
    const { searchParams } = new URL(req.url);
    const page = parseInt(searchParams.get('page') || '1', 10);
    const size = parseInt(searchParams.get('size') || '10', 10);
    const searchTerm = searchParams.get('searchTerm') || '';
    const skip = (page - 1) * size;

    // Récupérer les réclamations de prix avec pagination et relations utilisateur et prix
    const [prizeClaims, totalCount] = await Promise.all([
      prisma.prizeClaim.findMany({
        skip,
        take: size,
        include: {
          user: {
            select: {
              email: true,
            },
          },
          prize: {
            select: {
              prize_name: true,
            },
          },
          ticket: {
            select: {
              ticket_code: true,
            },
          },
        },
        where: {
          ticket: {
            ticket_code: {
              contains: searchTerm, // Filtrer par ticket_code contenant le terme de recherche
              mode: 'insensitive',  // Pour une recherche insensible à la casse
            },
          },
        },
      }),
      prisma.prizeClaim.count(),
    ]);

    const NotReceive = await prisma.prizeClaim.count({
      where: { status: 'Non remis' },
    });

    const receive = totalCount - NotReceive;

    return NextResponse.json({
      receive,
      NotReceive,
      data: prizeClaims,
      totalItems: totalCount,
    });
  } catch (error) {
    console.error('Erreur lors de la récupération des récompenses:', error);
    return NextResponse.json({ message: 'Erreur serveur' }, { status: 500 });
  }
}
