"use server"

import { PrismaClient } from "@prisma/client";
import { NextResponse } from "next/server";
import { getServerSession } from "next-auth"; 
import { logActivity } from "../../lib/services/logService";
import { createPrizeClaim } from "../../lib/utilities/createPrizeClaim";
import { getToken } from "next-auth/jwt";

const prisma = new PrismaClient();

console.log("entrer dans verif ticket")

export async function POST(request: Request) {
  const { code } = await request.json(); 
  const session = await getServerSession();
 
  console.log("la sessionTOken",session)


  console.log("Le code reçu est :", code);

  if (!session || !session.user) {
    console.log("Aucune session valide trouvée.");
    return NextResponse.json({ message: "Utilisateur non connecté ou session invalide." });
  }


  try {
 
    const user = await prisma.user.findUnique({
      where: { email: session.user.email },
    });
    console.log("affiche",user)

    const ticket = await prisma.ticket.findUnique({
      where: { ticket_code: code }, 
      include: {
        prize: true,
        user: true, 
        prize_claims: true,
      },
    });

    if (ticket) {

    //  console.log("gerer la section pour un compte gmail",session?.role)

      if (!user) {
        return NextResponse.json({ message: "Utilisateur non trouvé." });
      }

      // Vérifier si le ticket a déjà été utilisé
      if (ticket.status === "utilisé") {
        console.log("Le ticket a déjà été utilisé.");
        return NextResponse.json({ message: "Ce ticket a déjà été utilisé et ne peut plus être réutilisé." });
      }

  
      await logActivity("Tentative de participation");
      
     

        console.log("il y a un user")
        const updatedTicket = await prisma.ticket.update({
          where: { ticket_id: ticket.ticket_id },
          data: {
            user_id: user.user_id, 
            status: "utilisé",
            used_at: new Date(), 
          },
          include: {
            prize: true, 
            user: true,  
            prize_claims: true, 
          },
        });
        
        console.log("Ticket trouvé:", updatedTicket);
  
        // Créer une entrée dans prizeClaim
        await createPrizeClaim({
          user_id: user.user_id,
          ticket_id: updatedTicket.ticket_id,
          prize_id: ticket.prize_id,
          status: "non récupéré",
        });
        return NextResponse.json({ ticket: updatedTicket });
     


    } else {
      console.log("Ticket non trouvé pour le code :", code);
      return NextResponse.json({ message: "Ticket non trouvé" });
    }
  } catch (error) {
    console.error("Erreur lors de la vérification du ticket:", error);
    return NextResponse.json({ error: "Erreur interne" });
  }
}