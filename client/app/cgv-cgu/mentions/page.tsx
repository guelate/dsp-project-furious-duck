export default function MentionsLegales() {
  return (
      <div className="flex flex-col items-center px-10 py-20 relative z-0 md:px-10">
          <div className="w-full max-w-5xl mx-auto space-y-12 py-12 md:py-16 lg:py-20 sm:px-10 content-center">
          <h1 className="text-4xl font-bold mb-6 text-[#DBB556]">Mentions Légales</h1>
          <h4>
          Conformément aux dispositions des Articles 6-III et 19 de la Loi n°2004-575 du 21 juin 2004 pour la Confiance dans l’économie numérique, dite L.C.E.N., il est porté à la connaissance des utilisateurs et visiteurs, ci-après l"Utilisateur", du site http://dsp5-archi-o22a-4-5-g3.fr/ , ci-après le "Site", les présentes mentions légales.
          La connexion et la navigation sur le Site par l’Utilisateur implique acceptation intégrale et sans réserve des présentes mentions légales.
          Ces dernières sont accessibles sur le Site à la rubrique « Mentions légales ».

          </h4>
          <section className="mb-8">
            <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">1. Éditeur du Site</h2>
            <p>
            L'édition du Site est assurée par Thé Tip TOP Société Anonyme (SA) au capital de 300000 euros, immatriculée au Registre du Commerce et des Sociétés de Paris B 123 456 789 sous le numéro 123456789 dont le siège social est situé au 5 Avenue du Maréchal Joffre, 94120 Fontenay-sous-Bois, 
            Numéro de téléphone 0107080910, 
            Adresse e-mail : Thétiptopg1@gmail.com.
            N° de TVA intracommunautaire : FR12 123456789
            Le Directeur de la publication est Thé Tip Top
            ci-après l'"Editeur".
            </p>
          </section>
          <section className="mb-8">
            <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">2. Hébergement</h2>
            <p>
            L'hébergeur du Site est la société IONOS SARL, dont le siège social est situé au 7, place de la Gare, BP 70109, 57200 Sarreguemines Cedex , avec le numéro de téléphone : 0970808911.
            </p>
          </section>
          <section className="mb-8">
            <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">3. Accès au Site</h2>
            <p>
            Le Site est accessible en tout endroit, 7j/7, 24h/24 sauf cas de force majeure, interruption programmée ou non et pouvant découlant d’une nécessité de maintenance.
            En cas de modification, interruption ou suspension du Site, l'Editeur ne saurait être tenu responsable.
            </p>
          </section>
          <section className="mb-8">
            <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">4. Collecte des données</h2>
            <p>
            Le Site assure à l'Utilisateur une collecte et un traitement d'informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. 
            En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978, l'Utilisateur dispose d'un droit d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'Utilisateur exerce ce droit :
            ·         par mail à l'adresse email thetiptopg1@gmail.com
            ·         par voie postale au 5 Avenue du Maréchal Joffre, 94120 Fontenay-sous-Bois ;
            ·         via un formulaire de contact ;
            ·         via son espace personnel ;
            Toute utilisation, reproduction, diffusion, commercialisation, modification de toute ou partie du Site, sans autorisation de l’Editeur est prohibée et pourra entraîner des actions et poursuites judiciaires telles que notamment prévues par le Code de la propriété intellectuelle et le Code civil.
            Pour plus d’informations, se reporter aux CGU du site http://dsp5-archi-o22a-4-5-g3.fr/ accessible à la rubrique "CGU" 
            </p>
          </section>
          <section className="mb-8">
            <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">5. Protection des données Personnelles (RGPD)</h2>
            <p>
            En conformité avec le Règlement Général sur la Protection des Données (RGPD) en vigueur depuis le 25 mai 2018, le site http://dsp5-archi-o22a-4-5-g3.fr/, ci-après le "Site", assure la protection des données personnelles de l’Utilisateur.
            Responsable de traitementLe responsable du traitement des données est Thé Tip Top, société anonyme (SA), dont les coordonnées sont fournies à l’article 1 des présentes mentions légales.
            Finalité du traitement des donnéesLes données personnelles collectées sur le Site sont uniquement destinées à assurer le bon fonctionnement des services proposés (navigation, gestion des utilisateurs, contacts, etc.).
            Destinataires des donnéesLes données collectées sont destinées exclusivement à l’Editeur et ne seront en aucun cas cédées ou vendues à des tiers, sauf obligation légale.
            Droit d'opposition, d'accès et de rectificationConformément au RGPD et à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, l’Utilisateur dispose des droits suivants concernant ses données personnelles :
            Droit d’accès : connaître les données collectées sur sa personne.
            Droit de rectification : modifier ou compléter ses données.
            Droit d’opposition : refuser le traitement de ses données pour des motifs légitimes.
            Droit à l’effacement : demander la suppression de ses données.
            Ces droits peuvent être exercés en envoyant une demande à l'adresse email thetiptopg1@gmail.com ou par courrier postal à l’adresse mentionnée dans l'article 1.
            Transfert hors de l’Union européenneLes données personnelles de l’Utilisateur ne sont pas transférées hors de l’Union européenne. Si un transfert était nécessaire, l’Editeur s’engage à en informer l’Utilisateur et à garantir un niveau de protection conforme au RGPD.
            Base juridique du traitementLe traitement des données personnelles de l’Utilisateur est fondé sur le consentement explicite de ce dernier ou sur une obligation légale, notamment pour la fourniture de services proposés par le Site.
            </p>
          </section>
        </div>
      </div>
  );
}

