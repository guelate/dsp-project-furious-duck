export default function CGV() {
    return (
        <div className="flex flex-col items-center px-10 py-20 relative z-0 md:px-10">
            <div className="w-full max-w-5xl mx-auto space-y-12 py-12 md:py-16 lg:py-20 sm:px-10 content-center">
            <h1 className="text-4xl font-bold mb-6 text-[#DBB556]">Conditions Générales de Vente (CGV)</h1>
            <section className="mb-8">
              <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">1. Objet</h2>
              <p>
                Les présentes conditions générales de vente (CGV) régissent les ventes réalisées sur le site de Thé Tip Top dans le cadre du jeu-concours. En participant à ce jeu-concours, vous acceptez pleinement les conditions générales de vente décrites ci-après.
              </p>
            </section>
            <section className="mb-8">
              <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">2. Prix</h2>
              <p>
                Les prix des produits sont indiqués en euros toutes taxes comprises (TVA et autres taxes applicables au jour de la commande), sauf indication contraire. Toute commande passée sur le site Thé Tip Top est payable exclusivement en euros.
              </p>
            </section>
            <section className="mb-8">
              <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">3. Commande</h2>
              <p>
                Pour participer au jeu-concours, l'utilisateur doit acheter des produits participants et enregistrer son code sur notre site. Une fois la commande validée, un email de confirmation sera envoyé à l'adresse fournie lors de l'inscription.
              </p>
            </section>
            <section className="mb-8">
              <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">4. Paiement</h2>
              <p>
                Le paiement s'effectue directement en ligne via notre plateforme sécurisée. Les moyens de paiement acceptés sont les cartes bancaires (Visa, MasterCard) et les virements bancaires.
              </p>
            </section>
            <section className="mb-8">
              <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">5. Livraison</h2>
              <p>
                Les produits commandés dans le cadre du jeu-concours sont livrés à l'adresse indiquée par le participant lors de son inscription. Les délais de livraison sont estimés entre 5 et 7 jours ouvrés.
              </p>
            </section>
            <section className="mb-8">
              <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">6. Réclamations et Retours</h2>
              <p>
                En cas de non-conformité des produits ou de réclamations, les participants peuvent nous contacter via la rubrique "Support" du site. Les retours de produits doivent être effectués dans les 14 jours suivant la réception de la commande.
              </p>
            </section>
            <section className="mb-8">
              <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">7. Responsabilité</h2>
              <p>
                Thé Tip Top ne saurait être tenu responsable des dommages indirects qui pourraient survenir du fait de l'achat des produits. La responsabilité de Thé Tip Top est en tout état de cause limitée au montant de la commande.
              </p>
            </section>
          </div>
        </div>
    );
  }
