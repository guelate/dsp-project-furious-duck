export default function CGU() {
  return (
    <div className="flex flex-col items-center px-10 py-20 relative z-0 md:px-10">
      <div className="w-full max-w-5xl mx-auto space-y-12 py-12 md:py-16 lg:py-20 sm:px-10 content-center">
        <h1 className="text-4xl font-bold mb-6 text-[#DBB556]">
          Conditions Générales d'Utilisation (CGU)
        </h1>
        <h4>
        Les présentes conditions générales d'utilisation (dites « CGU ») ont pour objet l'encadrement juridique des modalités de mise à disposition du site et des services par Furious Ducks  et de définir les conditions d’accès et d’utilisation des services par « l'Utilisateur ».

        Les présentes CGU sont accessibles sur le site à la rubrique «CGU».

        Toute inscription ou utilisation du site implique l'acceptation sans aucune réserve ni restriction des présentes CGU par l’utilisateur. Lors de l'inscription sur le site via le Formulaire d’inscription, chaque utilisateur accepte expressément les présentes CGU en cochant la case précédant le texte suivant : « Je reconnais avoir lu et compris les CGU et je les accepte ».
        En cas de non-acceptation des CGU stipulées dans le présent contrat, l'Utilisateur se doit de renoncer à l'accès des services proposés par le site.
        https://dsp5-archi-o22a-4-5-g3.fr  se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes CGU.
        </h4>
        <section className="mb-8">
          <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">
            1. Les mentions légales
          </h2>
          <p>
          L'édition du site https://dsp5-archi-o22a-4-5-g3.fr est assurée par la Société Société Anonyme (SA) Furious Ducks au capital de 150000 euros, immatriculée au RCS de 503 567 890 sous le numéro 503567890, dont le siège social est situé au 18 rue Léon Frot, 75011 Paris
          Numéro de téléphone 0102030405
          Adresse e-mail : furiousduckg3@gmail.com.
          Le Directeur de la publication est : Furious Ducks
          Numéro de TVA intracommunautaire : FR 22 503567890

          L'hébergeur du site https://dsp5-archi-o22a-4-5-g3.fr est la société IONOS SARL, dont le siège social est situé au 7, place de la Gare, BP 70109, 57200 Sarreguemines Cedex, avec le numéro de téléphone : 0970 808 911.
          </p>
        </section>
        <section className="mb-8">
          <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">
            2. Accès au site
          </h2>
          <p>
          Le site https://dsp5-archi-o22a-4-5-g3.fr permet à l'Utilisateur un accès gratuit aux services suivants :
          Le site internet propose les services suivants :
          Présentation du jeu-concours et des lots à gagner 
          Page d'accueil avec une présentation détaillée du jeu-concours
          Description des lots à gagner et leurs pourcentages de répartition 
          Vidéo de présentation du jeu-concours (optionnelle) 
          Règlement officiel du jeu-concours Inscription au jeu-concours Inscription via compte Google
          Inscription via compte Facebook Inscription via formulaire classique (nom, prénom, email, mot de passe, etc.)
          Participation au jeu-concours
          Saisie du numéro de ticket de caisse pour participer
          Vérification instantanée du code pour déterminer le gain
          Affichage immédiat du lot gagné
          Visualisation de l'historique des gains
          Tableau de bord personnel pour chaque utilisateur
          Historique complet des participations et des gains associés
          Statut de récupération des lots (en attente, récupéré en magasin, envoyé par courrier)
          Administration et statistiques du jeu-concours
          Tableau de bord pour les administrateurs
          Statistiques en temps réel sur le nombre de tickets fournis et utilisés
          Statistiques sur les lots déjà gagnés
          Analyse des participants (sexe, âge, localisation, etc.)
          Exportation des données pour analyse supplémentaire
          Emailing et communication
          Outils pour les administrateurs pour envoyer des emails de communication
          Personnalisation des emails pour des promotions ou des notifications
          Gestion des gains en boutique
          Interface dédiée pour les employés en boutique
          Vérification des codes gagnants en magasin
          Enregistrement des lots remis en boutique
          Accessibilité et compatibilité
          Site accessible sur tous les devices (responsive design)
          Respect des normes d'accessibilité pour les personnes handicapées
          SEO (Search Engine Optimization) optimisé pour une meilleure visibilité
          Respect de la réglementation et de la confidentialité
          Conformité au RGPD (Règlement Général sur la Protection des Données)
          Politique de confidentialité détaillée
          Options pour les utilisateurs de gérer leurs données personnelles
          API et intégration
          API pour la communication avec la webApp, les caisses en magasin, et le futur site de vente en ligne
          Documentation API pour développeurs
          Sécurité des échanges de données entre les systèmes
          Éco-responsabilité
          Hébergement web éco-responsable
          Utilisation de technologies à faible consommation d'énergie
          Pratiques de développement durable intégrées dans le processus de création du site
          Support client
          FAQ (Foire Aux Questions) pour aider les utilisateurs
          Support technique par email ou chat en direct
          Formulaire de contact pour les questions ou problèmes
          Le site est accessible gratuitement en tout lieu à tout Utilisateur ayant un accès à Internet. Tous les frais supportés par l'Utilisateur pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge.

          L’Utilisateur non membre n'a pas accès aux services réservés. Pour cela, il doit s’inscrire en remplissant le formulaire. En acceptant de s’inscrire aux services réservés, l’Utilisateur membre s’engage à fournir des informations sincères et exactes concernant son état civil et ses coordonnées, notamment son adresse email.
          Pour accéder aux services, l’Utilisateur doit ensuite s'identifier à l'aide de son identifiant et de son mot de passe qui lui seront communiqués après son inscription.
          Tout Utilisateur membre régulièrement inscrit pourra également solliciter sa désinscription en se rendant à la page dédiée sur son espace personnel. Celle-ci sera effective dans un délai raisonnable.
          Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement du site ou serveur et sous réserve de toute interruption ou modification en cas de maintenance, n'engage pas la responsabilité de https://dsp5-archi-o22a-4-5-g3.fr. Dans ces cas, l’Utilisateur accepte ainsi ne pas tenir rigueur à l’éditeur de toute interruption ou suspension de service, même sans préavis.
          L'Utilisateur a la possibilité de contacter le site par messagerie électronique à l’adresse email de l’éditeur communiqué à l’ARTICLE 1.
          </p>
        </section>
        <section className="mb-8">
          <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">
            3. Collecte des données
          </h2>
          <p>
          Le site assure à l'Utilisateur une collecte et un traitement d'informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.
          En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978, l'Utilisateur dispose d'un droit d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'Utilisateur exerce ce droit :
          ·         par mail à l'adresse email thetiptopg1@gmail.com
          ·         par voie postale au 5 Avenue du Maréchal Joffre, 94120 Fontenay-sous-Bois ;
          ·         via un formulaire de contact ;
          ·         via son espace personnel ;

          </p>
        </section>
        <section className="mb-8">
          <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">
            4. Propriété intellectuelle
          </h2>
          <p>
          Les marques, logos, signes ainsi que tous les contenus du site (textes, images, son…) font l'objet d'une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d'auteur.
          La marque Thé Tip Top est une marque déposée par Mr Eric Bourdon.Toute représentation et/ou reproduction et/ou exploitation partielle ou totale de cette marque, de quelque nature que ce soit, est totalement prohibée.
          L'Utilisateur doit solliciter l'autorisation préalable du site pour toute reproduction, publication, copie des différents contenus. Il s'engage à une utilisation des contenus du site dans un cadre strictement privé, toute utilisation à des fins commerciales et publicitaires est strictement interdite.
          Toute représentation totale ou partielle de ce site par quelque procédé que ce soit, sans l’autorisation expresse de l’exploitant du site Internet constituerait une contrefaçon sanctionnée par l’article L 335-2 et suivants du Code de la propriété intellectuelle.
          Il est rappelé conformément à l’article L122-5 du Code de propriété intellectuelle que l’Utilisateur qui reproduit, copie ou publie le contenu protégé doit citer l’auteur et sa source.
          </p>
        </section>
        <section className="mb-8">
          <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">
            5. Responsabilité
          </h2>
          <p>
          Les sources des informations diffusées sur le site https://dsp5-archi-o22a-4-5-g3.fr. sont réputées fiables mais le site ne garantit pas qu’il soit exempt de défauts, d’erreurs ou d’omissions.
          Les informations communiquées sont présentées à titre indicatif et général sans valeur contractuelle. Malgré des mises à jour régulières, le site https://dsp5-archi-o22a-4-5-g3.fr. ne peut être tenu responsable de la modification des dispositions administratives et juridiques survenant après la publication. De même, le site ne peut être tenue responsable de l’utilisation et de l’interprétation de l’information contenue dans ce site.
          L'Utilisateur s'assure de garder son mot de passe secret. Toute divulgation du mot de passe, quelle que soit sa forme, est interdite. Il assume les risques liés à l'utilisation de son identifiant et mot de passe. Le site décline toute responsabilité.
          Le site https://dsp5-archi-o22a-15M-G3.fr ne peut être tenu pour responsable d’éventuels virus qui pourraient infecter l’ordinateur ou tout matériel informatique de l’Internaute, suite à une utilisation, à l’accès, ou au téléchargement provenant de ce site.
          La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d'un tiers.

          </p>
        </section>
        <section className="mb-8">
          <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">
            6. Liens hypertextes
          </h2>
          <p>
          Des liens hypertextes peuvent être présents sur le site. L’Utilisateur est informé qu’en cliquant sur ces liens, il sortira du site https://dsp5-archi-o22a-4-5-g3.fr. Ce dernier n’a pas de contrôle sur les pages web sur lesquelles aboutissent ces liens et ne saurait, en aucun cas, être responsable de leur contenu.
          </p>
        </section>
        <section className="mb-8">
          <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">
            7. Cookies
          </h2>
          <p>
          L’Utilisateur est informé que lors de ses visites sur le site, un cookie peut s’installer automatiquement sur son logiciel de navigation.
          Les cookies sont de petits fichiers stockés temporairement sur le disque dur de l’ordinateur de l’Utilisateur par votre navigateur et qui sont nécessaires à l’utilisation du site https://dsp5-archi-o22a-4-5-g3.fr Les cookies ne contiennent pas d’information personnelle et ne peuvent pas être utilisés pour identifier quelqu’un. Un cookie contient un identifiant unique, généré aléatoirement et donc anonyme. Certains cookies expirent à la fin de la visite de l’Utilisateur, d’autres restent.
          L’information contenue dans les cookies est utilisée pour améliorer le site https://dsp5-archi-o22a-4-5-g3.fr.
          En naviguant sur le site, L’Utilisateur les accepte.
          L’Utilisateur doit toutefois donner son consentement quant à l’utilisation de certains cookies.
          A défaut d’acceptation, l’Utilisateur est informé que certaines fonctionnalités ou pages risquent de lui être refusées.
          L’Utilisateur pourra désactiver ces cookies par l’intermédiaire des paramètres figurant au sein de son logiciel de navigation.
          </p>
        </section>
        <section className="mb-8">
          <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">
            8. Droit applicable et juridiction compétente
          </h2>
          <p>
          La législation française s'applique au présent contrat. En cas d'absence de résolution amiable d'un litige né entre les parties, les tribunaux français seront seuls compétents pour en connaître.
          Pour toute question relative à l’application des présentes CGU, vous pouvez joindre l’éditeur aux coordonnées inscrites à l’ARTICLE 1.
          </p>
        </section>
        <section className="mb-8">
          <h2 className="text-2xl font-bold mb-4 text-[#445d4d]">
            9. Sanction
          </h2>
          <p>
          En cas de non-respect des présentes conditions générales d'utilisation, Furious Ducks se réserve le droit de prendre les mesures disciplinaires et légales suivantes, sans préavis :
          1. Avertissements : Un rappel des obligations de l'Utilisateur sera envoyé par voie électronique en cas d'infraction mineure ou de comportement inapproprié.
          2. Suspension temporaire de l'accès aux services : Furious Ducks peut suspendre temporairement l'accès aux services en ligne, selon la gravité de la violation, afin de permettre à l'Utilisateur de corriger son comportement.
          3. Suppression du compte : En cas de récidive ou de manquement grave aux obligations stipulées dans les CGU, Furious Ducks se réserve le droit de supprimer définitivement le compte de l'Utilisateur, entraînant ainsi la perte d'accès à tous les services du site.
          4. Poursuites judiciaires : En cas d'infraction grave à la réglementation en vigueur (notamment en matière de propriété intellectuelle, de protection des données ou d'utilisation frauduleuse des services), Furious Ducks pourra engager des poursuites judiciaires à l'encontre de l'Utilisateur.
          Les sanctions sont appliquées en fonction de la gravité des faits et des récidives éventuelles. L'Utilisateur sanctionné sera informé par courrier électronique des mesures prises à son encontre.
          </p>
        </section>
      </div>
    </div>
  );
}
