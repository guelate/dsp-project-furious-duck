"use client";

import React from "react";
import { useRouter } from "next/navigation";
import { Card } from "@/components/ui/card";
import { Button } from "@/components/ui/button";

export default function BlockCard() {
  const router = useRouter();

  const handleClose = () => {
    router.push("/");
  };

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 backdrop-blur-sm z-10">
      <Card className="bg-white p-8 shadow-lg w-full max-w-md pt-10 relative rounded .rounded-lg">
        <div className="flex flex-col items-center justify-center gap-5">
          <p className="text-center text-lg font-semibold">
            Votre compte est bloqué
          </p>
          <Button
            className="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded"
            onClick={handleClose}
          >
            Fermer
          </Button>
        </div>
      </Card>
    </div>
  );
}

