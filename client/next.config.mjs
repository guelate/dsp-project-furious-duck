// /** @type {import('next').NextConfig} */
// const nextConfig = {
//     webpack: (config, { isServer }) => {
//       // Traiter les fichiers `.mjs` dans `node_modules` comme JavaScript natif
//       config.module.rules.push({
//         test: /\.mjs$/,
//         include: /node_modules/,
//         type: 'javascript/auto',
//       });
  
//       return config;
//     },
//     // Optionnel : Activer SWC pour remplacer Babel
//     swcMinify: true,
//   };
  
//   export default nextConfig;
  


/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true, // Si vous souhaitez l'utiliser, sinon laissez-le commenté
  images: {
      domains: [
          "images.unsplash.com",
          "source.unsplash.com",
          "lh3.googleusercontent.com", // Ajoutez ce domaine pour les images de Google
      ],
  },
};

export default nextConfig;
