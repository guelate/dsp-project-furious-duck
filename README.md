# 💻DSP PROJECT - Furious Duck 



## Accès base de donnée

https://supabase.com/dashboard/sign-in?returnTo=%2Forg%2Fwoergwnbdkruaqumfyqy%2Fteam


Identifiant: dsparchi022g3@gmail.com  
Mot de passe: Gq)_97^zkH2H8w

# Ticket
8QAY81UW6L  
20W8NK4W4K   
VAWIAABYHL  
T7FFCJM9L9  
WMRVWEMZW9  

## Compte utilisateur
Identifiant: Mario  
mot de passe: MarioMario

## Accès Espace Administration

URl: https://dsp5-archi-o22a-4-5-g3.fr/connexion


Identifiant: enzomorin@etu-digitalschool.paris
mot de passe: dspTea3

## Accès Documentation Projet
URl: https://dsp5-archi-o22a-4-5-g3.fr/Docs


# Commande de base du projet

#### Aller dans le chemin:

cd client
   
#### Récupérer le projet sur git
   
    Git Clone https://gitlab.com/guelate/dsp-project-furious-duck.git
 
#### Exécuter l'ensemble des services de l'application
 
    Docker compose up
 
#### Installation des dépendances du projet
 
    npm install --legacy-peer-deps
 
#### Lancement du projet
 
    npm run dev
 